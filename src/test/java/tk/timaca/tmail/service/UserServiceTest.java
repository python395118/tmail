package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.mapper.UserInformationMapper;

import java.util.List;

/**
 * Created by dede on 2017/4/14.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Autowired
    private UserInformationMapper userInformationMapper;

    @Test
    public void createAvatarByUsername() throws Exception {
        List<UserInformation> list = userInformationMapper.selectList(new EntityWrapper<UserInformation>());
        for (UserInformation userInformation:list) {
            String str = userService.createAvatarByUsername(userInformation.getNickName());
            userInformation.setAvatarPath(str);
            userInformationMapper.updateById(userInformation);
        }
    }

    @Test
    public void createUserNoPwd() throws Exception {
        System.out.println(userService.createUserNoPwd("15361931618@163.com"));
        System.out.println(userService.createUserNoPwd("758672830@qq.com"));
        System.out.println(userService.createUserNoPwd("18814182570@163.com"));
    }

    @Test
    public void UpdatePasswordTest() throws Exception{
        System.out.println(userService.UpdatePassword("test1","testTEST","testTEST"));
    }

}
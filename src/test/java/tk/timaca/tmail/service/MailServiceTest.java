package tk.timaca.tmail.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import tk.timaca.tmail.bean.JsonResult;

/**
 * Created by dede on 2017/4/26.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {

    @Autowired
    private MailService mailService;

    @Test
    public void findMailByTokenFromMailTest() throws Exception{
        String token = "25c88b7abb0d70dc8391a17c74c4f293299b608d";
        Integer type = 2;
        Integer first = 2;
        Integer sum = 4 ;
        JsonResult jsonResult = mailService.findMailByTokenFromMail(token,type,first,sum,"时间");
        System.out.println( jsonResult.getData());
    }

    @Test
    public void searchMailByKeyTest() throws Exception{
        System.out.println(mailService.searchMailByKey("<","test1",3,10,"时间"));
    }

    @Test
    public void sendEmailTest() throws Exception{
        mailService.sendEmail("test1","758672830@qq.com","","","标题","正文","");
    }
}
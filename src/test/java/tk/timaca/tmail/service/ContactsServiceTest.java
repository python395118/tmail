package tk.timaca.tmail.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by dede on 2017/5/8.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactsServiceTest {

    @Autowired
    private ContactsService contactsService;

    @Test
    public void addContactsTest() throws Exception {
        System.out.println(contactsService.addContacts("abc","15361931618@163.com","默认分组","153","dizhi","dianhua"));
        System.out.println(contactsService.addContacts("abc","18814182570@163.com","默认","188","dizhi","dianhua"));
    }

    @Test
    public void addContactsGroupTest() throws Exception {
        contactsService.addContactsGroup("abc","朋友");
        contactsService.addContactsGroup("abc","同事");
    }

    @Test
    public void getContactsTests() throws Exception {
        System.out.println(contactsService.getContactsByUsername("abc"));
    }

}
package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.UserInformation;

/**
 * Created by dede on 2017/3/19.
 * user_information数据层接口
 */
public interface UserInformationMapper extends BaseMapper<UserInformation>{
}

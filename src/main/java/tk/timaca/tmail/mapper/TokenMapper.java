package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Token;

/**
 * Created by dede on 2017/3/29.
 */
public interface TokenMapper extends BaseMapper<Token> {

}

package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.ContactsGroup;

/**
 * Created by dede on 2017/4/25.
 */
public interface ContactsGroupMapper extends BaseMapper<ContactsGroup> {
}

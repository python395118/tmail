package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Mail;

/**
 * Created by dede on 2017/4/10.
 */
public interface MailMapper extends BaseMapper<Mail> {

}

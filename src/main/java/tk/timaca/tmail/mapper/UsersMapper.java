package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Users;

/**
 * Created by dede on 2017/3/22.
 * users数据表操作接口
 */
public interface UsersMapper extends BaseMapper<Users>{

}

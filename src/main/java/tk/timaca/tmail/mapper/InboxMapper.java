package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Inbox;

/**
 * Created by dede on 2017/3/22.
 */
public interface InboxMapper extends BaseMapper<Inbox> {
}

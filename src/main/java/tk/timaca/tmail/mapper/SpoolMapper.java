package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Spool;

/**
 * Created by dede on 2017/3/22.
 */
public interface SpoolMapper extends BaseMapper<Spool> {
}

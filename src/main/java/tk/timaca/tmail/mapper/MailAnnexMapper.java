package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.MailAnnex;

/**
 * Created by dede on 2017/4/20.
 */
public interface MailAnnexMapper extends BaseMapper<MailAnnex> {
}

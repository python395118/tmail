package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Contacts;

/**
 * Created by dede on 2017/4/25.
 */
public interface ContactsMapper extends BaseMapper<Contacts> {
}

package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.Role;

/**
 * Created by dede on 2017/4/7.
 */
public interface RoleMapper extends BaseMapper<Role> {
}

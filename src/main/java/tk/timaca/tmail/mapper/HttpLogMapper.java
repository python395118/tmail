package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.HttpLog;

/**
 * Created by dede on 2017/3/31.
 * 请求日志数据层类
 */
public interface HttpLogMapper extends BaseMapper<HttpLog> {
}

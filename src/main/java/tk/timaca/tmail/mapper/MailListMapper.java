package tk.timaca.tmail.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import tk.timaca.tmail.bean.MailList;

/**
 * Created by dede on 2017/5/8.
 */
public interface MailListMapper extends BaseMapper<MailList> {
}

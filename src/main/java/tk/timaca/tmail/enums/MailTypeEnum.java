package tk.timaca.tmail.enums;

/**
 * Created by dede on 2017/4/12.
 * 邮件类型枚举
 */
public enum MailTypeEnum {

    DRAFTS(0,"草稿件"),
    SEND(1,"已发送"),
    INCOMING_MAIL(2,"收到的邮件"),
    TRASH_MAIL(3,"回收站邮件"),
    NOT_SET(-1,"没有设定的邮件类型"),
    SPAM_MAIL(4,"垃圾邮件")
    ;

    private Integer type;
    private String msg;

    MailTypeEnum(Integer type, String msg) {
        this.type = type;
        this.msg = msg;
    }

    public Integer getType() {
        return type;
    }

    public String getMsg() {
        return msg;
    }
}

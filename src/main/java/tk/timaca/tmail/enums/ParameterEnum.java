package tk.timaca.tmail.enums;

/**
 * Created by dede on 2017/4/8.
 * 传参异常枚举
 */
public enum ParameterEnum {
    NO_USERNAME("username参数不存在"),
    NO_TOKEN("token参数不存在"),
    NO_PASSWORD("password参数不存在"),
    NO_FROM("from参数不存在"),
    NO_TO("to参数不存在"),
    NO_SUBJECT("subject参数不存在"),
    NO_TEXT("text参数不存在"),
//    FONT_IMAGE_BASE_PATH("src/main/webapp/fontImage/"),
//    UPLOAD_IMAGE_BASE_PATH("src/main/webapp/"),
//    ANNEX_BASE_PATH("src/main/webapp/annex/"),
    FONT_IMAGE_BASE_PATH("fontImage/"),
    UPLOAD_IMAGE_BASE_PATH(""),
    ANNEX_BASE_PATH("annex/"),
    NO_MAIL_ID("-1"),
    ERROR("-1"),
    NO_PARAMS("参数不存在"),
    INBOX_LIST_BY_TIME("时间"),
    INBOX_LIST_BY_SUBJECT("主题"),
    INBOX_LIST_BY_FROM("发件人"),
    CHECK_IP_PERIOD("60000"),//统计时间段,毫秒
    CHECK_IP_SEND_MAX_TIMES("5"),//统计发送次数
    CHECH_IP_LOGIN_OR_REG_MAX_TIMES("5"),//统计登录注册次数
    SEND_ERROR_TEXT("发送邮件过于频繁，请稍后再试"),
    LOGIN_OR_REG_ERROR_TEXT("操作过于频繁，请稍后再试"),
    ADDRESS("@ttmail.ml"),
    ;

    private String msg;

    ParameterEnum(String msg) {
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }

    @Override
    public String toString() {
        return "ParameterEnum{" +
                "msg='" + msg + '\'' +
                '}';
    }
}

package tk.timaca.tmail.enums;

/**
 * Created by dede on 2017/3/28.
 */
public enum ResultEnum {
    LOGIN_SUCCESS(0,"登录成功"),
    LOGIN_FAIL(1,"登录失败"),
    LOGIN_FAIL_PASSWORD(2,"登录失败,密码错误"),
    LOGIN_FAIL_ACCOUNT(3,"登录失败,账号错误"),
    LOGIN_FAIL_CODE(4,"登录失败，验证码错误"),
    REGISTER_SUCCESS(5,"注册成功"),
    REGISTER_FAIL(6,"注册失败"),
    UPDATE_PASSWORD_SUCCESS(7,"密码修改成功"),
    UPDATE_PASSWORD_FAIL(8,"密码修改失败"),
    LOGOUT_SUCCESS(9,"退出成功"),
    LOGOUT_FAIL(10,"退出失败"),
    DELETE_SUCCESS(11,"删除成功"),
    DELETE_FAIL(12,"删除失败"),
    AUTH_YES(13,"有权限"),
    AUTH_NO(14,"无权限"),
    SEND_SUCCESS(15,"发送成功"),
    SEND_FAIL(16,"发送失败"),
    GET_MAILS_SUCCESS(17,"获取邮件列表成功"),
    GET_MAILS_FAIL(18,"获取邮件列表失败"),
    SUCCESS(200,"成功"),
    ERROR(404,"失败")
    ;

    private Integer code;

    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}

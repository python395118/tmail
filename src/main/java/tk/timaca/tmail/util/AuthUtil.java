package tk.timaca.tmail.util;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import tk.timaca.tmail.bean.Token;
import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by dede on 2017/3/19.
 * 权限工具类
 */
@Component
public class AuthUtil {

    /**
     * 验证码生成
     * @return
     */
    public static String CreateVerificationCode(){
        int which=-1;
        String code="";
        for(int i=6;i>0;i--){
            which=new Random().nextInt(4);
            switch (which){
                case 1: code = code + (char)(Math.random()*26+'A');
                    break;
                case 2: code = code + (char)(Math.random()*26+'a');
                    break;
                case 3: code = code + String.valueOf(new Random().nextInt(9));
                    break;
                default:
                    code = code + String.valueOf(new Random().nextInt(9));
            }
        }
        return code;
    }

    /**
     * 根据用户昵称、验证码、时间戳生成新的token
     * @param username
     * @param code
     * @param timestamp
     * @return
     */
    public static Token NewToken(String username, String code, long timestamp){
        String arr[]=new String[]{String.valueOf(timestamp),username,code};
        //排序
        Arrays.sort(arr);

        //生成字符串
        StringBuffer content=new StringBuffer();
        for(int i=0;i<arr.length;i++){
            content.append(arr[i]);
        }

        //sha1加密
        String temp=getSha1(content.toString());
        Token newToken = new Token(new Random().nextInt(9999999),username,temp,timestamp);
        return newToken;
    }



    /**
     * sha1加密算法
     * @param str
     * @return
     */
    private static String getSha1(String str){
        if (null == str || 0 == str.length()){
            return null;
        }
        char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest mdTemp = MessageDigest.getInstance("SHA");
            mdTemp.update(str.getBytes("UTF-8"));

            byte[] md = mdTemp.digest();
            int j = md.length;
            char[] buf = new char[j * 2];
            int k = 0;
            for (int i = 0; i < j; i++) {
                byte byte0 = md[i];
                buf[k++] = hexDigits[byte0 >>> 4 & 0xf];
                buf[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(buf);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * 刷新token
     */
//    @Scheduled(cron="0 0/5 * * * ?") //每分钟执行一次
    @Scheduled(cron = "0 0 2 * * ?") //每天凌晨两点执行一次 用于训练垃圾邮件过滤器
    public static void RefreshToken(){
        System.out.println("每天凌晨两点执行一次 用于训练垃圾邮件过滤器。开始……"+System.currentTimeMillis());

    }

    /*
   * 加密
   * 1.构造密钥生成器
   * 2.根据ecnodeRules规则初始化密钥生成器
   * 3.产生密钥
   * 4.创建和初始化密码器
   * 5.内容加密
   * 6.返回字符串
   */
    public static String AESEncode(String encodeRules,String content){
        try {
//            //1.构造密钥生成器，指定为AES算法,不区分大小写
//            KeyGenerator keygen= KeyGenerator.getInstance("AES");
//            //2.根据ecnodeRules规则初始化密钥生成器
//            //生成一个128位的随机源,根据传入的字节数组
//            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
//            random.setSeed(encodeRules.getBytes());
//            keygen.init(128, random);
//            //keygen.init(128, new SecureRandom(encodeRules.getBytes()));
//            //3.产生原始对称密钥
//            SecretKey original_key=keygen.generateKey();
            SecretKey original_key = getKey(encodeRules);
            //4.获得原始对称密钥的字节数组
            byte [] raw=original_key.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKey key=new SecretKeySpec(raw, "AES");
            //6.根据指定算法AES自成密码器
            Cipher cipher=Cipher.getInstance("AES");
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(Cipher.ENCRYPT_MODE, key);
            //8.获取加密内容的字节数组(这里要设置为utf-8)不然内容中如果有中文和英文混合中文就会解密为乱码
            byte [] byte_encode=content.getBytes("utf-8");
            //9.根据密码器的初始化方式--加密：将数据加密
            byte [] byte_AES=cipher.doFinal(byte_encode);
            //10.将加密后的数据转换为字符串
            //这里用Base64Encoder中会找不到包
            //解决办法：
            //在项目的Build path中先移除JRE System Library，再添加库JRE System Library，重新编译后就一切正常了。
            String AES_encode=new String(new BASE64Encoder().encode(byte_AES));
            //11.将字符串返回
            return AES_encode;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        //如果有错就返加nulll
        return null;
    }
    /*
     * 解密
     * 解密过程：
     * 1.同加密1-4步
     * 2.将加密后的字符串反纺成byte[]数组
     * 3.将加密内容解密
     */
    public static String AESDncode(String encodeRules,String content){
        try {
            //1.构造密钥生成器，指定为AES算法,不区分大小写
//            KeyGenerator keygen=KeyGenerator.getInstance("AES");
//            //2.根据ecnodeRules规则初始化密钥生成器
//            //生成一个128位的随机源,根据传入的字节数组
//            SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
//            random.setSeed(encodeRules.getBytes());
//            keygen.init(128, random);
////            keygen.init(128, new SecureRandom(encodeRules.getBytes()));
//            //3.产生原始对称密钥
//            SecretKey original_key=keygen.generateKey();
            SecretKey original_key = getKey(encodeRules);
            //4.获得原始对称密钥的字节数组
            byte [] raw=original_key.getEncoded();
            //5.根据字节数组生成AES密钥
            SecretKey key=new SecretKeySpec(raw, "AES");
            //6.根据指定算法AES自成密码器
            Cipher cipher= Cipher.getInstance("AES");
            //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密(Decrypt_mode)操作，第二个参数为使用的KEY
            cipher.init(Cipher.DECRYPT_MODE, key);
            //8.将加密并编码后的内容解码成字节数组
            byte [] byte_content= new BASE64Decoder().decodeBuffer(content);
            /*
             * 解密
             */
            byte [] byte_decode=cipher.doFinal(byte_content);
            String AES_decode=new String(byte_decode,"utf-8");
            return AES_decode;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        }

        //如果有错就返加nulll
        return null;
    }

    /**
     * 生成KEY
     * SecureRandom 实现完全随操作系统本身的內部状态，
     * 除非调用方在调用 getInstance 方法之后又调用了 setSeed 方法；
     * 该实现在 windows 上每次生成的 key 都相同，但是在 solaris 或部分 linux 系统上则不同。
     * @param strKey
     * @return
     */
    public static SecretKey getKey(String strKey) {
        try {
            KeyGenerator _generator = KeyGenerator.getInstance( "AES" );
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG" );
            secureRandom.setSeed(strKey.getBytes());
            _generator.init(128,secureRandom);
            return _generator.generateKey();
        }  catch (Exception e) {
            throw new RuntimeException( " 初始化密钥出现异常 " );
        }
    }

    public static void main(String[] args) throws Exception{
        /*int i=0;
        long time=System.currentTimeMillis();
        while (i<50) {
            i++;
            System.out.println(AuthUtil.Signature(time, 0L));
        }*/
//        System.out.println(AuthUtil.getSha1("test1"));
//        System.out.println(AuthUtil.encryptSHA("test1"));
        System.out.println(DigestUtil.digestString("testTEST","SHA"));
//        System.out.println(AuthUtil.CreateVerificationCode());
//        System.out.println(String.valueOf(new Random().nextInt(9)));
//        System.out.println(new Random().nextInt(9));
        System.out.println(AuthUtil.AESEncode("test2","testTEST"));
//        System.out.println(AuthUtil.AESEncode("test4","test4"));
//        System.out.println(AuthUtil.AESDncode("test2","GvLfltvKtT58mqdCl/GS5Q=="));
        System.out.println(AuthUtil.AESDncode("test2","MhIxBwYiaM4yY+9uTUW7ww=="));
        System.out.println(AuthUtil.AESDncode("test2","ZKPbJ+IrkTWDJdKlaWRmKQ=="));

    }

}

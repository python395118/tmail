package tk.timaca.tmail.util;

/**
 * Created by dede on 2017/4/20.
 * http://blog.csdn.net/smok56888/article/details/50070453
 * 发送邮件工具类
 *
 *  sina.com:
     POP3服务器地址:pop3.sina.com.cn（端口：110）
     SMTP服务器地址:smtp.sina.com.cn（端口：25）

     sinaVIP：
     POP3服务器:pop3.vip.sina.com （端口：110）
     SMTP服务器:smtp.vip.sina.com （端口：25）

     sohu.com:
     POP3服务器地址:pop3.sohu.com（端口：110）
     SMTP服务器地址:smtp.sohu.com（端口：25）

     126邮箱：
     POP3服务器地址:pop.126.com（端口：110）
     SMTP服务器地址:smtp.126.com（端口：25）

     139邮箱：
     POP3服务器地址：POP.139.com（端口：110）
     SMTP服务器地址：SMTP.139.com(端口：25)

     163.com:
     POP3服务器地址:pop.163.com（端口：110）
     SMTP服务器地址:smtp.163.com（端口：25）

     QQ邮箱
     POP3服务器地址：pop.qq.com（端口：110）
     SMTP服务器地址：smtp.qq.com （端口：25）

     QQ企业邮箱
     POP3服务器地址：pop.exmail.qq.com （SSL启用 端口：995）
     SMTP服务器地址：smtp.exmail.qq.com（SSL启用 端口：587/465）

     yahoo.com:
     POP3服务器地址:pop.mail.yahoo.com
     SMTP服务器地址:smtp.mail.yahoo.com

     yahoo.com.cn:
     POP3服务器地址:pop.mail.yahoo.com.cn（端口：995）
     SMTP服务器地址:smtp.mail.yahoo.com.cn（端口：587

     HotMail
     POP3服务器地址：pop3.live.com （端口：995）
     SMTP服务器地址：smtp.live.com （端口：587）

     gmail(google.com)
     POP3服务器地址:pop.gmail.com（SSL启用 端口：995）
     SMTP服务器地址:smtp.gmail.com（SSL启用 端口：587）

     263.net:
     POP3服务器地址:pop3.263.net（端口：110）
     SMTP服务器地址:smtp.263.net（端口：25）

     263.net.cn:
     POP3服务器地址:pop.263.net.cn（端口：110）
     SMTP服务器地址:smtp.263.net.cn（端口：25）

     x263.net:
     POP3服务器地址:pop.x263.net（端口：110）
     SMTP服务器地址:smtp.x263.net（端口：25）

     21cn.com:
     POP3服务器地址:pop.21cn.com（端口：110）
     SMTP服务器地址:smtp.21cn.com（端口：25）

     Foxmail：
     POP3服务器地址:POP.foxmail.com（端口：110）
     SMTP服务器地址:SMTP.foxmail.com（端口：25）

     china.com:
     POP3服务器地址:pop.china.com（端口：110）
     SMTP服务器地址:smtp.china.com（端口：25）

     tom.com:
     POP3服务器地址:pop.tom.com（端口：110）
     SMTP服务器地址:smtp.tom.com（端口：25）

     etang.com:
     POP3服务器地址:pop.etang.com
     SMTP服务器地址:smtp.etang.com

 */

import tk.timaca.tmail.enums.ParameterEnum;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.*;

public class SendEmailUtil {
    private static String defaultSenderName = "";// 默认的发件人用户名，defaultEntity用得到
    private static String defaultSenderPass = "";// 默认的发件人密码，defaultEntity用得到
    private static String defaultSmtpHost = "";// 默认的邮件服务器地址，defaultEntity用得到

    private String smtpHost; // 邮件服务器地址
    private String sendUserName; // 发件人的用户名
    private String sendUserPass; // 发件人密码

    private MimeMessage mimeMsg; // 邮件对象
    private Session session;
    private Properties props;
    private Multipart mp;// 附件添加的组件
    private List<FileDataSource> files = new LinkedList<FileDataSource>();// 存放附件文件

    private void init() {
        if (props == null) {
            props = System.getProperties();
        }
        props.put("mail.smtp.host", smtpHost);
        props.put("mail.smtp.auth", "true"); // 需要身份验证
        session = Session.getDefaultInstance(props, null);
        // 置true可以在控制台（console)上看到发送邮件的过程
        session.setDebug(true);
        // 用session对象来创建并初始化邮件对象
        mimeMsg = new MimeMessage(session);
        // 生成附件组件的实例
        mp = new MimeMultipart();
    }

    private SendEmailUtil(String smtpHost, String sendUserName, String sendUserPass, String to, String cc,String bcc, String mailSubject, String mailBody,
                          List<String> attachments) {
        this.smtpHost = smtpHost;
        this.sendUserName = sendUserName;
        this.sendUserPass = sendUserPass;

        init();
        setFrom(sendUserName);
        setTo(to);
        setCC(cc);
        setBCC(bcc);
        setBody(mailBody);
        setSubject(mailSubject);
        if (attachments != null) {
            for (String attachment : attachments) {
                addFileAffix(attachment);
            }
        }

    }

    /**
     * 邮件实体
     *
     * @param smtpHost
     *            邮件服务器地址
     * @param sendUserName
     *            发件邮件地址
     * @param sendUserPass
     *            发件邮箱密码
     * @param to
     *            收件人，多个邮箱地址以半角逗号分隔
     * @param cc
     *            抄送，多个邮箱地址以半角逗号分隔
     * @param bcc
     *            密送，多个邮箱地址以半角逗号分隔
     * @param mailSubject
     *            邮件主题
     * @param mailBody
     *            邮件正文
     * @param attachments
     *            附件路径
     * @return
     */
    public static SendEmailUtil entity(String smtpHost, String sendUserName, String sendUserPass, String to, String cc,String bcc, String mailSubject, String mailBody,
                                       List<String> attachments) {
        return new SendEmailUtil(smtpHost, sendUserName, sendUserPass, to, cc,bcc, mailSubject, mailBody, attachments);
    }

    /**
     * 默认邮件实体，用了默认的发送帐号和邮件服务器
     *
     * @param to
     *            收件人，多个邮箱地址以半角逗号分隔
     * @param cc
     *            抄送，多个邮箱地址以半角逗号分隔
     * @param bcc
     *            密送，多个邮箱地址以半角逗号分隔
     * @param subject
     *            邮件主题
     * @param body
     *            邮件正文
     * @param attachments
     *            附件全路径
     * @return
     */
    public static SendEmailUtil defaultEntity(String to, String cc, String bcc,String subject, String body, List<String> attachments) {
        return new SendEmailUtil(defaultSmtpHost, defaultSenderName, defaultSenderPass, to, cc, bcc,subject, body, attachments);
    }

    /**
     * 设置邮件主题
     *
     * @param mailSubject
     * @return
     */
    private boolean setSubject(String mailSubject) {
        try {
            mimeMsg.setSubject(mailSubject);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 设置邮件内容,并设置其为文本格式或HTML文件格式，编码方式为UTF-8
     *
     * @param mailBody
     * @return
     */
    private boolean setBody(String mailBody) {
        try {
            BodyPart bp = new MimeBodyPart();
            bp.setContent("<meta http-equiv=Content-Type content=text/html; charset=UTF-8>" + mailBody, "text/html;charset=UTF-8");
            // 在组件上添加邮件文本
            mp.addBodyPart(bp);
        } catch (Exception e) {
            System.err.println("设置邮件正文时发生错误！" + e);
            return false;
        }
        return true;
    }

    /**
     * 添加一个附件
     *
     * @param filename
     *            邮件附件的地址，只能是本机地址而不能是网络地址，否则抛出异常
     * @return
     */
    public boolean addFileAffix(String filename) {
        try {
            if (filename != null && filename.length() > 0) {
                BodyPart bp = new MimeBodyPart();
                FileDataSource fileds = new FileDataSource(filename);
                bp.setDataHandler(new DataHandler(fileds));
                bp.setFileName(MimeUtility.encodeText(fileds.getName(), "utf-8", null)); // 解决附件名称乱码
                mp.addBodyPart(bp);// 添加附件
                files.add(fileds);
            }
        } catch (Exception e) {
            System.err.println("增加邮件附件：" + filename + "发生错误！" + e);
            return false;
        }
        return true;
    }

    /**
     * 删除所有附件
     *
     * @return
     */
    public boolean delFileAffix() {
        try {
            FileDataSource fileds = null;
            for (Iterator<FileDataSource> it = files.iterator(); it.hasNext();) {
                fileds = it.next();
                if (fileds != null && fileds.getFile() != null) {
                    fileds.getFile().delete();
                }
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 设置发件人地址
     *
     * @param from
     *            发件人地址
     * @return
     */
    private boolean setFrom(String from) {
        try {
            mimeMsg.setFrom(new InternetAddress(from));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 设置收件人地址
     *
     * @param to 收件人的地址
     * @return
     */
    private boolean setTo(String to) {
        if (to == null)
            return false;
        try {
            mimeMsg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 设置抄送
     *
     * @param cc
     * @return
     */
    private boolean setCC(String cc) {
        if (cc == null) {
            return false;
        }
        try {
            mimeMsg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 设置密送
     *
     * @param bcc
     * @return
     */
    private boolean setBCC(String bcc) {
        if (bcc == null) {
            return false;
        }
        try {
            mimeMsg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(bcc));
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    /**
     * 发送邮件
     *
     * @return
     */
    public boolean send() throws Exception {
        /*From: 发件人
           其中 InternetAddress 的三个参数分别为: 邮箱, 显示的昵称(只用于显示, 没有特别的要求), 昵称的字符集编码
           真正要发送时, 邮箱必须是真实有效的邮箱。*/
        mimeMsg.setFrom(new InternetAddress(sendUserName+ ParameterEnum.ADDRESS.getMsg(), sendUserName, "UTF-8"));
        mimeMsg.setContent(mp);
        mimeMsg.saveChanges();
        System.out.println("正在发送邮件....");
        Transport transport = session.getTransport("smtp");
        // 连接邮件服务器并进行身份验证
        transport.connect(smtpHost, sendUserName, sendUserPass);
        /*// 发送邮件
        transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.TO));
        *//*发送给抄送*//*
        transport.sendMessage(mimeMsg, mimeMsg.getRecipients(Message.RecipientType.CC));*/
        // 发送，message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人, 抄送人, 密送人
        transport.sendMessage(mimeMsg, mimeMsg.getAllRecipients());
        System.out.println("发送邮件成功！");
        transport.close();
        return true;
    }

    public static void main(String[] args) throws Exception{
        String userName = "test1"; // 发件人邮箱
        String password = "testTEST"; // 发件人密码
        String smtpHost = "123.207.246.38"; // 邮件服务器

//        String to = "test1@tmail.com,test2@tmail.com,test3@tmail.com,test4@tmail.com,test5@tmail.com"; // 收件人，多个收件人以半角逗号分隔
        String to = "758672830@qq.com";
        String cc = "18814182570@sohu.com,test1@ttmail.ml,18814182570@163.com"; // 抄送，多个抄送以半角逗号分隔
        String bcc = "dengdeming@aliyun.com";
//        String cc = "";
        String subject = "10:41这是邮件的主题"; // 主题
        String body = "10:41这是邮件的正文"; // 正文，可以用html格式的哟
        List<String> attachments = Arrays.asList("C:\\Users\\dede\\Desktop\\反垃圾流程图.vsd", "C:\\Users\\dede\\Pictures\\树人格鲁特\\snapshot20150521212518.jpg"); // 附件的路径，多个附件也不怕
//        List<String> attachments = null;
        SendEmailUtil email = SendEmailUtil.entity(smtpHost, userName, password, to, cc , bcc, subject, body, attachments);

        email.send(); // 发送！
    }
}

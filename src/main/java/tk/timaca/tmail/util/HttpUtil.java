package tk.timaca.tmail.util;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by dede on 2017/3/21.
 * json工具类
 */
public class HttpUtil {

    /**
     * 解析request获取json 并解析json返回map
     * 每个request只能调用该方法一次，因为字节流在第一次调用的时候已经关闭，有几率使字节流丢失
     */
    public static Map<String,Object> JsonToMap(HttpServletRequest request){


        /*HttpServletRequest requestCopy = null;
        try {
           requestCopy = new BodyReaderHttpServletRequestWrapper(request);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        String str = RequestToString(request);

        /**
         * 去掉换行
         */
        str = str.replaceAll("\n","");
        str = str.replaceAll("\t","");

        System.out.println(str);

        /**
         * 解析json字符串 存储到map中
         */
        /*JacksonJsonParser jacksonJsonParser=new JacksonJsonParser();
        Map<String, Object> map = new HashMap<String, Object>();
        // 参数Map
        Map properties = jacksonJsonParser.parseMap(str);
        // 返回值Map

        Iterator entries = properties.entrySet().iterator();
        Map.Entry entry;
        String name = "";
        String value = "";
        while (entries.hasNext()) {
            entry = (Map.Entry) entries.next();
            name = (String) entry.getKey();
            Object valueObj = entry.getValue();
            if(null == valueObj){
                value = "";
            }else if(valueObj instanceof String[]){
                String[] values = (String[])valueObj;
                for(int i=0;i<values.length;i++){
                    value = values[i] + ",";
                }
                value = value.substring(0, value.length()-1);
            }else{
                value = valueObj.toString();
            }
            map.put(name, value);
        }*/
        ObjectMapper mapper = new ObjectMapper(); //转换器
        Map map = new HashMap<String, Object>();
        try{
            map = mapper.readValue(str,Map.class);
        }catch (IOException E){

        }

        return map;
    }

    /**
     * json2Map
     */
    public static Map<String,Object> JsonToMap(String jsonString){

        String str = jsonString;

        /**
         * 去掉换行
         */
        str = str.replaceAll("\n","");
        str = str.replaceAll("\t","");

        System.out.println(str);

        /**
         * 解析json字符串 存储到map中
         */
        ObjectMapper mapper = new ObjectMapper(); //转换器
        Map map = new HashMap<String, Object>();
        try{
            map = mapper.readValue(str,Map.class);
        }catch (IOException E){

        }

        return map;
    }

    /**
     * 获取字节流并保存到str
     * @param request
     * @return
     */
    public static String RequestToString(HttpServletRequest request){

        /**
         * 获取字节流并保存到str
         */
        InputStream inputStream = null;
        InputStreamReader isr=null;
        String str="";
        try{
            inputStream = request.getInputStream();
            isr = new InputStreamReader(inputStream,request.getCharacterEncoding());
            int i;
            while ((i=isr.read())!=-1){
//                System.out.println("i: "+i);
                str=str+(char)i;
            }
            inputStream.close();
            isr.close();
        }catch (IOException e){

        }
        return str;
    }

    /**
     * 获取上传文件并保存到指定路径，然后返回保存路径
     * @param request
     * @return
     */
    public static String uploadFile(HttpServletRequest request){
        String src ="";
        return src;
    }

    public static void main(String[] args) {
        String str = "{\n" +
                "\t\"username\":\"test4\",\n" +
                "\t\"password\":\"test4\"\n" +
                "}";
        ObjectMapper mapper = new ObjectMapper(); //转换器
        Map map = new HashMap<String, Object>();
        try{
            map = mapper.readValue(str,Map.class);
        }catch (IOException E){

        }
        Set set = map.entrySet();
        System.out.println(map.size()+":"+set.size());
    }



}

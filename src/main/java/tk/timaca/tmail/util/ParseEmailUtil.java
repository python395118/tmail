package tk.timaca.tmail.util;

import tk.timaca.tmail.bean.Mail;
import tk.timaca.tmail.bean.MailAnnex;
import tk.timaca.tmail.enums.MailTypeEnum;
import tk.timaca.tmail.enums.ParameterEnum;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeUtility;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Created by dede on 2017/4/20.
 * 解析邮件工具类
 */
public class ParseEmailUtil {

    /**
     * 解析邮件并转换成mail对象
     * 当start与end相等时且在1---getMessageCount()时，则获取第start封邮件
     * http://blog.csdn.net/muzizhuben/article/details/9284821
     * @param username
     * @param password
     * @param start
     * @param end
     */
    public static List parseEmailByInbox(String username,String password,Integer start,Integer end){
        /*list用于存放邮件对象和邮件附件对象集合*/
        List list = new ArrayList();
        Mail mail  = new Mail();
        try {
            // 1. 设置连接信息, 生成一个 Session
            Properties props = new Properties();
            props.put("mail.transport.protocol", "pop3");// POP3 收信协议
            props.put("mail.pop.port", "110");
            // props.put("mail.debug", "true");// 调试

            Session session = Session.getInstance(props);

            // 2. 获取 Store 并连接到服务器
            Store store = session.getStore("pop3");
            store.connect("localhost", username, password);
            // 3. 通过 Store 打开默认目录 Folder
            Folder folder = store.getDefaultFolder();// 默认父目录
            if (folder == null) {

                System.out.println("服务器不可用");
                return null;
            }

            Folder popFolder = folder.getFolder("INBOX");// 获取收件箱
            popFolder.open(Folder.READ_WRITE);// 可读邮件,可以删邮件的模式打开目录
            // 4. 列出来收件箱 下所有邮件
//            Message[] messages = popFolder.getMessages();

            if(start <= 0 || start > popFolder.getMessageCount()){
                start = popFolder.getMessageCount();
            }else{}
            if(end <= 0 || end > popFolder.getMessageCount()){
                end = popFolder.getMessageCount();
            }else{}

            Message[] messages =  popFolder.getMessages(start,end);
            // 取出来邮件数
//            int msgCount = popFolder.getMessageCount();
            int msgCount = messages.length;
            System.out.println("共有邮件: " + msgCount + "封");

            // FetchProfile fProfile = new FetchProfile();// 选择邮件的下载模式,
            // 根据网速选择不同的模式
            // fProfile.add(FetchProfile.Item.ENVELOPE);
            // folder.fetch(messages, fProfile);// 选择性的下载邮件

            // 5. 循环处理每个邮件并实现邮件转为新闻的功能
            for (int i = 0; i < msgCount; i++) {
                /*创建附件集合*/
                List<MailAnnex> mailAnnexes = new ArrayList<MailAnnex>();
        /*生成mailId*/
                Long mailId = StringUtil.randomLong();
                String content = "";
                String fromMail = "";
                List<String> ccOrBcc = new ArrayList<String>();

                Message msg = messages[i];
                // 发件人信息
                Address[] froms = msg.getFrom();
                System.out.println("发送时间："+msg.getSentDate());
                if(froms != null) {
                    System.out.println("发件人信息:" + froms[0]);
                    InternetAddress addr = (InternetAddress)froms[0];
                    System.out.println("发件人地址:" + addr.getAddress());
                    fromMail = addr.getAddress();
                    System.out.println("发件人显示名:" + addr.getPersonal());
                }

                String cc = getMailAddress("cc",msg);
                String bcc = getMailAddress("bcc",msg);
                System.out.println("邮件抄送："+cc);
                System.out.println("邮件密送："+bcc);

                String[] ccArr = splitEmailAddr(cc);
                if(ccArr!=null){
                    for (String string:ccArr) {
                        ccOrBcc.add(string);
                    }
                }
                String[] bccArr = splitEmailAddr(bcc);
                if(bccArr!=null){
                    for (String string:bccArr){
                        ccOrBcc.add(string);
                    }
                }


                System.out.println("邮件主题:" + msg.getSubject());
                String subject = msg.getSubject();
                // getContent() 是获取包裹内容, Part 相当于外包装
                System.out.println("邮件正文类型："+msg.getContentType());
                if(msg.getContentType().indexOf("text/plain") == -1){
                    Multipart multipart = (Multipart) msg.getContent();// 获取邮件的内容, 就一个大包裹,
                    // MultiPart
                    // 包含所有邮件内容(正文+附件)
                    System.out.println("邮件共有" + multipart.getCount() + "部分组成");
                    // 依次处理各个部分
                    for (int j = 0, n = multipart.getCount(); j < n; j++) {
                        System.out.println("处理第" + j + "部分");
                        Part part = multipart.getBodyPart(j);//解包, 取出 MultiPart的各个部分, 每部分可能是邮件内容,
                        // 也可能是另一个小包裹(MultipPart)

                        // 判断此包裹内容是不是一个小包裹, 一般这一部分是 正文 Content-Type: multipart/alternative
                        if (part.getContent() instanceof Multipart) {
                            Multipart p = (Multipart) part.getContent();// 转成小包裹
                            System.out.println("小包裹中有" + p.getCount() + "部分");
                            // 列出小包裹中所有内容
                            for (int k = 0; k < p.getCount(); k++) {
                                System.out.println("小包裹内容:" + p.getBodyPart(k).getContent());
                                System.out.println("内容类型:"
                                        + p.getBodyPart(k).getContentType());
                                if(p.getBodyPart(k).getContentType().startsWith("text/plain")) {
                                    // 处理文本正文
//                                news.setBody(p.getBodyPart(k).getContent() + "");
                                    content = p.getBodyPart(k).getContent() + "";
                                } else {
                                    // 处理 HTML 正文
//                                news.setBody(p.getBodyPart(k).getContent() + "");
                                    content = p.getBodyPart(k).getContent() + "";
                                }
                            }
                        }else{
                            System.out.println("内容类型:"
                                    + part.getContentType());
                            if(part.getContentType().startsWith("text/")) {
                                // 处理文本正文 处理 HTML 正文
//                                news.setBody(p.getBodyPart(k).getContent() + "");
                                content = part.getContent() + "";
                            } else {
                                // 处理 HTML 正文
//                                news.setBody(p.getBodyPart(k).getContent() + "");
//                                content = part.getContent() + "";
                            }
                            System.out.println(content);
                        }

                        // Content-Disposition: attachment;    filename="String2Java.jpg"
                        String disposition = part.getDisposition();// 处理是否为附件信息

                        if (disposition != null) {
                            String filename =  part.getFileName();
                            if(filename.toLowerCase().indexOf("gb2312")!=-1
                                    ||filename.toLowerCase().indexOf("utf")!=-1){
                                filename = MimeUtility.decodeText(filename);
                            }
                            System.out.println("发现附件: " + filename);
                            System.out.println("内容类型: " + part.getContentType());
                            System.out.println("附件内容:" + part.getContent());
                            java.io.InputStream in = part.getInputStream();// 打开附件的输入流
                            // 读取附件字节并存储到文件中
                  /*  if(!new File(ParameterEnum.ANNEX_BASE_PATH.getMsg()).exists()){

                    }*/
                            File file = new File(ParameterEnum.ANNEX_BASE_PATH.getMsg()+StringUtil.randomLong()+filename);
                            System.out.println("附件保存路径："+file.getAbsolutePath());
                            java.io.FileOutputStream out = new FileOutputStream(file);
                            int data;
                            while((data = in.read()) != -1) {
                                out.write(data);
                            }
                            in.close();
                            out.close();

                            /*往附件集合中添加附件对象*/
                            mailAnnexes.add(new MailAnnex(StringUtil.randomLong(),file.getPath(),part.getFileName(),part.getContentType(),mailId));
                        }
                    }

                    // 6. 删除单个邮件, 标记一下邮件需要删除, 不会真正执行删除操作
                    // msg.setFlag(Flags.Flag.DELETED, true);
                }else{
                    System.out.println(msg.getContent());
                    content = msg.getContent().toString();
                }

                mail.setBcc(bcc);
                mail.setWcc(cc);
                mail.setMailId(mailId);
                mail.setContent(content);
                mail.setFlagRead(0);
                mail.setFromMail(fromMail);
                mail.setTimestamp(msg.getSentDate().getTime());
                mail.setSubject(subject);
                mail.setToMail(StringUtil.isTmail(username));
                mail.setType(MailTypeEnum.INCOMING_MAIL.getType());
                list.add(mail);
                list.add(mailAnnexes);

            }

            // 7. 关闭 Folder 会真正删除邮件, false 不删除
            popFolder.close(true);
            // 8. 关闭 store, 断开网络连接
            store.close();
        } catch (NoSuchProviderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return list;
    }

    /**
     *      https://my.oschina.net/cshuangxi/blog/223417
     * 　*　获得邮件的收件人，抄送，和密送的地址和姓名，根据所传递的参数的不同
     * 　*　"to"----收件人　"cc"---抄送人地址　"bcc"---密送人地址 　
     */
    public  static String getMailAddress(String type,Message mimeMessage) throws Exception {
        String mailAddr = "";
        String addType = type.toUpperCase();

        InternetAddress[] address = null;
        if (addType.equals("TO") || addType.equals("CC")
                || addType.equals("BCC")) {

            if (addType.equals("TO")) {
                address = (InternetAddress[]) mimeMessage
                        .getRecipients(Message.RecipientType.TO);
            } else if (addType.equals("CC")) {
                address = (InternetAddress[]) mimeMessage
                        .getRecipients(Message.RecipientType.CC);
            } else {
                address = (InternetAddress[]) mimeMessage
                        .getRecipients(Message.RecipientType.BCC);
            }

            if (address != null) {
                for (int i = 0; i < address.length; i++) {
                    String emailAddr = address[i].getAddress();
                    if (emailAddr == null) {
                        emailAddr = "";
                    } else {
                        System.out.println("转换之前的emailAddr: " + emailAddr);
                        emailAddr = MimeUtility.decodeText(emailAddr);
                        System.out.println("转换之后的emailAddr: " + emailAddr);
                    }
                    String personal = address[i].getPersonal();
                    if (personal == null) {
                        personal = "";
                    } else {
                        System.out.println("转换之前的personal: " + personal);
                        personal = MimeUtility.decodeText(personal);
                        System.out.println("转换之后的personal: " + personal);
                    }
                    String compositeto = personal + "<" + emailAddr + ">";
                    System.out.println("完整的邮件地址：" + compositeto);
                    mailAddr += "," + emailAddr;
                }
                mailAddr = mailAddr.substring(1);
            }
        } else {
            throw new Exception("错误的电子邮件类型!");
        }
        return mailAddr;
    }

    private static String[] splitEmailAddr(String string){
        if(string !=""){
            return string.split("\\,");
        }else{
            return null;
        }
    }
    public static void main(String[] args) {
        List list = ParseEmailUtil.parseEmailByInbox("test4","test4" ,0,0);
        if(list.size() > 1){
            Mail mail =(Mail)list.get(0);
            System.out.println(mail.toString());
            List<MailAnnex> mailAnnexes = (List<MailAnnex>)list.get(1);
            for (MailAnnex mailAnnex:mailAnnexes) {
                System.out.println(mailAnnex.toString());
            }
        }
    }
}

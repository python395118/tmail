package tk.timaca.tmail.util;

import tk.timaca.tmail.enums.MailTypeEnum;
import tk.timaca.tmail.enums.ParameterEnum;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dede on 2017/4/6.
 * 字符串工具类
 */
public class StringUtil {

    private static final String  ENGLISH_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 判断字符串是否为email,返回邮件地址
     * @param str
     * @return
     */
    public static String isTmail(String str){
        String email="\\w[-\\w.+]*@([A-Za-z0-9][-A-Za-z0-9]+\\.)+[A-Za-z]{2,14}";
        Matcher matcher= Pattern.compile(email).matcher(str);
        return matcher.find()?str:str+"@ttmail.ml";
    }

    /**
     * 判断字符数组是否含有指定字符
     * @param str
     * @param str1
     * @return
     */
    public static boolean isContain(String[] str,String str1){
        int length = str.length;
        int i = 0;
        while(i < length){
            if(str1.equals(str[i])){
                return true;
            }else{
                i++;
            }
        }
        return false;
    }

    /**
     * 随机生成Long
     * @return
     */
    public static Long randomLong(){
        return new Long(new Random().nextInt(54378635));
    }

    /**
     * 获取首字母或汉字来，用来进行绘制头像
     * @param nick
     * @return
     */
    public static String getCharString(String nick){

        if(nick==null){
            return  String.valueOf(ENGLISH_CHARS.charAt((int)(Math.random() * 26)));
        }

        char[] chars = nick.toCharArray();
        if(chars.length>0){
            char c=chars[0];
            if(Character.isLetter(c)){
                //是字母
                if(Character.isLowerCase(c)){
                    //是否是小写字母
                    c=Character.toUpperCase(c);
                    return String.valueOf(c);
                }else {
                    return String.valueOf(c);
                }
            }else{
                //不是字母返回
                return String.valueOf(c);
            }
        }
        return String.valueOf(ENGLISH_CHARS.charAt((int)(Math.random() * 26)));
    }

    /**
     * 判断类型是否包含在MailTypeEnum中
     * @param type
     * @return
     */
    public static boolean isMailTypeEnum(Integer type){
        for(MailTypeEnum mailTypeEnum:MailTypeEnum.values()){
            if(mailTypeEnum.getType() == type){
                return true;
            }
        }
        return false;
    }

    /**
     * 判断用户名或密码字符串是否符合要求
     * @param str
     * @return
     */
    public static boolean isLegal(String str){

        return false;
    }

    /**
     * 根据,将邮件附件字符串分割成一个数组
     * @param mailAnnex
     * @return
     */
    public static String[] splitMailAnnexString(String mailAnnex){
        return mailAnnex.split("\\,");
    }

    /**
     * 验证收件人或抄送字符串 "dsfsfas@tmail.com,dfskk@164.com"
     * 并把不是邮箱地址的字符串移除，返回正确的收件人或抄送字符串
     * @param emailsString
     * @return
     */
    public static String checkEmailsString(String emailsString){
        String checkEmailsString = "";
        if(!emailsString.equals("")){
            String[] emailsStr = splitMailAnnexString(emailsString);
            for (String string:emailsStr) {
                if(checkEmail(string)){
                    checkEmailsString += ","+string;
                }
            }
            return checkEmailsString.substring(1);
        }else{
            return null;
        }
    }

    /**
     * 根据附件名，获取单个附件的类型
     * @param mailAnnexPath
     * @return
     */
    public static String getMailAnnexContentType(String mailAnnexPath){
        return mailAnnexPath.split("\\.")[1];
    }

    /**
     * 根据,将邮件附件字符串分割成一个数组
     * @param mailAnnexString
     * @return
     */
    public static List<String> getMailAnnex(String mailAnnexString){
        String[] string = splitMailAnnexString(mailAnnexString);
        List<String> stringList = new ArrayList<String>();
        for(String str:string){
            stringList.add(str);
        }
        return stringList;
    }

    /**
     * 获取附件绝对路径集合
     * @param mailAnnexString
     * @return
     */
    public static List<String> getMailAnnexTruePath(String mailAnnexString){
        List<String> strings = getMailAnnex(mailAnnexString);
        List<String> mailAnnexTruePath = new ArrayList<String>();
        for (String string:strings) {
            File file = new File(ParameterEnum.UPLOAD_IMAGE_BASE_PATH.getMsg()+string);
            if(file.exists()){
                System.out.println(file.getAbsolutePath());
                mailAnnexTruePath.add(file.getAbsolutePath());
            }else{
                return null;
            }
        }
        return mailAnnexTruePath;
    }

    /**
     * 附件的相对路径转绝对路径
     * @param path
     * @return
     */
    public static String annexPathToTruePath(String path){
        File file = new File(ParameterEnum.UPLOAD_IMAGE_BASE_PATH.getMsg()+path);
        if(file.exists()){
            return file.getAbsolutePath();
        }else{
            return "";
        }
    }

    /**
     * 根据附件名，获取单个附件的文件名
     * @param mailAnnexPath
     * @return
     */
    public static String getMailAnnexFileName(String mailAnnexPath){
        String[] string = mailAnnexPath.split("\\/");
        return string[string.length-1];
    }

    /**
     * 验证邮箱
     * @param email
     * @return
     */
    public static boolean checkEmail(String email){
        boolean flag = false;
        try{
            String check = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
            Pattern regex = Pattern.compile(check);
            Matcher matcher = regex.matcher(email);
            flag = matcher.matches();
        }catch(Exception e){
            flag = false;
        }
        return flag;
    }

    public static void main(String[] args) {
       /* int i = 100;
        while(i>0){
            System.out.println(StringUtil.isTmail("15361931618@163.com"));
            i--;
        }*/
//       StringUtil.getMailAnnexTruePath("/uploadImg/305.jpg,/uploadImg/1.jpg,/uploadImg/20150521173904571.xls");
        System.out.println(StringUtil.isTmail("fsdhfhsd@tmail.comdsfsd@165.com"));
        System.out.println(StringUtil.isTmail("fsdhfhsd@tmail.comdsfsd@165.com"));
        System.out.println(StringUtil.isTmail("fsdhfhsd@tmail.com;dsfsd@165.com"));
        System.out.println(StringUtil.isTmail("fsdhfhsd@tmail.com"));
    }
}

package tk.timaca.tmail.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import tk.timaca.tmail.bean.ShowapiResult;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dede on 2017/5/13.
 */
public class WordBreakerUtil {

    private final static String  SHOWAPI_APPID = "38098";
    private final static String  SHOWAPI_SIGN = "9c1a30c2ecf44e788a0c78e098efc5a0";

    /**
     * 发送分词请求，返回json字符串
     * @param text
     * @return
     * @throws Exception
     */
    private static String postParticiple(String text) throws Exception {
        URL u=new URL("http://route.showapi.com/269-1?showapi_appid="+SHOWAPI_APPID+"&precise=0&debug=0&text="+text+"&showapi_sign="+SHOWAPI_SIGN);
        InputStream in=u.openStream();
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        try {
            byte buf[]=new byte[1024];
            int read = 0;
            while ((read = in.read(buf)) > 0) {
                out.write(buf, 0, read);
            }
        }  finally {
            if (in != null) {
                in.close();
            }
        }
        byte b[]=out.toByteArray( );
//        System.out.println(new String(b,"utf-8"));
        return new String(b,"utf-8");
    }

    /**
     * 解析json获取关键字集合
     * @param str
     * @return
     */
    private static List<String> parseJson(String str){
        List<String> keyList = new ArrayList<String>();
        ObjectMapper mapper = new ObjectMapper(); //转换器
        try{
            if(!str.equals("")){
                ShowapiResult showapiResult = mapper.readValue(str,ShowapiResult.class);
                String key = showapiResult.getShowapi_res_body().get("list").toString();
                key = key.substring(1,key.length()-1);
                String[] keys = key.split("\\,");
                for (String string:keys) {
                    string = string.replaceAll(" ","");
                    keyList.add(string);
                }
            }else{
                keyList = null;
            }
        }catch (IOException E){
            E.printStackTrace();
        }finally {
            return keyList;
        }
    }

    /**
     * 根据文本获取关键字集合
     * @param text
     * @return
     * @throws Exception
     */
    public static List<String> getKeysList(String text) throws Exception{
        return parseJson(postParticiple(text));
    }

    /**
     * 返回根据分词结果处理过的文本
     * @param text
     * @return
     */
    public static String getHandledText(String text) throws Exception{
        List<String> keysList = getKeysList(text);
        String handledText = "";
        for (String string:keysList) {
            text = text.replaceAll(string," "+string+" ");
        }
        return text;
    }

    public static void main(String[] args) throws Exception{
        System.out.println( WordBreakerUtil.getHandledText("最好多多推荐一些对中国本土的一些互联网用词——比如说“卧槽，给力，牛逼，好六，六六六，水贴，爆吧，女票，男票，蓝孩子，孩纸”之类的俚语也能有很好的分词能力的中文分词API就好了。"));
        /*for (String string:KeysList) {
            System.out.println("'"+string+"'");
        }*/
    }
}

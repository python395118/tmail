package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.Inbox;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.bean.Mail;
import tk.timaca.tmail.mapper.InboxMapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dede on 2017/3/22.
 * 处理邮件的业务逻辑类
 */
@Service
public class InboxService {

    @Autowired
    private InboxMapper inboxMapper;

    public String ParseMessage(byte[] message){
        return message.toString();
    }

    /**
     * 返回data包含一个邮件对象的JsonResult
     * @return
     */
    public JsonResult SelectOne(){
        return new JsonResult(inboxMapper.selectPage(new Page<Inbox>(1,2),new EntityWrapper<Inbox>().eq("repository_name","test2")));
    }

    /**
     * 解析message
     */
    public void ParseMessageBody(){
        List<Inbox> inboxes = inboxMapper.selectPage(new Page<Inbox>(1,2),new EntityWrapper<Inbox>().eq("repository_name","test2").orderBy("last_updated",true));
        String messagebody="";
        if(inboxes.size() > 0){
            messagebody = inboxes.get(0).getMessageBody();
        }
        System.out.println(messagebody);
    }

    public JsonResult findInboxByToken(String token){
        return new JsonResult();
    }
}

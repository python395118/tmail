package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.HttpLog;
import tk.timaca.tmail.bean.Token;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.mapper.HttpLogMapper;
import tk.timaca.tmail.mapper.RoleMapper;
import tk.timaca.tmail.mapper.TokenMapper;
import tk.timaca.tmail.mapper.UserInformationMapper;
import tk.timaca.tmail.util.AuthUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dede on 2017/3/29.
 * token业务逻辑类
 */
@Service
public class TokenService {

    @Autowired
    private TokenMapper tokenMapper;

    @Autowired
    private UserInformationMapper userInformationMapper;

    @Autowired
    private HttpLogMapper httpLogMapper;

    /**
     * 检测令牌是否有效
     * @param token
     * @return
     */
    public boolean checkToken(String token,String username){
        /**
         * 若token 为 “token参数不存在"
         * 先判断username  是否为 “username参数不存在 ” 是则直接返回false
         * 若username 角色为 游客 返回false
         * 若username 角色为 用户 或 管理员 并且token存在 返回true
         */
        List<UserInformation> uis = new ArrayList<UserInformation>();
        Long roleId;
        List<Token> tokens =new ArrayList<Token>();
        if(username.equals(ParameterEnum.NO_USERNAME.toString())){
            return false;
        }else{
            uis = userInformationMapper.selectList(new EntityWrapper<UserInformation>().eq("username",username));
            if(uis.size() > 0){
                roleId = uis.get(0).getRoleId();
                if(roleId == 0){
                    return false;
                }else{
                    tokens=tokenMapper.selectList(new EntityWrapper<Token>().eq("Token",token).eq("state",0).eq("username",username));
                }
            }
        }

        return tokens.size()>0?true:false;
    }

    /**
     * 根据username生成新的token，并更新过期的token
     * @param username
     * @return
     */
    public Token NewToken(String username){
        Token newToken=AuthUtil.NewToken(username, AuthUtil.CreateVerificationCode(),System.currentTimeMillis());
//        tokenMapper.delete(new EntityWrapper<Token>().eq("username",username));
        tokenMapper.update(new Token(1),new EntityWrapper<Token>().eq("username",username));
        tokenMapper.insert(newToken);
        return newToken;
    }

    /**
     * 根据token生成新的token，并更新过期的token
     * @param newToken
     * @return
     */
    public Token NewToken(Token newToken){
//        tokenMapper.delete(new EntityWrapper<Token>().eq("username",newToken.getUsername()));
        tokenMapper.update(new Token(1),new EntityWrapper<Token>().eq("username",newToken.getUsername()));
        tokenMapper.insert(newToken);
        return newToken;
    }

    /**
     * 更新token状态为过期
     * @param username
     * @return
     */
    public boolean UpdateToken(String username){
        int result = tokenMapper.update(new Token(1),new EntityWrapper<Token>().eq("username",username));
        return result > 1 ? true :false ;
    }

    /**
     * 根据token查找username
     * @param token
     * @return
     */
    public String findNameByToken(String token){
        String username = "";
        List<Token> tokens = tokenMapper.selectList(new EntityWrapper<Token>().eq("token",token));
        System.out.println(tokens.get(0).toString());
        if(tokens.size() > 0){
            username = tokens.get(0).getUsername();
//            System.out.println("findNameByToken"+username);
        }else{}
        return username;
    }

    /**
     * 验证ip能否发送邮件
     * @param ip
     * @return
     */
    public boolean checkIPCanSend(String ip){
        /*开始统计时间*/
        Long timestamp = System.currentTimeMillis()-Long.parseLong(ParameterEnum.CHECK_IP_PERIOD.getMsg());
        /*统计一段时间内ip发送邮件的次数*/
        Integer count = httpLogMapper.selectCount(new EntityWrapper<HttpLog>()
                .eq("ip",ip).and("timestamp > "+timestamp+" and url like '%send%'"));
        if(count >= Integer.valueOf(ParameterEnum.CHECK_IP_SEND_MAX_TIMES.getMsg())){
            /*当次数大于最大值时，返回false*/
            return false;
        }
        return true;
    }

    /**
     * 验证ip是否能进行登录注册操作
     * @param ip
     * @return
     */
    public boolean checkIpCanLoginOrReg(String ip){
        /*开始统计时间*/
        Long timestamp = System.currentTimeMillis()-Long.parseLong(ParameterEnum.CHECK_IP_PERIOD.getMsg());
        /*统计一段时间内ip发送邮件的次数*/
        Integer count = httpLogMapper.selectCount(new EntityWrapper<HttpLog>()
                .eq("ip",ip).and("timestamp > "+timestamp+" and (url like '%login%' or url like '%reg%')"));
        if(count >= Integer.valueOf(ParameterEnum.CHECH_IP_LOGIN_OR_REG_MAX_TIMES.getMsg())){
            /*当次数大于最大值时，返回false*/
            return false;
        }
        return true;
    }
}

package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.bean.Mail;
import tk.timaca.tmail.bean.MailAnnex;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.enums.MailTypeEnum;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.mapper.MailAnnexMapper;
import tk.timaca.tmail.mapper.MailMapper;
import tk.timaca.tmail.util.AuthUtil;
import tk.timaca.tmail.util.SendEmailUtil;
import tk.timaca.tmail.util.StringUtil;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Created by dede on 2017/3/21.
 * 通过javamail收发邮件核心类
 */
@Service
public class MailService {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private MailMapper mailMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private MailAnnexMapper mailAnnexMapper;

    /**
     * 同步发送邮件
     * @param mail
     * @return
     */
    public JsonResult SendMail(Mail mail) {
        String host = "localhost"; // 本机smtp服务器
        //String host = "smtp.163.com"; // 163的smtp服务器
//       String from = "test1@tmail.com"; // 邮件发送人的邮件地址
        String from = StringUtil.isTmail(mail.getFromMail());
//        String to = "test2@tmail.com"; // 邮件接收人的邮件地址
        String to = StringUtil.isTmail(mail.getToMail());
//        String to = "758672830@qq.com"; // 邮件接收人的邮件地址
//        final String username = "test1";  //发件人的邮件帐户
//        final String password = "test1";   //发件人的邮件密码
        String username = mail.getFromMail();

        /*获取aes加密后的密码*/
        String pwd = userService.findUserInformationByUsername(mail.getFromMail()).getPassword();
        /*aes解密*/
        String password = AuthUtil.AESDncode(username,pwd);

        /*保存邮件*/
        mail.setType(MailTypeEnum.SEND.getType());
        mail.setFlagRead(1);
        saveMail(mail);

        // 创建Properties 对象
        Properties props = System.getProperties();

        // 添加smtp服务器属性
        props.put("Mail.smtp.host", host);
        props.put("Mail.smtp.auth", "true");

        // 创建邮件会话
//        Session session = Session.getDefaultInstance(props, new Authenticator(){
          Session session = Session.getInstance(props, new Authenticator(){
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }

        });

        try {
            // 定义邮件信息
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                    to));
            //message.setSubject(transferChinese("我有自己的邮件服务器了"));
//            message.setSubject("I hava my own Mail server");
//            message.setText("From now, you have your own Mail server, congratulation!");
            message.setSubject(mail.getSubject(),"utf-8");
            message.setText(mail.getContent(),"utf-8");

            // 发送消息
            session.getTransport("smtp").send(message);
            //Transport.send(message); //也可以这样创建Transport对象发送
            System.out.println("SendMail Process Over!");
            return new JsonResult(ResultEnum.SEND_SUCCESS);

        } catch (MessagingException e) {
            e.printStackTrace();
        }

        return new JsonResult(ResultEnum.SEND_FAIL);
    }

    public static String toChinese(String strvalue){
        try{
            if(strvalue==null)
                return null;
            else{
                strvalue = new String(strvalue.getBytes("ISO8859_1"), "GBK");
                return strvalue;
            }
        }catch(Exception e){
            return null;
        }
    }

    /**
     * 使用Javamail实现邮件接收功能
     * @param username
     * @param password
     * @return
     */
    public List<Mail> getMailByName(String username,String password) throws MessagingException,IOException{
        //用于接收查询到的邮件集合
        List<Mail> mails = new ArrayList<Mail>();

        // 初始化主机
        String host = "localhost";
        Properties props = new Properties();
        props.put("mail.smtp.host", "localhost"); // smtp服务器
        props.put("mail.smtp.auth", "true"); // 是否smtp认证
        props.put("mail.smtp.port", "25"); // 设置smtp端口
        props.put("mail.transport.protocol", "smtp"); // 发邮件协议
        props.put("mail.store.protocol", "pop3"); // 收邮件协议
        // 获取会话
        Session session = Session.getDefaultInstance(props, null);
        // 获取Store对象，使用POP3协议，也可能使用IMAP协议
        Store store = session.getStore("pop3");
        // 连接到邮件服务器
        store.connect(host, username, password);
        // 获取该用户Folder对象，并以只读方式打开
        Folder folder = store.getFolder("inbox");
        folder.open(Folder.READ_ONLY);
        // 检索所有邮件，按需填充
        Message message[] =folder.getMessages();
        for (int i = 0; i < message.length; i++) {
            // 打印出每个邮件的发件人和主题
            System.out.println(i + ":" + message[i].getFrom()[0] + "\t"
                    + message[i].getSubject()+":"+message[i].getContentType());

            Object content = message[i].getContent();
            mails.add(new Mail(StringUtil.randomLong(),username,message[i].getFrom()[0].toString(),message[i].getSubject(),"","",
                    message[i].getContent().toString(),System.currentTimeMillis(),0,2,userService.findUserInformationByUsername(username).getUiId()));
        }
        folder.close(false);
        store.close();

        return mails;
    }

    /**
     * 根据token和邮件类型查找从mail表中查询邮件集合
     * @param token
     * @param type 邮件类型
     * @param whichPage 第几页
     * @param max 每页最多多少条
     * @return
     * @throws Exception
     */
    public JsonResult findMailByTokenFromMail(String token,Integer type,Integer whichPage,Integer max,String listType) throws Exception{
        String username = tokenService.findNameByToken(token);
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        JsonResult jsonResult = new JsonResult();
        if(!username.equals("")){
           /* *//*获取已发送邮件集合*//*
            List<Mail> mailsFrom = mailMapper.selectList(new EntityWrapper<Mail>().eq("from_mail", username).orderBy("timestamp",true));*/
            /*获取收件箱邮件集合*/
//            List<Mail> mailsTo = mailMapper.selectList(new EntityWrapper<Mail>().eq("to_mail", username).orderBy("timestamp",true));
            /*获取草稿件或已发送邮件集合*/
//            List<Mail> mailDrafts = mailMapper.selectList(new EntityWrapper<Mail>().eq("from_mail", username).eq("type",type).orderBy("timestamp",true));
            List<Mail> mails = new ArrayList<Mail>();
            if(StringUtil.isMailTypeEnum(type)){
                /*设置根据那个参数排序*/
                String orderBy = "timestamp";
                if(listType.equals(ParameterEnum.INBOX_LIST_BY_TIME.getMsg())){
                    orderBy = "timestamp";
                }else if(listType.equals(ParameterEnum.INBOX_LIST_BY_SUBJECT.getMsg())){
                    orderBy = "subject";
                }else if(listType.equals(ParameterEnum.INBOX_LIST_BY_FROM.getMsg())){
                    orderBy = "from_mail";
                }else{}
                System.out.println(orderBy);
                mails.addAll(mailMapper.selectPage(
                        new Page<Mail>(whichPage,max),
                        new EntityWrapper<Mail>()
                                .eq("user_id",userInformation.getUiId())
                                .eq("type",type).orderBy(orderBy,false)));
                jsonResult.setData(mails);
                jsonResult.setCode(ResultEnum.GET_MAILS_SUCCESS.getCode());
            }/*else if(type == 1||type ==0){
                mails.addAll(mailDrafts);
                jsonResult.setData(mails);
                jsonResult.setCode(ResultEnum.GET_MAILS_SUCCESS.getCode());
            }*/else{
                jsonResult.setCode(ResultEnum.GET_MAILS_FAIL.getCode());
            }

        }else{
            jsonResult.setCode(ResultEnum.GET_MAILS_FAIL.getCode());
        }

        return jsonResult;
    }

    /**
     * 根据token type统计邮件数目
     * @param token
     * @param type
     * @return
     */
    public JsonResult countMailByTokenFromMail(String token,Integer type){
        String username = tokenService.findNameByToken(token);
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        JsonResult jsonResult = new JsonResult();
        Map<String,Object> data = new HashMap<String, Object>();
        int count = -1;
        if(!username.equals("")){
            if(StringUtil.isMailTypeEnum(type)){
                count = mailMapper.selectCount(new EntityWrapper<Mail>()
                                .eq("user_id",userInformation.getUiId())
                                .eq("type",type).orderBy("timestamp",true));
                data.put("count",count);
                jsonResult.setData(data);
                jsonResult.setCode(ResultEnum.GET_MAILS_SUCCESS.getCode());
            }else{
                jsonResult.setCode(ResultEnum.GET_MAILS_FAIL.getCode());
            }

        }else{
            jsonResult.setCode(ResultEnum.GET_MAILS_FAIL.getCode());
        }

        return jsonResult;
    }

    /**
     * 根据token查找从inbox表中查询邮件集合
     * @param token
     * @return
     * @throws Exception
     */
    public JsonResult findMailByTokenFromInbox(String token) throws Exception{
        JsonResult jsonResult = new JsonResult();
        String username = tokenService.findNameByToken(token);
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        String password = "";
        List<Mail> mails = new ArrayList<Mail>();
        if(userInformation != null){
            password = AuthUtil.AESDncode(username,userInformation.getPassword());
            if(username != ""&&password != ""){
                mails = getMailByName(username,password);
                jsonResult.setData(mails);
                jsonResult.setCode(ResultEnum.GET_MAILS_SUCCESS.getCode());
            }else{
                jsonResult.setCode(ResultEnum.GET_MAILS_FAIL.getCode());
            }
        }else{
            jsonResult.setCode(ResultEnum.GET_MAILS_FAIL.getCode());
        }
        return jsonResult;
    }

    /**
     * 根据username查找从inbox表中查询邮件集合
     * @param username
     * @return
     * @throws Exception
     */
    public List<Mail> findMailByUsernameFromInbox(String username) throws Exception{
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        String password = "";
        List<Mail> mails = new ArrayList<Mail>();
        if(userInformation != null){
            password = AuthUtil.AESDncode(username,userInformation.getPassword());
            if(username != ""&&password != ""){
                mails = getMailByName(username,password);
            }else{
                mails = null;
            }
        }else{
            mails = null;
        }
        return mails;
    }

    /**
     * 保存邮件
     * @param mail
     * @return
     */
    public boolean saveMail(Mail mail){
        if(mail.getMailId() == null){
            mail.setMailId(StringUtil.randomLong());
        }
        if(mail.getUserId() == null){
            mail.setUserId(userService.findUserInformationByUsername(mail.getFromMail()).getUiId());
        }
        if(mail.getTimestamp() ==null){
            mail.setTimestamp(System.currentTimeMillis());
        }
        return mailMapper.insert(mail) > 0 ? true : false ;
    }

    /**
     * 根据mailId在mail中查找邮件
     * @param mailId
     * @return
     */
    public JsonResult findMailByMailId(Long mailId){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setData(mailMapper.selectById(mailId));
        jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        return jsonResult;
    }

    /**
     * 根据token获取邮箱信息
     * @param token
     * @return
     */
    public JsonResult getInboxInfo(String token){
        JsonResult jsonResult = new JsonResult();
        Map<String,Object> data = new HashMap<String, Object>();

        String username = tokenService.findNameByToken(token);
        if(username!=""){
            UserInformation userInformation = userService.findUserInformationByUsername(username);
            /*获取草稿箱集合*/
            List<Mail> draftsMails = mailMapper.selectList(new EntityWrapper<Mail>().eq("user_id",userInformation.getUiId()).
                    eq("type", MailTypeEnum.DRAFTS.getType()).eq("flag_read",0));
            /*获取收到的邮件集合*/
            List<Mail> incomingMails = mailMapper.selectList(new EntityWrapper<Mail>().eq("user_id",userInformation.getUiId()).
                    eq("type", MailTypeEnum.INCOMING_MAIL.getType()).eq("flag_read",0));
            /*获取已发送的邮件集合*/
            List<Mail> sendMails = mailMapper.selectList(new EntityWrapper<Mail>().eq("user_id",userInformation.getUiId()).
                    eq("type", MailTypeEnum.SEND.getType()).eq("flag_read",0));
            data.put("drafts",draftsMails.size());
            data.put("incoming",incomingMails.size());
            data.put("send",sendMails.size());
        }else{}
            jsonResult.setData(data);
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());

        return jsonResult;
    }

    /**
     * 根据mailId设置邮件为已读
     * @param mailId
     * @return
     */
    public JsonResult setRead(Long mailId){
        JsonResult jsonResult = new JsonResult();
        Mail mail = mailMapper.selectById(mailId);
        int data = 0;
        if(mail != null){
            mail.setFlagRead(1);
            data = mailMapper.updateById(mail);
        }else{}
        jsonResult.setCode(data > 0 ? ResultEnum.SUCCESS.getCode():ResultEnum.ERROR.getCode());
        return jsonResult;
    }

    /**
     * 根据关键字在用户邮箱检索邮件
     * @param key
     * @param username
     * @param whichPage
     * @param max
     * @return
     */
    public JsonResult searchMailByKey(String key,String username,Integer whichPage,Integer max,String listType){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        List<Mail> mails = new ArrayList<Mail>();
        if(!username.equals(ParameterEnum.NO_PARAMS.getMsg()) && !key.equals(ParameterEnum.NO_PARAMS.getMsg())
                && !whichPage.equals(Integer.valueOf(ParameterEnum.ERROR.getMsg()))
                && !max.equals(Integer.valueOf(ParameterEnum.ERROR.getMsg()))){

            if(userInformation != null){
                /*设置根据那个参数排序*/
                String orderBy = "timestamp";
                if(listType.equals(ParameterEnum.INBOX_LIST_BY_TIME.getMsg())){
                    orderBy = "timestamp";
                }else if(listType.equals(ParameterEnum.INBOX_LIST_BY_SUBJECT.getMsg())){
                    orderBy = "subject";
                }else if(listType.equals(ParameterEnum.INBOX_LIST_BY_FROM.getMsg())){
                    orderBy = "from_mail";
                }else{
                }
                System.out.println(orderBy);
                mails = mailMapper.selectPage(new Page<Mail>(whichPage,max),new EntityWrapper<Mail>()
                        .where("user_id = "+userInformation.getUiId()+" and ( subject like '%"+ key+"%' or content like '%"+ key+"%' )").orderBy(orderBy,false));
            }else{}
            jsonResult.setData(mails);
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }

        return jsonResult;
    }

    /**
     * 根据key、username统计匹配的邮件个数
     * @param key
     * @param username
     * @return
     */
    public JsonResult countMailByKey(String key,String username){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        Map<String,Object> data = new HashMap<String, Object>();
        Integer count = -1;
        if(!username.equals(ParameterEnum.NO_PARAMS.getMsg()) && !key.equals(ParameterEnum.NO_PARAMS.getMsg())){

            if(userInformation != null){
                count = mailMapper.selectCount(new EntityWrapper<Mail>()
                        .eq("user_id",userInformation.getUiId()).and("subject like '%"+ key+"%' or content like '%"+ key+"%'"));
            }else{}
            data.put("count",count);
            jsonResult.setData(data);
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }

        return jsonResult;
    }

    /**
     * 根据mailId将邮件彻底删除
     * @param mailId
     * @return
     */
    public JsonResult removeMail(Long mailId){
        JsonResult jsonResult = new JsonResult();
        int result = mailMapper.deleteById(mailId);
        jsonResult.setCode(result > 0 ?
                ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        return jsonResult;
    }

    /**
     * 根据mailId将邮件移除到垃圾箱
     * @param mailId
     * @param afterType
     * @return
     */
    public JsonResult removeMail(Long mailId,Integer afterType){
        JsonResult jsonResult = new JsonResult();
        Mail mail = mailMapper.selectById(mailId);
        /*判断mailId是否合法*/
        if(mail != null){
            if(mail.getType()!=MailTypeEnum.TRASH_MAIL.getType()){
                mail.setType(MailTypeEnum.TRASH_MAIL.getType());
                mailMapper.updateById(mail);
                jsonResult.setCode(ResultEnum.SUCCESS.getCode());
            }else{
                /*若邮件已经存在回收站，则彻底删除邮件*/
               return removeMail(mailId);
            }
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 批量将username的beforeType类型的邮件更新为afterType类型的邮件
     * @param beforeType
     * @param afterType
     * @param username
     * @return
     */
    public JsonResult removeMail(Integer beforeType,Integer afterType,String username){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        /*判断beforeType和afterType是否包含在MailTypeEnum中以及username是否存在*/
        if(StringUtil.isMailTypeEnum(beforeType) && StringUtil.isMailTypeEnum(afterType) && userInformation != null ){
            Mail newMail =  new Mail();
            newMail.setType(afterType);
            mailMapper.update(newMail,new EntityWrapper<Mail>().eq("type",beforeType).eq("user_id",userInformation.getUiId()));
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /*修改草稿件*/
    public JsonResult updateMail(Mail mail,String mailAnnex){
        /*判断邮件类型是否是草稿件并且邮件已经存在*/
        if(mail.getType() == MailTypeEnum.DRAFTS.getType() && mailMapper.selectById(mail.getMailId()) != null){
            if(updateMailAnnex(mail.getMailId(),mailAnnex) && mailMapper.updateById(mail)>0){
                return new JsonResult(ResultEnum.SUCCESS);
            }
        }
        return new JsonResult(ResultEnum.ERROR);
    }

    /**
     * 批量将用户名为username的type类型邮件设置为可读
     * @param username
     * @param type
     * @return
     */
    public JsonResult setReadByType(String username,Integer type){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null && StringUtil.isMailTypeEnum(type)){
            int result = -1;
            Mail mail = new Mail();
            mail.setFlagRead(1);
            result = mailMapper.update(mail,new EntityWrapper<Mail>()
                    .eq("user_id",userInformation.getUiId()).eq("type",type));
            jsonResult.setCode(result > 0 ? ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        }else {
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    public static void main(String[] args) throws UnsupportedEncodingException,MessagingException{
     /*   // 初始化主机
        String host = "localhost";
        String username = "test1";
        String password = "test1";
        Properties props = new Properties();
        props.put("mail.smtp.host", "localhost"); // smtp服务器
        props.put("mail.smtp.auth", "true"); // 是否smtp认证
        props.put("mail.smtp.port", "25"); // 设置smtp端口
        props.put("mail.transport.protocol", "smtp"); // 发邮件协议
        props.put("mail.store.protocol", "pop3"); // 收邮件协议
        // 获取会话
        Session session = Session.getDefaultInstance(props, null);
        // 获取Store对象，使用POP3协议，也可能使用IMAP协议
        Store store = session.getStore("pop3");
        // 连接到邮件服务器
        store.connect(host, username, password);
        // 获取该用户Folder对象，并以只读方式打开
        Folder folder = store.getFolder("inbox");
        folder.open(Folder.READ_ONLY);
        // 检索所有邮件，按需填充
        Message message[] =folder.getMessages();
        for (int i = 0; i < message.length; i++) {
            // 打印出每个邮件的发件人和主题
            System.out.println(i + ":" + message[i].getFrom()[0] + "\t"
                    + message[i].getSubject()+":"+message[i].getContent());
        }
        folder.close(false);
        store.close();*/
     /*List<Mail> mails = new ArrayList<Mail>();
         try{
            mails = new MailService().getMailByName("test1","test1");
        }catch (IOException E){


         }
        for (Mail mail: mails) {
            System.out.println(mail.toString());
        }*/
//        System.out.println(new MailService().searchMailByKey("1","test1",0,10).getData().toString());
    }

    /**
     * 发送可带附件、抄送、密送的邮件
     * @param username
     * @param to
     * @param cc
     * @param bcc
     * @param subject
     * @param body
     * @param mailAnnex
     * @return
     * @throws Exception
     */
    public JsonResult sendEmail(String username,String to,String cc,String bcc,String subject,String body,String mailAnnex) throws Exception{
        JsonResult jsonResult = new JsonResult();
//        String userName = "abc"; // 发件人邮箱
//        String password = "abc"; // 发件人密码
//        String smtpHost = "localhost"; // 邮件服务器
        String smtpHost = "123.207.246.38";
//
//        String to = "test1@tmail.com,test2@tmail.com,test3@tmail.com,test4@tmail.com,test5@tmail.com"; // 收件人，多个收件人以半角逗号分隔
//        String cc = "abc@tmail.com,test1@tmail.com"; // 抄送，多个抄送以半角逗号分隔
//        String subject = "这是邮件的主题"; // 主题
//        String body = "这是邮件的正文"; // 正文，可以用html格式的哟
//        List<String> attachments = Arrays.asList("C:\\Users\\dede\\Pictures\\树人格鲁特\\snapshot20150521212513.jpg", "C:\\Users\\dede\\Pictures\\树人格鲁特\\snapshot20150521212518.jpg"); // 附件的路径，多个附件也不怕

        UserInformation userInformation = userService.findUserInformationByUsername(username);
        String password = AuthUtil.AESDncode(username,userInformation.getPassword());
        /*List<String> attachments = null;
        if(mailAnnexes != null && mailAnnexes.size() > 0){
            for (MailAnnex mailAnnex : mailAnnexes) {
                attachments.add(mailAnnex.getPath());
            }
        }*/
        if(userInformation != null && !to.equals(ParameterEnum.ERROR.getMsg())
                && !cc.equals(ParameterEnum.ERROR.getMsg()) && !bcc.equals(ParameterEnum.ERROR.getMsg()) && !subject.equals(ParameterEnum.ERROR.getMsg())
                && !body.equals(ParameterEnum.ERROR.getMsg()) && !mailAnnex.equals(ParameterEnum.ERROR.getMsg())){
            List<String> attachments = null;
            if(mailAnnex.equals("")){
                attachments = null;
            }else{
                attachments = StringUtil.getMailAnnexTruePath(mailAnnex);
            }
            SendEmailUtil email = SendEmailUtil.entity(smtpHost, username, password, to, cc, bcc,subject, body, attachments);
            /*保存邮件*/
            Mail mail = new Mail();
            mail.setUserId(userInformation.getUiId());
            mail.setMailId(StringUtil.randomLong());
            mail.setWcc(cc);
            mail.setBcc(bcc);
            mail.setTimestamp(System.currentTimeMillis());
            mail.setSubject(subject);
            mail.setFromMail(StringUtil.isTmail(username));
            mail.setContent(body);
            mail.setToMail(to);
            mail.setType(MailTypeEnum.SEND.getType());
            mail.setFlagRead(1);

            if(saveMailAnnex(mailAnnex,mail.getMailId()) && saveMail(mail)){
                email.send(); // 发送！
                return new JsonResult(ResultEnum.SEND_SUCCESS);
            }else{
                return new JsonResult(ResultEnum.SEND_FAIL);
            }
        }
        return new JsonResult(ResultEnum.SEND_FAIL);
    }

    /**
     * 保存附件信息
     * @param mailAnnexString
     * @param mailId
     * @return
     */
    public boolean saveMailAnnex(String mailAnnexString,Long mailId){
        if(!mailAnnexString.equals("")){
            String[] mailAnnexs = StringUtil.splitMailAnnexString(mailAnnexString);
            List<MailAnnex> mailAnnexList = new ArrayList<MailAnnex>();
            for (String string : mailAnnexs) {
                MailAnnex mailAnnex = new MailAnnex(StringUtil.randomLong(),StringUtil.annexPathToTruePath(string),StringUtil.getMailAnnexFileName(string),StringUtil.getMailAnnexContentType(string),mailId);
                if(mailAnnexMapper.insert(mailAnnex) > 0){
                    mailAnnexList.add(mailAnnex);
                }else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * 根据mailId获取附件信息
     * @param mailId
     * @return
     */
    public JsonResult getMailAnnexByMailId(Long mailId){
        JsonResult jsonResult = new JsonResult();
        Map<String,Object> data = new HashMap<String, Object>();
        List<MailAnnex> mailAnnexList = mailAnnexMapper.selectList(new EntityWrapper<MailAnnex>().eq("mail_id",mailId));
        List<Object> annexList = new ArrayList<Object>();
        if(mailAnnexList!=null){
            for (MailAnnex mailAnnex:mailAnnexList) {
                File file = new File(mailAnnex.getPath());
                if(file.exists()){
                    Map<String,Object> annex = new HashMap<String, Object>();
                    annex.put("name",file.getName());
                    annex.put("size",file.length());
                    annex.put("type",mailAnnex.getContentType());
                    annex.put("file",mailAnnex.getFileName());
                    annex.put("path",mailAnnex.getPath());
                    annexList.add(annex);
                }else{
                    jsonResult.setData("找不到文件");
                    jsonResult.setCode(ResultEnum.ERROR.getCode());
                }
            }
            data.put("files",annexList);
            jsonResult.setData(data);
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }
        return jsonResult;
    }

    /**
     * 保存草稿件
     * @param mail
     * @param mailAnnex
     * @return
     */
    public JsonResult saveDraftsMail(Mail mail,String mailAnnex){
        if(mail.getMailId()==null){
            Long mailId = StringUtil.randomLong();
            mail.setMailId(mailId);
            if(saveMail(mail) && saveMailAnnex(mailAnnex,mailId)){
                return new JsonResult(ResultEnum.SUCCESS);
            }
        }
        return new JsonResult(ResultEnum.ERROR);
    }

    /**
     * 更新邮件附件
     * @param mailId
     * @param mailAnnex
     * @return
     */
    public boolean updateMailAnnex(Long mailId,String mailAnnex){
        Mail mail = mailMapper.selectById(mailId);
        if( mail != null && mail.getType() == MailTypeEnum.DRAFTS.getType()){
            mailAnnexMapper.delete(new EntityWrapper<MailAnnex>().eq("mail_id",mailId));
            if(saveMailAnnex(mailAnnex,mailId)){
                return true;
            }
        }
        return false;
    }
}

package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.bean.Token;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.bean.Users;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.mapper.UserInformationMapper;
import tk.timaca.tmail.mapper.UsersMapper;
import tk.timaca.tmail.util.AuthUtil;
import tk.timaca.tmail.util.DigestUtil;
import tk.timaca.tmail.util.FontImageUtil;
import tk.timaca.tmail.util.StringUtil;

import java.io.File;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dede on 2017/3/19.
 * 用户业务逻辑实现类
 */
@Service
public class UserService {

    @Autowired
    private UserInformationMapper uimapper;

    @Autowired
    private UsersMapper usersMapper;

    @Autowired
    private TokenService tokenService;

    /**
     * 匹配登录信息
     * @param username
     * @param password
     * @return
     */
    public JsonResult isLogin(String username, String password) throws Exception {
        Map<String,Object> data = new HashMap<String, Object>();
        //根据账号查找对应的密码
        Users user = findUserByUsername(username);
//        System.out.println(user.getPwdAlgorithm()+" "+password+" "+DigestUtil.digestString(password,user.getPwdAlgorithm()));
        if(user != null){
             if( user.getPwdHash().equals(DigestUtil.digestString(password,user.getPwdAlgorithm()))){
                 //密码匹配则返回true
                 Token newToken = AuthUtil.NewToken(
                         username,AuthUtil.CreateVerificationCode(),System.currentTimeMillis());
                 data.put("token",newToken.getToken());
                 tokenService.NewToken(newToken);
                 /**
                  * 根据username查询UserInformation，并将role_id设为用户
                  */
                 UserInformation ui = findUserInformationByUsername(username);
                 if(ui == null){
                     Long timestamp = System.currentTimeMillis();
                      uimapper.insert(new UserInformation(StringUtil.randomLong(),
                             username, username, timestamp, timestamp,
                              0,1l,AuthUtil.AESEncode(username,password)));
                 }else{
                     ui.setRoleId(1L);
                     updateRoleId(ui);
                 }

                 return  new JsonResult(ResultEnum.LOGIN_SUCCESS.getCode(),data);
             }else{
                 return new JsonResult(ResultEnum.LOGIN_FAIL.getCode(),ResultEnum.LOGIN_FAIL.getMsg());
             }
        }else {

            return new JsonResult(ResultEnum.LOGIN_FAIL.getCode(),ResultEnum.LOGIN_FAIL.getMsg());
        }
    }


    /**
     * 创建新用户
     * @param username
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public JsonResult createUser(String username, String password) throws Exception{
        //如果用户名不存在则创建用户
        long timestamp = System.currentTimeMillis();
        int result=0;
        if(findUserByUsername(username) == null) {
            result=usersMapper.insert(new Users(username, DigestUtil.digestString(password, "SHA"), "SHA", 0, 0));
        }

        if(findUserInformationByUsername(username) == null) {
            if (result > 0) {
                result = uimapper.insert(new UserInformation(StringUtil.randomLong(),
                        username, username, timestamp, timestamp,0,0l,AuthUtil.AESEncode(username,password),createAvatarByUsername(username)));
            }
        }
        //result大于0则注册成功
        return result > 0 ?
                new JsonResult(ResultEnum.REGISTER_SUCCESS.getCode(),ResultEnum.REGISTER_SUCCESS.getMsg()):
                new JsonResult(ResultEnum.REGISTER_FAIL.getCode(),ResultEnum.REGISTER_FAIL.getMsg());
    }

    /**
     * 创建没有密码的用户
     * @param username
     * @return
     * @throws Exception
     */
    public JsonResult createUserNoPwd(String username) throws Exception{
        //如果用户名不存在则创建用户
        long timestamp = System.currentTimeMillis();
        int result=0;

        if(findUserInformationByUsername(username) == null) {
                result = uimapper.insert(new UserInformation(StringUtil.randomLong(),
                        username, username, timestamp, timestamp,0,0l,null,createAvatarByUsername(username)));
        }
        //result大于0则注册成功
        return result > 0 ?
                new JsonResult(ResultEnum.REGISTER_SUCCESS.getCode(),ResultEnum.REGISTER_SUCCESS.getMsg()):
                new JsonResult(ResultEnum.REGISTER_FAIL.getCode(),ResultEnum.REGISTER_FAIL.getMsg());
    }

    /**
     * 更新密码
     * @param account
     * @param oldPasseord
     * @param newPassword
     * @return
     * @throws Exception
     */
    public JsonResult UpdatePassword(String account,String oldPasseord,String newPassword) throws Exception {
        Users oneUser = findUserByUsername(account);
        UserInformation userInformation = findUserInformationByUsername(account);
        int result=-1;
        if(oneUser != null && userInformation != null){
            if( DigestUtil.digestString(oldPasseord,oneUser.getPwdAlgorithm()).equals(oneUser.getPwdHash())
                    && AuthUtil.AESDncode(account,userInformation.getPassword()).equals(oldPasseord)){
                oneUser.setPwdHash(DigestUtil.digestString(newPassword,oneUser.getPwdAlgorithm()));
                result = usersMapper.updateById(oneUser);
                userInformation.setPassword(AuthUtil.AESEncode(account,newPassword));
                result = uimapper.updateById(userInformation);
            }
        }
//        System.out.println(result);
        //result大于0则密码修改成功
        return result > 0 ?
                new JsonResult(ResultEnum.UPDATE_PASSWORD_SUCCESS.getCode(),ResultEnum.UPDATE_PASSWORD_SUCCESS.getMsg()):
                new JsonResult(ResultEnum.UPDATE_PASSWORD_FAIL.getCode(),ResultEnum.UPDATE_PASSWORD_FAIL.getMsg());
    }

    /**
     * 根据用户名退出登录
     * @param username
     * @return
     */
    public JsonResult logout(String username){
        UserInformation userInformation = findUserInformationByUsername(username);
        int result = -1;
        if(userInformation != null){
            userInformation.setKeepAlive(0);
            userInformation.setRoleId(0L);
            result = uimapper.updateById(userInformation);
            tokenService.UpdateToken(username);
        }
        return result > 0 ?
                new JsonResult(ResultEnum.LOGOUT_SUCCESS.getCode(),ResultEnum.LOGOUT_SUCCESS.getMsg()):
                new JsonResult(ResultEnum.LOGOUT_FAIL.getCode(),ResultEnum.LOGOUT_FAIL.getMsg());
    }

    /**
     * 根据用户名删除用户
     * @param username
     * @return
     */
    public JsonResult deleteUser(String username){
        int result = -1;
        result = usersMapper.delete(new EntityWrapper<Users>().eq("username",username));
        if(result > 0){
            result = uimapper.delete(new EntityWrapper<UserInformation>().eq("username",username));
        }
        return result > 0 ?
                new JsonResult(ResultEnum.DELETE_SUCCESS.getCode(),ResultEnum.DELETE_SUCCESS.getMsg()):
                new JsonResult(ResultEnum.DELETE_FAIL.getCode(),ResultEnum.DELETE_FAIL.getMsg());
    }

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public Users findUserByUsername(String username){
        List<Users> userss=usersMapper.selectList(new EntityWrapper<Users>().eq("username",username));
        return userss.size()>0?userss.get(0):null;
    }

    /**
     * 根据用户名查找用户
     * @param username
     * @return
     */
    public UserInformation findUserInformationByUsername(String username){
        List<UserInformation> userInformations=uimapper.selectList(new EntityWrapper<UserInformation>().eq("username",username));
        return userInformations.size()>0?userInformations.get(0):null;
    }

    /**
     * 更新用户信息
     * @param userInformation
     * @return
     */
    private boolean updateRoleId(UserInformation userInformation){
        int result = uimapper.update(userInformation,new EntityWrapper<UserInformation>().eq("username",userInformation.getUsername()));
        return result > 1 ? true : false;
    }

    /**
     * 根据username生成头像
     * @return
     */
    public String createAvatarByUsername(String username) throws Exception{
        /*以用户名第一个字符当头像文件名*/
        String imgName = username.charAt(0)+".png";
        /*创建文件对象用于保存头像文件*/
        File file = new File(ParameterEnum.FONT_IMAGE_BASE_PATH.getMsg()+imgName);
        /*文件不存在时*/
        if(!file.exists()){
            /*生成头像*/
            FontImageUtil.createImage(String.valueOf(username.charAt(0)),file);
        }
        /*返回相对路径*/
        return "fontImage/"+imgName;
    }

    /**
     * 获取用户信息
     * @param username
     * @return
     */
    public JsonResult getUserInfo(String username) throws Exception{
        JsonResult jsonResult = new JsonResult();
        Map<String,Object> data = new HashMap<String, Object>();
        UserInformation userInformation = findUserInformationByUsername(username);
        /*判断用户名是否存在*/
        if(userInformation != null){
            /*存在则在数据库中获取头像路径*/
            data.put("avatar_path",userInformation.getAvatarPath());
            data.put("nick_name",userInformation.getNickName());
            data.put("bg_path",userInformation.getBgPath());
        }else{
            /*不存在则再判断头像保存路径是否存在以用户名首字符命名的头像文件*/
            File imageFile = new File(ParameterEnum.FONT_IMAGE_BASE_PATH.getMsg()+username.charAt(0)+".png");
            /*文件不存在，则创建文件，并返回路径*/
            if(!imageFile.exists()){
                data.put("avatar_path",createAvatarByUsername(username));
            }else{
                /*存在直接返回路径*/
                data.put("avatar_path","fontImage/"+username.charAt(0)+".png");
            }
        }
        /*设置jsonResult*/
        jsonResult.setData(data);
        jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        return jsonResult;
    }

    /**
     * 修改昵称
     * @param username
     * @param nickname
     * @return
     */
    public JsonResult updateNickname(String username,String nickname){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = findUserInformationByUsername(username);
        int result = -1;
        if(userInformation != null && !nickname.equals(ParameterEnum.ERROR.getMsg())){
            userInformation.setNickName(nickname);
            result = uimapper.updateById(userInformation);
        }else{}
        if(result > 0){
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 根据id获取用户信息
     * @param userId
     * @return
     */
    public UserInformation selectByUiId(Long userId){
        return uimapper.selectById(userId);
    }

    /**
     * 判断用户名是否存在,不存在返回成功
     * @param username
     * @return
     */
    public JsonResult isExistsByUsername(String username){
        if(!username.equals(ParameterEnum.NO_USERNAME.getMsg())){
            if(findUserInformationByUsername(username) == null && findUserByUsername(username) == null){
                return new JsonResult(ResultEnum.SUCCESS);
            }
        }
        return new JsonResult(ResultEnum.ERROR);
    }

    /**
     * 修改用户头像
     * @param username
     * @param avatarPath
     * @return
     */
    public JsonResult updateAvatar(String username,String avatarPath){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = findUserInformationByUsername(username);
        int result = -1;
        if(userInformation != null && !avatarPath.equals(ParameterEnum.ERROR.getMsg())){
            userInformation.setAvatarPath(avatarPath);
            result = uimapper.updateById(userInformation);
        }else{}
        if(result > 0){
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 修改用户背景路径
     * @param username
     * @param bgPath
     * @return
     */
    public JsonResult updateBgPath(String username,String bgPath){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = findUserInformationByUsername(username);
        int result = -1;
        if(userInformation != null && !bgPath.equals(ParameterEnum.ERROR.getMsg())){
            userInformation.setBgPath(bgPath);
            result = uimapper.updateById(userInformation);
        }else{}
        if(result > 0){
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }
}

package tk.timaca.tmail.service;

import com.alibaba.otter.canal.client.CanalConnector;
import com.alibaba.otter.canal.client.CanalConnectors;
import com.alibaba.otter.canal.common.utils.AddressUtils;
import com.alibaba.otter.canal.protocol.CanalEntry.*;
import com.alibaba.otter.canal.protocol.Message;
import tk.timaca.tmail.bean.Mail;
import tk.timaca.tmail.bean.MailAnnex;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.enums.MailTypeEnum;
import tk.timaca.tmail.mapper.MailAnnexMapper;
import tk.timaca.tmail.util.AuthUtil;
import tk.timaca.tmail.util.ParseEmailUtil;
import tk.timaca.tmail.util.SpringUtil;
import tk.timaca.tmail.util.StringUtil;

import java.net.InetSocketAddress;
import java.util.List;

/**
 * Created by dede on 2017/4/17.
 * 数据库监听类
 * http://www.oschina.net/p/canal?fromerr=MXVVaDxo
 * canal Client
 * canal 是 阿里巴巴 MySQL 数据库 Binlog 的增量订阅&消费组件
 */
//@Component
public class DatabaseMonitoring {

    private MessageWebSocketHandler messageWebSocketHandler = (MessageWebSocketHandler) SpringUtil.getBean(MessageWebSocketHandler.class);

    private MailService mailService = (MailService) SpringUtil.getBean(MailService.class);

    private UserService userService = (UserService) SpringUtil.getBean(UserService.class);

    private MailAnnexMapper mailAnnexMapper = (MailAnnexMapper) SpringUtil.getBean(MailAnnexMapper.class);

    private SpamFilterService spamFilterService = (SpamFilterService) SpringUtil.getBean(SpamFilterService.class);

   /* @Autowired
    private MessageWebSocketHandler messageWebSocketHandler;

    @Autowired
    private MailService mailService;

    @Autowired
    private UserService userService;

    private static DatabaseMonitoring databaseMonitoring;  //  关键点1   静态初使化 一个工具类  这样是为了在spring初使化之前

    public void setMessageWebSocketHandler(MessageWebSocketHandler messageWebSocketHandler) {
        this.messageWebSocketHandler = messageWebSocketHandler;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }*/

    /*@PostConstruct     //关键二   通过@PostConstruct 和 @PreDestroy 方法 实现初始化和销毁bean之前进行的操作
    public void init() {
        databaseMonitoring = this;
        databaseMonitoring.mailService = this.mailService;   // 初使化时将已静态化的testService实例化
        databaseMonitoring.userService = this.userService;
        databaseMonitoring.messageWebSocketHandler = this.messageWebSocketHandler;
    }*/
    public DatabaseMonitoring(){
       /* databaseMonitoring = this;
        databaseMonitoring.mailService = this.mailService;   // 初使化时将已静态化的testService实例化
        databaseMonitoring.userService = this.userService;
        databaseMonitoring.messageWebSocketHandler = this.messageWebSocketHandler;*/

        // 创建链接
        CanalConnector connector = CanalConnectors.newSingleConnector(new InetSocketAddress(AddressUtils.getHostIp(),
                11111), "example", "", "");
        int batchSize = 1000;
        int emptyCount = 0;
        try {
            connector.connect();
            connector.subscribe(".*\\..*");
            connector.rollback();
            System.out.println("canal启动");
//            int totalEmptyCount = 120;
            while (true) {
                Message message = connector.getWithoutAck(batchSize); // 获取指定数量的数据
                long batchId = message.getId();
                int size = message.getEntries().size();
                if (batchId == -1 || size == 0) {
                    emptyCount++;
//                    System.out.println("empty count : " + emptyCount);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                    }
                } else {
//                    emptyCount = 0;
                    // System.out.printf("message[batchId=%s,size=%s] \n", batchId, size);
                    monitorInbox(message.getEntries());
                }

                connector.ack(batchId); // 提交确认
                // connector.rollback(batchId); // 处理失败, 回滚数据
            }

//            System.out.println("empty too many times, exit");
        } finally {
            connector.disconnect();
        }
    }

    private void monitorInbox(List<Entry> entrys) {
        for (Entry entry : entrys) {
            if (entry.getEntryType() == EntryType.TRANSACTIONBEGIN || entry.getEntryType() == EntryType.TRANSACTIONEND) {
                continue;
            }

            RowChange rowChage = null;
            try {
                rowChage = RowChange.parseFrom(entry.getStoreValue());
            } catch (Exception e) {
                throw new RuntimeException("ERROR ## parser of eromanga-event has an error , data:" + entry.toString(),
                        e);
            }

            EventType eventType = rowChage.getEventType();
            String schemaName = entry.getHeader().getSchemaName();
            String tableName = entry.getHeader().getTableName();
            /*System.out.println(String.format("================> binlog[%s:%s] , name[%s,%s] , eventType : %s",
                    entry.getHeader().getLogfileName(), entry.getHeader().getLogfileOffset(),
                    schemaName, tableName,
                    eventType));*/

            for (RowData rowData : rowChage.getRowDatasList()) {
                if(schemaName.equals("tmail")&&tableName.equals("inbox")){
                    if (eventType == EventType.DELETE) {
                        List<Column> columns = rowData.getBeforeColumnsList();
                        for (Column column : columns) {
                            if(column.getName().equals("repository_name")){
                                System.out.println(column.getName() + " : " + column.getValue() + column.getClass()+":"+column.getMysqlType()+":"+column.getDefaultInstanceForType()+"    update=" + column.getUpdated());
                            }
                        }
                    } else if (eventType == EventType.INSERT) {
                        List<Column> columns = rowData.getAfterColumnsList();
                        mailList2Object(columns);
                        System.out.println("inbox有新数据");
                        for (Column column : columns) {
                            /*获取收件人信息*/
                            if(column.getName().equals("repository_name")){
                                System.out.println(column.getName() + " : " + column.getValue() + "    update=" + column.getUpdated());
                                try {
                                    /*向前端指定用户推送信息*/
                                    messageWebSocketHandler.send(column.getValue());
                                }catch (Exception e){}
                                /*保存邮件以及邮件附件*/
                                saveMailAndAnnexes(column.getValue());
                            }
                        }
                    } else {
                        System.out.println("-------> before");
                        printColumn(rowData.getBeforeColumnsList());
                        System.out.println("-------> after");
                        printColumn(rowData.getAfterColumnsList());
                    }
                }
            }
        }
    }

    private void printColumn(List<Column> columns) {
        for (Column column : columns) {
            System.out.println(column.getName() + " : " + column.getValue() + "    update=" + column.getUpdated());
        }
    }

    /**
     * 将list转换成Mail对象,并保持到数据库中
     * @param columns
     * @return
     */
    public Mail mailList2Object(List<Column> columns){
//        List<Mail> mails = new ArrayList<Mail>();
        Mail newmail = new Mail(StringUtil.randomLong(),columns.get(1).getValue(),columns.get(4).getValue(),columns.get(9).getValue(),"","",
                columns.get(8).getValue(),System.currentTimeMillis(),0,2,userService.findUserInformationByUsername(columns.get(1).getValue()).getUiId());
//        mailService.saveMail(newmail);

        return  newmail;
    }

    /**
     * 保存该用户最新的邮件和附件
     * @param username
     */
    public void saveMailAndAnnexes(String username){
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        System.out.println("userInformation::"+userInformation != null);
        if(userInformation != null){
            String password = AuthUtil.AESDncode(username,userInformation.getPassword());
            Long userId = userInformation.getUiId();
            List list = ParseEmailUtil.parseEmailByInbox(username,password,-1,-1);
            if(list.size() > 0){
                Mail mail =(Mail)list.get(0);

                String ip = "127.0.0.1";
                /*判断邮件是否是垃圾邮件*/
                if (spamFilterService.isSpam(ip,mail.getContent(),mail.getFromMail(),username)){
                    mail.setType(MailTypeEnum.SPAM_MAIL.getType());
                }else{}

                mail.setUserId(userId);
                System.out.println("收到新邮件插入："+mailService.saveMail(mail));
                List<MailAnnex> mailAnnexes = (List<MailAnnex>)list.get(1);
                for (MailAnnex mailAnnex:mailAnnexes) {
                    mailAnnexMapper.insert(mailAnnex);
                }
            }
        }

    }

    /**
     * 根据抄送，密送解析邮件
     * @param ccOrBcc
     * @param mail
     * @param mailAnnexes
     * @return
     * @throws Exception
     */
    /*public List saveMailAndAnnexesByCcOrBcc(List<String> ccOrBcc,Mail mail,List<MailAnnex> mailAnnexes) throws Exception{
        List list = new ArrayList();
        List<Mail> mailList = new ArrayList<Mail>();
        List<MailAnnex> mailAnnexList = new ArrayList<MailAnnex>();
        if(ccOrBcc != null && ccOrBcc.size() > 0){
            for (String string : ccOrBcc) {
                UserInformation userInformation = userService.findUserInformationByUsername(string);
                if(userInformation != null && userInformation.getPassword() != null){
                    messageWebSocketHandler.send(userInformation.getUsername());
                    Long mailId = StringUtil.randomLong();
                    mail.setMailId(mailId);
                    mail.setUserId(userInformation.getUiId());
                    mailList.add(mail);
                    for (MailAnnex mailAnnex:mailAnnexes) {
                        mailAnnex.setMailAnnexId(StringUtil.randomLong());
                        mailAnnex.setMailId(mailId);
                        mailAnnexList.add(mailAnnex);
                    }
                }
            }
        }
        list.add(mailList);
        list.add(mailAnnexList);
        return list;
    }*/
}

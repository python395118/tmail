package tk.timaca.tmail.service;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dede on 2017/4/17.
 */
@Component
public class MessageWebSocketHandler implements WebSocketHandler {

    private static List<WebSocketSession> webSocketSessionList = new ArrayList<WebSocketSession>();

    public List<WebSocketSession> getWebSocketSessionList() {
        return webSocketSessionList;
    }

    public void setWebSocketSessionList(List<WebSocketSession> webSocketSessionList) {
        this.webSocketSessionList = webSocketSessionList;
    }

    /**
     * 连接成功后的操作
     * @param session
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        if(!isOnline(session)){
            webSocketSessionList.add(session);
            System.out.println("当前在线人数："+webSocketSessionList.size());
        }
    }

    @Override
    public void handleMessage(WebSocketSession session, WebSocketMessage<?> message) throws Exception{
        System.out.println(message.toString());
        session.sendMessage(new TextMessage("你也是888吗"));
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus closeStatus) {
//        System.out.println("close eeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        /*连接关闭后，将session在集合中删除*/
        webSocketSessionList.remove(session);
        System.out.println("xx关闭后，当前在线人数："+webSocketSessionList.size());
    }

    @Override
    public boolean supportsPartialMessages() {
        return false;
    }

    /**
     * 向指定用户发送收到新邮件的消息
     * @param username
     * @throws Exception
     */
    public void send(String username) throws Exception{
        WebSocketSession session = findSessionByUsername(username);
        if(session!=null){
            System.out.println("收到一封新邮件！！！");
            session.sendMessage(new TextMessage("收到一封新邮件！！！"));
        }
    }

    /**
     * 向指定用户发送收到指定的消息
     * @param username
     * @param text
     * @throws Exception
     */
    public void send(String username,String text) throws Exception{
        WebSocketSession session = findSessionByUsername(username);
        if(session!=null){
//            System.out.println("收到一封新邮件！！！");
            session.sendMessage(new TextMessage(text));
        }
    }

    /**
     * 根据username在webSocketSessions中查找session，找不到返回null
     * @param username
     * @return
     */
    public WebSocketSession findSessionByUsername(String username){
        List<WebSocketSession> webSocketSessions = getWebSocketSessionList();
        for (WebSocketSession webSocketSession:webSocketSessions) {
            Map<String,Object> maps = webSocketSession.getAttributes();
//            String token = maps.get("token").toString();
            if(maps.get("username").toString().equals(username)){
                return webSocketSession;
            }
        }
        return null;
    }

    /**
     * 判断WebSocketSession集合中是否存在session
     * @param session
     * @return
     */
    public boolean isOnline(WebSocketSession session){
        Map<String,Object> maps = session.getAttributes();
        String username = maps.get("username").toString();
        if(findSessionByUsername(username) == null){
            return false;
        }
        return true;
    }
}

package tk.timaca.tmail.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.HttpLog;
import tk.timaca.tmail.mapper.HttpLogMapper;

/**
 * Created by dede on 2017/3/31.
 * 处理日志的业务逻辑类
 */
@Service
public class LogService {

    @Autowired
    private HttpLogMapper httpLogMapper;

    public boolean addLog(HttpLog httpLog){
        return httpLogMapper.insert(httpLog) > 0 ? true : false;
    }

}

package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.bean.MailList;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.mapper.MailListMapper;
import tk.timaca.tmail.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dede on 2017/5/8.
 * 邮箱黑白名单业务逻辑类
 */
@Service
public class MailListService {

    @Autowired
    private MailListMapper mailListMapper;

    @Autowired
    private UserService userService;

    /**
     * 根据用户名获取黑名单列表
     * @param username
     * @return
     */
    public JsonResult getAllBlackListByUsername(String username){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null){
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
            Long userId = userInformation.getUiId();
            jsonResult.setData(getAllList(userId,0));
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 根据用户名获取白名单列表
     * @param username
     * @return
     */
    public JsonResult getAllWhiteListByUsername(String username){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null){
            Long userId = userInformation.getUiId();
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
            jsonResult.setData(getAllList(userId,1));
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 根据用户id和类型获取邮箱名单字符串集合
     * @param userId
     * @param type
     * @return
     */
    public List<String> getAllList(Long userId,Integer type){
        List<String> list = new ArrayList<String>();
        List<MailList> mailLists = mailListMapper.selectList(new EntityWrapper<MailList>()
                .eq("user_id",userId)
                .eq("mail_list_type",type));
        if(mailLists.size() > 0){
            for (MailList mailList:mailLists) {
                list.add(mailList.getMailListName());
            }
        }
        return list;
    }

    /**
     * 删除名单
     * @param name
     * @param username
     * @return
     */
    public JsonResult deleteList(String name,String username){
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation!=null){
            int result = mailListMapper.delete(new EntityWrapper<MailList>()
                    .eq("mail_list_name",name).eq("user_id",userInformation.getUiId()));
            if(result > 0 ){
                return new JsonResult(ResultEnum.SUCCESS);
            }
        }
        return new JsonResult(ResultEnum.ERROR);
    }

    /**
     * 根据主键删除名单
     * @param mailListId
     * @return
     */
    public JsonResult deleteListById(Long mailListId){
        JsonResult jsonResult = new JsonResult();
        jsonResult.setCode(mailListMapper.deleteById(mailListId) > 0 ?
                ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        return jsonResult;
    }

    /**
     * 插入名单
     * @param name
     * @param username
     * @param type
     * @return
     */
    public JsonResult addList(String name,String username,Integer type){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null){
            if(StringUtil.checkEmail(name) && (type == 1 || type == 0)){
                Long userId = userInformation.getUiId();
                jsonResult.setCode(mailListMapper.insert(new MailList(StringUtil.randomLong(),name,type,userId)) > 0 ?
                        ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
            }else{
                jsonResult.setCode(ResultEnum.ERROR.getCode());
            }
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 判断address 是否在 用户名为username的黑名单里
     * @param username
     * @param address
     * @return
     */
    public boolean isMailBlackList(String username,String address){
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null){
            Integer count = mailListMapper.selectCount(new EntityWrapper<MailList>()
                    .eq("user_id",userInformation.getUiId()).eq("mail_list_name",address)
                    .eq("mail_list_type",0));
            if(count == 1){
                return true;
            }
        }
        return false;
    }

    /**
     * 判断address 是否在 用户名为username的白名单里
     * @param username
     * @param address
     * @return
     */
    public boolean isMailWhiteList(String username,String address){
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null){
            Integer count = mailListMapper.selectCount(new EntityWrapper<MailList>()
                    .eq("user_id",userInformation.getUiId()).eq("mail_list_name",address)
                    .eq("mail_list_type",1));
            if(count == 1){
                return true;
            }
        }
        return false;
    }
}

package tk.timaca.tmail.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.config.CaptchaConfig;
import tk.timaca.tmail.config.JedisConfig;
import tk.timaca.tmail.enums.ResultEnum;

/**
 * Created by dede on 2017/5/11.
 * 验证码业务逻辑类
 */
@Service
public class CaptchaService {

    @Autowired
    private CaptchaConfig captchaConfig;

    @Autowired
    private JedisConfig jedisConfig;

    private Jedis jedis;

    public CaptchaService(){
        if(jedisConfig==null){
            JedisPool pool = new JedisPool(new JedisPoolConfig(), "127.0.0.1");
            this.jedis = pool.getResource();
        }
    }

    /**
     * 保存验证码信息到redis中，uuid：标识、capText：验证码
     * @param uuid
     * @param capText
     */
    public void saveCaptcha(String uuid,String capText){
        /**
         * EX seconds − 设置指定的到期时间(以秒为单位)。
         * PX milliseconds - 设置指定的到期时间(以毫秒为单位)。
         * NX - 仅在键不存在时设置键。
         * XX - 只有在键已存在时才设置。
         *
         * 将验证码存进redis中，有效期为5分钟
         */
        this.jedis.set(uuid,capText,"NX","EX",60*5);
    }

    /**
     * 根据uuid获取验证码
     * @param uuid
     * @return
     */
    public String getCapTextByUUID(String uuid){
        return this.jedis.get(uuid);
    }

    /**
     * 判断验证码是否正确
     * @param uuid
     * @param capText
     * @return
     */
    public Boolean checkCapText(String uuid,String capText){
        String trueCapText = getCapTextByUUID(uuid).toUpperCase();
        if(trueCapText != null){
            if(trueCapText.equals(capText.toUpperCase())){
                return true;
            }
        }
        return false;
    }

    /**
     * 对外判断验证码是否正确
     * @param uuid
     * @param capText
     * @return
     */
    public JsonResult checkCapTextForeign(String uuid,String capText){
        return new JsonResult(checkCapText(uuid, capText) ? ResultEnum.SUCCESS : ResultEnum.ERROR);
    }
}

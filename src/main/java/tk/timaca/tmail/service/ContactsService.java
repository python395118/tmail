package tk.timaca.tmail.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.timaca.tmail.bean.Contacts;
import tk.timaca.tmail.bean.ContactsGroup;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.bean.UserInformation;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.mapper.ContactsGroupMapper;
import tk.timaca.tmail.mapper.ContactsMapper;
import tk.timaca.tmail.util.StringUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dede on 2017/4/25.
 * 通讯录业务逻辑类
 */
@Service
public class ContactsService {

    @Autowired
    private ContactsGroupMapper contactsGroupMapper;

    @Autowired
    private ContactsMapper contactsMapper;

    @Autowired
    private UserService userService;

    /**
     * 往username的name群组添加otherusername的记录
     * @param username
     * @param otherUsername
     * @param name
     * @return
     * @throws Exception
     */
    public JsonResult addContacts(String username,String otherUsername,String name,String remarks,String address,String phone) throws Exception{
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        UserInformation userInformation1 = userService.findUserInformationByUsername(otherUsername);
        ContactsGroup contactsGroup = selectContactsGroupByUsernameAndName(username,name);
        Contacts contacts = selectContactsByUsernameAndNameAndOtherusername(username, otherUsername, name);
        JsonResult jsonResult = new JsonResult();
        int result = -1;
        /*判断操作者是否合法、分组是否合法、联系人是否存在*/
        if(userInformation != null && contactsGroup != null && contacts == null){
            /*若添加的联系人不存在，则创建新的用户信息，再新增联系人*/
            if(userInformation1 == null){
                 if(userService.createUserNoPwd(otherUsername).getCode() == ResultEnum.REGISTER_SUCCESS.getCode()) {
                     UserInformation userInformation2 = userService.findUserInformationByUsername(otherUsername);
                     result = contactsMapper.insert(new Contacts(StringUtil.randomLong(), userInformation2.getUiId(), contactsGroup.getContactsGroupId(), "friend",remarks,address,phone));
                 }
            }else{/*若用户信息存在，直接新增联系人*/
                result = contactsMapper.insert(new Contacts(StringUtil.randomLong(),userInformation1.getUiId(),contactsGroup.getContactsGroupId(),"friend",remarks,address,phone));
            }
        }else{}
        jsonResult.setCode(result > 0 ? ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        return jsonResult;
    }

    /**
     * 根据contactsUserId删除通讯录记录
     * @param contactsUserId
     * @return
     */
    public JsonResult removeContactsByContactsUserId(Long contactsUserId){
        JsonResult jsonResult = new JsonResult();
        int result = contactsMapper.delete(new EntityWrapper<Contacts>().eq("contacts_user_id",contactsUserId));
        jsonResult.setCode(result > 0 ? ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        return jsonResult;
    }

    /**
     * 查询用户名为username、联系人名为othername的name群组的通讯录记录
     * @param username
     * @param name
     * @param otherUsername
     * @return
     */
    public JsonResult removeContacts(String username,String name,String otherUsername){
        JsonResult jsonResult = new JsonResult();
        Contacts contacts = selectContactsByUsernameAndNameAndOtherusername(username, name, otherUsername);
        int result = -1;
        if(contacts!=null){
            result = contactsMapper.deleteById(contacts.getContactsId());
            jsonResult.setCode(result > 0 ? ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 添加分组
     * @param username
     * @param name
     * @return
     */
    public JsonResult addContactsGroup(String username,String name){
        JsonResult jsonResult = new JsonResult();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        Long contactsGroupId = StringUtil.randomLong();
        int result = -1;
        if(userInformation != null){
            if(selectContactsGroupByUsernameAndName(username,name) == null){
                result = contactsGroupMapper.insert(new ContactsGroup(contactsGroupId,name,userInformation.getUiId()));
                if(result > 0){
                    result = contactsMapper.insert(new Contacts(StringUtil.randomLong(),userInformation.getUiId(),contactsGroupId,"owner"));
                    jsonResult.setCode(result>0 ? ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
                    return jsonResult;
                }
            }
        }
        jsonResult.setCode(ResultEnum.ERROR.getCode());
        return jsonResult;
    }

    /**
     * 移除用户名为username，组名为name的群组，并把其移到默认分组中
     * @param username
     * @param name
     * @return
     */
    public JsonResult removeContactsGroup(String username,String name){
        JsonResult jsonResult = new JsonResult();
        int result = -1;
        if(!name.equals("默认分组")){
            ContactsGroup contactsGroup = selectContactsGroupByUsernameAndName(username, name);
            if(contactsGroup!=null){
                /*删除组数据*/
                contactsGroupMapper.deleteById(contactsGroup.getContactsGroupId());
                ContactsGroup contactsGroup1 = selectContactsGroupByUsernameAndName(username,"默认分组");
                /*将该组的成员移到默认分组中*/
                Contacts contacts = new Contacts();
                contacts.setContactsGroupId(contactsGroup1.getContactsGroupId());
                result = contactsMapper.update(contacts,new EntityWrapper<Contacts>().eq("contacts_group_id",contactsGroup.getContactsGroupId()));
                jsonResult.setCode(result>0 ? ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
            }
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }

        return jsonResult;
    }

    /**
     * 根据用户名和群组名查询组信息，并返回组信息
     * @param username
     * @param name
     * @return
     */
    public ContactsGroup selectContactsGroupByUsernameAndName(String username,String name){
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        List<ContactsGroup> contactsGroups = contactsGroupMapper.selectList(new EntityWrapper<ContactsGroup>().eq("name",name).eq("owner",userInformation.getUiId()));
        if(contactsGroups.size() > 0){
            return contactsGroups.get(0);
        }else{
            return null;
        }
    }

    /**
     * 查询用户名为username、联系人名为othername的name群组的通讯录记录
     * @param username
     * @param name
     * @param otherUsername
     * @return
     */
    public Contacts selectContactsByUsernameAndNameAndOtherusername(String username,String name,String otherUsername){
        ContactsGroup contactsGroup = selectContactsGroupByUsernameAndName(username, name);
        UserInformation userInformation = userService.findUserInformationByUsername(otherUsername);
        List<Contacts> contacts = new ArrayList<Contacts>();
        if(contactsGroup != null && userInformation!=null){
            contacts = contactsMapper.selectList(new EntityWrapper<Contacts>().eq("contacts_group_id",contactsGroup.getContactsGroupId())
                    .eq("contacts_user_id",userInformation.getUiId()));
            return contacts.size() > 0 ? contacts.get(0) : null;
        }else{
            return null;
        }
    }

    /**
     * 根据username获取通讯录列表
     * @param username
     * @return
     */
    public JsonResult getContactsByUsername(String username){
        JsonResult jsonResult = new JsonResult();
        Map<String,Object> data = new HashMap<String, Object>();
        UserInformation userInformation = userService.findUserInformationByUsername(username);
        if(userInformation != null){/*判断用户名为username的用户是否存在*/
            List<ContactsGroup> contactsGroups = contactsGroupMapper.selectList(new EntityWrapper<ContactsGroup>()
                    .eq("owner",userInformation.getUiId()));/*获取用户联系人分组集合*/
            if(contactsGroups.size() > 0){/*判断用户联系人分组集合长度*/
                for (ContactsGroup contactsGroup : contactsGroups) {/*遍历分组集合*/
                    Long contactsGroupId = contactsGroup.getContactsGroupId();
                    List<Contacts> contacts = contactsMapper.selectList(new EntityWrapper<Contacts>()
                            .eq("contacts_group_id",contactsGroupId));/*根据分组主键获取联系人集合*/
                    if(contacts.size() > 0 ){
                        Map<String,Object> contactsListData = new HashMap<String, Object>();
                        for (Contacts contacts1 : contacts) {
                            UserInformation userInformation1 = userService.selectByUiId(contacts1.getContactsUserId());
                            /*若联系人用户信息存在，且不是操作人，则添加到map中*/
                            if(userInformation1 != null && !userInformation1.getUsername().equals(username)){
                                Map<String,Object> contactsData = new HashMap<String, Object>();
                                contactsData.put("avatar",userInformation1.getAvatarPath());
                                contactsData.put("nick_name",contacts1.getRemarks());
                                contactsData.put("user_name",userInformation1.getUsername());
                                contactsListData.put(String.valueOf(userInformation1.getUiId()),contactsData);
                            }else{}
                        }
                        data.put(contactsGroup.getName(),contactsListData);
                    }else{}
                    jsonResult.setCode(ResultEnum.SUCCESS.getCode());
                    jsonResult.setData(data);
                }
            }
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 更新联系人信息
     * @param username
     * @param groupName
     * @param otherUsername
     * @param remarks
     * @param address
     * @param phone
     * @param updatedGroupName
     * @return
     */
    public JsonResult updateContacts(String username,String groupName,String otherUsername,String remarks,String address,String phone,String updatedGroupName){
        JsonResult jsonResult = new JsonResult();
        Contacts contacts = selectContactsByUsernameAndNameAndOtherusername(username, groupName, otherUsername);
        ContactsGroup contactsGroup = selectContactsGroupByUsernameAndName(username,groupName);
        ContactsGroup afterContactsGroup = selectContactsGroupByUsernameAndName(username,updatedGroupName);
        /*判断username、groupName、otherUsername参数是否合法*/
        if(contacts != null && contactsGroup != null){
            /*判断remarks是否合法*/
            if(remarks != ParameterEnum.ERROR.getMsg()){
                contacts.setRemarks(remarks);
            }else{}
            /*判断address是否合法*/
            if(address != ParameterEnum.ERROR.getMsg()){
                contacts.setAddress(address);
            }else{}
            /*判断phone是否合法*/
            if(phone != ParameterEnum.ERROR.getMsg()){
                contacts.setPhone(phone);
            }else{}
            /*若updateGroupName合法，则更新联系人所在分组*/
            if(afterContactsGroup != null){
                contacts.setContactsGroupId(afterContactsGroup.getContactsGroupId());
            }else{}
            /*更新联系人信息，并根据结果设置返回码*/
            jsonResult.setCode(contactsMapper.updateById(contacts)>0 ?
                    ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 更新分组名称
     * @param username
     * @param groupName
     * @param afterGroupName
     * @return
     */
    public JsonResult updateContactsGroup(String username,String groupName,String afterGroupName){
        JsonResult jsonResult = new JsonResult();
        ContactsGroup contactsGroup = selectContactsGroupByUsernameAndName(username,groupName);
        ContactsGroup afterContactsGroup = selectContactsGroupByUsernameAndName(username,afterGroupName);
        /*判断参数是否合法*/
        if(contactsGroup != null && afterContactsGroup == null){
            if(!afterGroupName.equals(ParameterEnum.ERROR.getMsg())){
                /*设置分组名称，更新数据，然后根据结果设置返回码*/
                contactsGroup.setName(afterGroupName);
                jsonResult.setCode(contactsGroupMapper.updateById(contactsGroup) >0 ?
                        ResultEnum.SUCCESS.getCode() : ResultEnum.ERROR.getCode());
            }else{
                jsonResult.setCode(ResultEnum.ERROR.getCode());
            }
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }

    /**
     * 获取联系人详细信息
     * @param username
     * @param groupName
     * @param otherUsername
     * @return
     */
    public JsonResult getContact(String username,String groupName,String otherUsername){
        JsonResult jsonResult = new JsonResult();
        Contacts contacts = selectContactsByUsernameAndNameAndOtherusername(username, groupName, otherUsername);
        Map<String,Object> data = new HashMap<String, Object>();
        /*判断参数是否合法*/
        if(contacts != null){
            data.put("address",contacts.getAddress());
            data.put("phone",contacts.getPhone());
            jsonResult.setData(data);
            jsonResult.setCode(ResultEnum.SUCCESS.getCode());
        }else{
            jsonResult.setCode(ResultEnum.ERROR.getCode());
        }
        return jsonResult;
    }
}

package tk.timaca.tmail.handle;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.exception.NoAuthException;

/**
 * Created by dede on 2017/3/31.
 * 捕获到异常后统一处理
 */
@ControllerAdvice
public class ExceptionHandle {

    private final static Logger logger = LoggerFactory.getLogger(ExceptionHandle.class);

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public JsonResult handle(Exception e){
        if(e instanceof NoAuthException){
            NoAuthException noAuthException = (NoAuthException)e;
            return new JsonResult(noAuthException.getCode(),noAuthException.getMessage());
        }else{
            logger.error("系统异常{}",e);
            return  new JsonResult(-1,"未知错误");
        }
    }

}

package tk.timaca.tmail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by dede on 2017/4/10.
 */
@Component
@ConfigurationProperties(prefix = "auth")
public class AuthConfig {

    private String[] NoPermissionRequired;

    public String[] getNoPermissionRequired() {
        return NoPermissionRequired;
    }

    public void setNoPermissionRequired(String[] noPermissionRequired) {
        NoPermissionRequired = noPermissionRequired;
    }
}

package tk.timaca.tmail.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;

/**
 * Created by dede on 2017/4/9.
 * 配置url和html页面的映射关系
 */
@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
//        registry.addViewController("/666").setViewName("/index.html");
//        registry.addViewController("/777").setViewName("/templates/index.html");
        registry.addViewController("/888").setViewName("inbox");
//        registry.addViewController("/error").setViewName("error");
        registry.setOrder(Ordered.HIGHEST_PRECEDENCE);
        super.addViewControllers(registry);
    }

    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(false);
        super.configurePathMatch(configurer);
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/fontImage/**").addResourceLocations("file:"+new File("").getAbsolutePath()+"/fontImage/");
        registry.addResourceHandler("/annex/**").addResourceLocations("file:"+new File("").getAbsolutePath()+"/annex/");
        registry.addResourceHandler("/uploadImg/**").addResourceLocations("file:"+new File("").getAbsolutePath()+"/uploadImg/");
//        registry.addResourceHandler("/fontImage/**").addResourceLocations("classpath:/fontImage/");
//        registry.addResourceHandler("/annex/**").addResourceLocations("classpath:/annex/");
        if(!new File("fontImage").exists()){
            new File("fontImage").mkdirs();
        }
        if(!new File("annex").exists()){
            new File("annex").mkdirs();
        }
        if(!new File("uploadImg").exists()){
            new File("uploadImg").mkdirs();
        }
        System.out.println("新建文件保存路径："+new File("fontImage").getAbsolutePath());
        System.out.println("新建文件保存路径："+new File("annex").getAbsolutePath());
        System.out.println("新建文件保存路径："+new File("uploadImg").getAbsolutePath());
        super.addResourceHandlers(registry);
    }
}

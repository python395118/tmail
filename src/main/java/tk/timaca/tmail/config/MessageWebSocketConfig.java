package tk.timaca.tmail.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import tk.timaca.tmail.controller.MessageWebSocketInterceptor;
import tk.timaca.tmail.service.MessageWebSocketHandler;

/**
 * Created by dede on 2017/4/17.
 * WebSocket配置文件
 */
@Configuration
@EnableWebSocket
public class MessageWebSocketConfig implements WebSocketConfigurer {
    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(messageWebSocketHandler(), "/sockjs/message")
                .addInterceptors(messageWebSocketInterceptor()).withSockJS();
    }

    @Bean
    public MessageWebSocketHandler messageWebSocketHandler() {
        return new MessageWebSocketHandler();
    }

    @Bean
    public MessageWebSocketInterceptor messageWebSocketInterceptor(){
        return new MessageWebSocketInterceptor();
    }
}

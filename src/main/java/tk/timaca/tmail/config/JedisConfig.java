package tk.timaca.tmail.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by dede on 2017/5/11.
 * redis配置
 */
@Configuration
@ConfigurationProperties(prefix = "Jedis")
public class JedisConfig {

    private String host;

    private Jedis jedis;

    @Bean
    public Jedis jedis() {
        System.out.println("Jedis.host"+host);
        JedisPool pool = new JedisPool(new JedisPoolConfig(), this.host);
        this.jedis = pool.getResource();
        //   jedis.auth("password");
        return this.jedis;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Jedis getJedis() {
        return jedis;
    }

    public void setJedis(Jedis jedis) {
        this.jedis = jedis;
    }
}

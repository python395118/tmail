package tk.timaca.tmail;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import tk.timaca.tmail.service.DatabaseMonitoring;

@MapperScan("tk.timaca.tmail.mapper*")
@SpringBootApplication
@EnableScheduling
@ServletComponentScan
public class TmailApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmailApplication.class, args);
		DatabaseMonitoring databaseMonitoring = new DatabaseMonitoring();

	}
}

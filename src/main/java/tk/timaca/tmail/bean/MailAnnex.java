package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/4/20.
 * 邮件附件实体类
 */
@TableName(value = "mail_annex")
public class MailAnnex extends Model<MailAnnex> {

    @TableId(value = "mail_annex_id")
    private Long mailAnnexId;

    /*附件保存路径*/
    private String path;

    /*文件名*/
    @TableField(value = "file_name")
    private String fileName;

    /*文件类型*/
    @TableField(value = "content_type")
    private String contentType;

    @TableField(value = "mail_id")
    private Long mailId;

    public MailAnnex() {
    }

    public MailAnnex(Long mailAnnexId, String path, String fileName, String contentType, Long mailId) {
        this.mailAnnexId = mailAnnexId;
        this.path = path;
        this.fileName = fileName;
        this.contentType = contentType;
        this.mailId = mailId;
    }

    public Long getMailAnnexId() {
        return mailAnnexId;
    }

    public void setMailAnnexId(Long mailAnnexId) {
        this.mailAnnexId = mailAnnexId;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getMailId() {
        return mailId;
    }

    public void setMailId(Long mailId) {
        this.mailId = mailId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Override
    public String toString() {
        return "MailAnnex{" +
                "mailAnnexId=" + mailAnnexId +
                ", path='" + path + '\'' +
                ", fileName='" + fileName + '\'' +
                ", contentType='" + contentType + '\'' +
                ", mailId=" + mailId +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.mailAnnexId;
    }
}

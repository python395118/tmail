package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/4/25.
 */
@TableName(value = "contacts")
public class Contacts extends Model<Contacts> {
    /*通讯录主键*/
    @TableId(value = "contacts_id")
    private Long contactsId;

    /*用户id*/
    @TableField(value = "contacts_user_id")
    private Long contactsUserId;

    /*组id*/
    @TableField(value = "contacts_group_id")
    private Long contactsGroupId;

    /*组拥有者对用户的权限*/
    private String permission;

    /*备注 即昵称*/
    private String remarks;

    /*地址*/
    private String address;

    /*电话*/
    private String phone;

    public Contacts() {
    }

    public Contacts(Long contactsId, Long contactsUserId, Long contactsGroupId, String permission, String remarks, String address, String phone) {
        this.contactsId = contactsId;
        this.contactsUserId = contactsUserId;
        this.contactsGroupId = contactsGroupId;
        this.permission = permission;
        this.remarks = remarks;
        this.address = address;
        this.phone = phone;
    }

    public Contacts(Long contactsId, Long contactsUserId, Long contactsGroupId, String permission, String remarks) {
        this.contactsId = contactsId;
        this.contactsUserId = contactsUserId;
        this.contactsGroupId = contactsGroupId;
        this.permission = permission;
        this.remarks = remarks;
    }

    public Contacts(Long contactsId, Long contactsUserId, Long contactsGroupId, String permission) {
        this.contactsId = contactsId;
        this.contactsUserId = contactsUserId;
        this.contactsGroupId = contactsGroupId;
        this.permission = permission;
    }

    public Long getContactsId() {
        return contactsId;
    }

    public void setContactsId(Long contactsId) {
        this.contactsId = contactsId;
    }

    public Long getContactsUserId() {
        return contactsUserId;
    }

    public void setContactsUserId(Long contactsUserId) {
        this.contactsUserId = contactsUserId;
    }

    public Long getContactsGroupId() {
        return contactsGroupId;
    }

    public void setContactsGroupId(Long contactsGroupId) {
        this.contactsGroupId = contactsGroupId;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Contacts{" +
                "contactsId=" + contactsId +
                ", contactsUserId=" + contactsUserId +
                ", contactsGroupId=" + contactsGroupId +
                ", permission='" + permission + '\'' +
                ", remarks='" + remarks + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.contactsId;
    }
}

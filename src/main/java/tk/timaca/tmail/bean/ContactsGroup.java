package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/4/25.
 */
@TableName(value = "contacts_group")
public class ContactsGroup extends Model<ContactsGroup> {

    /*组id*/
    @TableId(value = "contacts_group_id")
    private Long contactsGroupId;

    /*组显示名称*/
    private String name;

    /*组拥有者*/
    private Long owner;

    public ContactsGroup() {
    }

    public ContactsGroup(Long contactsGroupId, String name, Long owner) {
        this.contactsGroupId = contactsGroupId;
        this.name = name;
        this.owner = owner;
    }

    public Long getContactsGroupId() {
        return contactsGroupId;
    }

    public void setContactsGroupId(Long contactsGroupId) {
        this.contactsGroupId = contactsGroupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getOwner() {
        return owner;
    }

    public void setOwner(Long owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        return "ContactsGroup{" +
                "contactsGroupId=" + contactsGroupId +
                ", name='" + name + '\'' +
                ", owner=" + owner +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.contactsGroupId;
    }
}

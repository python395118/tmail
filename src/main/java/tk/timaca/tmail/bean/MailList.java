package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/5/8.
 * 邮件黑白名单实体类
 */
@TableName(value = "mail_list")
public class MailList  extends Model<MailList> {
    /*主键*/
    @TableId(value = "mail_list_id")
    private Long mailListId;

    /*名称*/
    @TableField(value = "mail_list_name")
    private String mailListName;

    /*类型，0为黑名单，1为白名单*/
    @TableField(value = "mail_list_type")
    private Integer mailListType;

    /*所属用户id*/
    @TableField(value = "user_id")
    private Long userId;

    public MailList() {
    }

    public MailList(Long mailListId, String mailListName, Integer mailListType, Long userId) {
        this.mailListId = mailListId;
        this.mailListName = mailListName;
        this.mailListType = mailListType;
        this.userId = userId;
    }

    public Long getMailListId() {
        return mailListId;
    }

    public void setMailListId(Long mailListId) {
        this.mailListId = mailListId;
    }

    public String getMailListName() {
        return mailListName;
    }

    public void setMailListName(String mailListName) {
        this.mailListName = mailListName;
    }

    public Integer getMailListType() {
        return mailListType;
    }

    public void setMailListType(Integer mailListType) {
        this.mailListType = mailListType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "MailList{" +
                "mailListId=" + mailListId +
                ", mailListName='" + mailListName + '\'' +
                ", mailListType=" + mailListType +
                ", userId=" + userId +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.mailListId;
    }
}

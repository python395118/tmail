package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/3/19.
 * 用户基本信息实体类
 */
@TableName(value = "user_information")
public class UserInformation extends Model<UserInformation>{

    /**
     * 主键
     */
    @TableId(value = "ui_id")
    private long uiId;
    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    private String nickName;
    /**
     * 账号
     */
    private String username;

    /**
     * 注册时间
     */
    @TableField(value = "reg_time")
    private long regTime;
    /**
     * 最后登录时间
     */
    @TableField(value = "last_login_time")
    private long lastLoginTime;

    /**
     * 是否保持在线，1表示保持在线
     */
    @TableField(value = "keep_alive")
    private int keepAlive;

    /**
     * 所属角色id 0为游客 1为用户 2为管理员
     */
    @TableField(value = "role_id")
    private Long roleId;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像路径
     */
    @TableField(value = "avatar_path")
    private String avatarPath;

    /**
     * 界面背景路径
     */
    @TableField(value = "bg_path")
    private String bgPath = "img/登录背景.jpg";

    public UserInformation() {
    }

    @Override
    protected Serializable pkVal() {
        return this.uiId;
    }

    public UserInformation(long uiId, String nickName, String username, long regTime, long lastLoginTime, int keepAlive, Long roleId, String password) {
        this.uiId = uiId;
        this.nickName = nickName;
        this.username = username;
        this.regTime = regTime;
        this.lastLoginTime = lastLoginTime;
        this.keepAlive = keepAlive;
        this.roleId = roleId;
        this.password = password;
    }

    public UserInformation(long uiId, String nickName, String username, long regTime, long lastLoginTime, int keepAlive, Long roleId, String password, String avatarPath) {
        this.uiId = uiId;
        this.nickName = nickName;
        this.username = username;
        this.regTime = regTime;
        this.lastLoginTime = lastLoginTime;
        this.keepAlive = keepAlive;
        this.roleId = roleId;
        this.password = password;
        this.avatarPath = avatarPath;
    }

    public UserInformation(String username) {
        this.username = username;
    }

    public UserInformation(long uiId, String nickName, String username, long regTime, long lastLoginTime, int keepAlive, Long roleId, String password, String avatarPath, String bgPath) {
        this.uiId = uiId;
        this.nickName = nickName;
        this.username = username;
        this.regTime = regTime;
        this.lastLoginTime = lastLoginTime;
        this.keepAlive = keepAlive;
        this.roleId = roleId;
        this.password = password;
        this.avatarPath = avatarPath;
        this.bgPath = bgPath;
    }

    public long getUiId() {
        return uiId;
    }

    public void setUiId(long uiId) {
        this.uiId = uiId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public long getRegTime() {
        return regTime;
    }

    public void setRegTime(long regTime) {
        this.regTime = regTime;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public int getKeepAlive() {
        return keepAlive;
    }

    public void setKeepAlive(int keepAlive) {
        this.keepAlive = keepAlive;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatarPath() {
        return avatarPath;
    }

    public void setAvatarPath(String avatarPath) {
        this.avatarPath = avatarPath;
    }

    public String getBgPath() {
        return bgPath;
    }

    public void setBgPath(String bgPath) {
        this.bgPath = bgPath;
    }
}

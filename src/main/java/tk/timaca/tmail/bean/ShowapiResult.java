package tk.timaca.tmail.bean;

import java.util.Map;

/**
 * Created by dede on 2017/5/13.
 */
public class ShowapiResult {

    private Integer showapi_res_code;

    private String showapi_res_error;

    private Map<String,Object> showapi_res_body;

    public ShowapiResult() {
    }

    public ShowapiResult(Integer showapi_res_code, String showapi_res_error, Map<String, Object> showapi_res_body) {
        this.showapi_res_code = showapi_res_code;
        this.showapi_res_error = showapi_res_error;
        this.showapi_res_body = showapi_res_body;
    }

    public Integer getShowapi_res_code() {
        return showapi_res_code;
    }

    public void setShowapi_res_code(Integer showapi_res_code) {
        this.showapi_res_code = showapi_res_code;
    }

    public String getShowapi_res_error() {
        return showapi_res_error;
    }

    public void setShowapi_res_error(String showapi_res_error) {
        this.showapi_res_error = showapi_res_error;
    }

    public Map<String, Object> getShowapi_res_body() {
        return showapi_res_body;
    }

    public void setShowapi_res_body(Map<String, Object> showapi_res_body) {
        this.showapi_res_body = showapi_res_body;
    }

    @Override
    public String toString() {
        return "ShowapiResult{" +
                "showapi_res_code=" + showapi_res_code +
                ", showapi_res_error='" + showapi_res_error + '\'' +
                ", showapi_res_body=" + showapi_res_body +
                '}';
    }
}

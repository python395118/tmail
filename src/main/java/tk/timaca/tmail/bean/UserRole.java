package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/4/7.
 * 用户角色对应实体类
 */
@TableName(value = "user_role")
public class UserRole extends Model<UserRole>{

    @TableId(value = "ui_id")
    private Long uiId;

    @TableField(value = "role_id")
    private Long roleId;

    public UserRole() {
    }

    public UserRole(Long uiId, Long roleId) {
        this.uiId = uiId;
        this.roleId = roleId;
    }

    public Long getUiId() {
        return uiId;
    }

    public void setUiId(Long uiId) {
        this.uiId = uiId;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "uiId=" + uiId +
                ", roleId=" + roleId +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.uiId;
    }
}

package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import java.sql.Timestamp;
import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by dede on 2017/3/21.
 * james服务器生成的数据表，存储邮件的收发状态，当邮件成功发送，记录将被删除
 */
@TableName(value = "spool")
public class Spool extends Model<Spool>{
    @TableId(value = "message_name")
    private String messageName;

    @TableField(value = "repository_name")
    private String repositoryName;

    @TableField(value = "message_state")
    private String messageState;

    @TableField(value = "error_message")
    private String errorMessage;

    @TableField(value = "sender")
    private String sender;

    @TableField(value = "recipients")
    private String recipients;

    @TableField(value = "remote_host")
    private String remoteHost;

    @TableField(value = "remote_addr")
    private String remoteAddr;

    @TableField(value = "message_body")
    private byte[] messageBody;

    @TableField(value = "message_attributes")
    private byte[] messageAttributes;

    @TableField(value = "last_updated")
    private Timestamp lastUpdated;

    public Spool() {
    }

    public Spool(String messageName, String repositoryName, String messageState, String errorMessage, String sender, String recipients, String remoteHost, String remoteAddr, byte[] messageBody, byte[] messageAttributes, Timestamp lastUpdated) {
        this.messageName = messageName;
        this.repositoryName = repositoryName;
        this.messageState = messageState;
        this.errorMessage = errorMessage;
        this.sender = sender;
        this.recipients = recipients;
        this.remoteHost = remoteHost;
        this.remoteAddr = remoteAddr;
        this.messageBody = messageBody;
        this.messageAttributes = messageAttributes;
        this.lastUpdated = lastUpdated;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }

    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    public String getMessageState() {
        return messageState;
    }

    public void setMessageState(String messageState) {
        this.messageState = messageState;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    public String getRemoteAddr() {
        return remoteAddr;
    }

    public void setRemoteAddr(String remoteAddr) {
        this.remoteAddr = remoteAddr;
    }

    public byte[] getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(byte[] messageBody) {
        this.messageBody = messageBody;
    }

    public byte[] getMessageAttributes() {
        return messageAttributes;
    }

    public void setMessageAttributes(byte[] messageAttributes) {
        this.messageAttributes = messageAttributes;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Timestamp lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    @Override
    public String
    toString() {
        return "Spool{" +
                "messageName='" + messageName + '\'' +
                ", repositoryName='" + repositoryName + '\'' +
                ", messageState='" + messageState + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                ", sender='" + sender + '\'' +
                ", recipients='" + recipients + '\'' +
                ", remoteHost='" + remoteHost + '\'' +
                ", remoteAddr='" + remoteAddr + '\'' +
                ", messageBody=" + Arrays.toString(messageBody) +
                ", messageAttributes=" + Arrays.toString(messageAttributes) +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    @Override
    protected Serializable pkVal() {

        return messageName;
    }
}

package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/3/21.
 * james自动生成的存储用户信息的数据表
 */
@TableName(value = "users")
public class Users extends Model<Users>{
    @TableId(value = "username")
    private String username;

    private String pwdHash;

    private String pwdAlgorithm;

    private Integer useForwarding;

    private String forwardDestination;

    private Integer useAlias;

    private String alias;

    public Users() {

    }

    public Users(String username, String pwdHash, String pwdAlgorithm, Integer useForwarding, String forwardDestination, Integer useAlias, String alias) {
        this.username = username;
        this.pwdHash = pwdHash;
        this.pwdAlgorithm = pwdAlgorithm;
        this.useForwarding = useForwarding;
        this.forwardDestination = forwardDestination;
        this.useAlias = useAlias;
        this.alias = alias;
    }

    public Users(String username, String pwdHash, String pwdAlgorithm, Integer useForwarding, Integer useAlias) {
        this.username = username;
        this.pwdHash = pwdHash;
        this.pwdAlgorithm = pwdAlgorithm;
        this.useForwarding = useForwarding;
        this.useAlias = useAlias;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwdHash() {
        return pwdHash;
    }

    public void setPwdHash(String pwdHash) {
        this.pwdHash = pwdHash;
    }

    public String getPwdAlgorithm() {
        return pwdAlgorithm;
    }

    public void setPwdAlgorithm(String pwdAlgorithm) {
        this.pwdAlgorithm = pwdAlgorithm;
    }

    public Integer getUseForwarding() {
        return useForwarding;
    }

    public void setUseForwarding(Integer useForwarding) {
        this.useForwarding = useForwarding;
    }

    public String getForwardDestination() {
        return forwardDestination;
    }

    public void setForwardDestination(String forwardDestination) {
        this.forwardDestination = forwardDestination;
    }

    public Integer getUseAlias() {
        return useAlias;
    }

    public void setUseAlias(Integer useAlias) {
        this.useAlias = useAlias;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String toString() {
        return "Users{" +
                "username='" + username + '\'' +
                ", pwdHash='" + pwdHash + '\'' +
                ", pwdAlgorithm='" + pwdAlgorithm + '\'' +
                ", useForwarding=" + useForwarding +
                ", forwardDestination='" + forwardDestination + '\'' +
                ", useAlias=" + useAlias +
                ", alias='" + alias + '\'' +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return username;
    }
}

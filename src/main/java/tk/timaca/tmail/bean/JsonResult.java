package tk.timaca.tmail.bean;

import tk.timaca.tmail.enums.ResultEnum;

/**
 * Created by dede on 2017/3/19.
 * 接口请求接收数据对象
 *
 */
public class JsonResult {
    /**
     * 状态码
     */
    private int code;
    /**
     * 返回结果
     */
    private Object data;

    public JsonResult() {
    }

    public JsonResult(int code, Object data) {
        this.code = code;
        this.data = data;
    }

    public JsonResult(ResultEnum resultEnum){
        this.code = resultEnum.getCode();
        this.data = resultEnum.getMsg();
    }

    public JsonResult(Object data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "JsonResult{" +
                "code=" + code +
                ", data=" + data +
                '}';
    }
}

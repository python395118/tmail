package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/3/31.
 * http请求实体
 */
@TableName(value = "http_log")
public class HttpLog extends Model<HttpLog>{

    /**
     * 主键
     */
    @TableId(value = "http_id")
    private long httpId;

    /**
     * 请求地址
     */
    private String url;

    /**
     * 请求类型
     */
    private String method;

    /**
     * 请求ip
     */
    private String ip;

    /**
     * 请求方法
     */
    @TableField(value = "class_method")
    private String classMethod;

    /**
     * 请求参数
     */
    private String args;

    /**
     * 请求时间戳
     */
    private long timestamp;

    public HttpLog() {
    }

    public HttpLog(long httpId, String url, String method, String ip, String classMethod, String args, long timestamp) {
        this.httpId = httpId;
        this.url = url;
        this.method = method;
        this.ip = ip;
        this.classMethod = classMethod;
        this.args = args;
        this.timestamp = timestamp;
    }

    public long getHttpId() {
        return httpId;
    }

    public void setHttpId(long httpId) {
        this.httpId = httpId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getClassMethod() {
        return classMethod;
    }

    public void setClassMethod(String classMethod) {
        this.classMethod = classMethod;
    }

    public String getArgs() {
        return args;
    }

    public void setArgs(String args) {
        this.args = args;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "HttpLog{" +
                "httpId=" + httpId +
                ", url='" + url + '\'' +
                ", method='" + method + '\'' +
                ", ip='" + ip + '\'' +
                ", classMethod='" + classMethod + '\'' +
                ", args='" + args + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    @Override
    protected Serializable pkVal() {
        return this.httpId;
    }
}

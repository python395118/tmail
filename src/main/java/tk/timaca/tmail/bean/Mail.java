package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/3/21.
 * 邮件实体类
 */
@TableName(value = "mail")
public class Mail extends Model<Mail>{

    /*主键*/
    @TableId(value = "mail_id")
    private Long mailId;

    /*收件人*/
    @TableField(value = "to_mail")
    private String toMail;

    /*发件人*/
    @TableField(value = "from_mail")
    private String fromMail;

    /*主题*/
    private String subject;

    /*抄送*/
    private String wcc;

    /*密送*/
    private String bcc;

    /*内容*/
    private String content;

    /*时间戳*/
    private Long timestamp;

    /*是否被读*/
    @TableField(value = "flag_read")
    private Integer flagRead;

    /*邮件类型*/
    private Integer type;

    /*用户id*/
    @TableField(value = "user_id")
    private Long userId;

    public Mail() {
    }

    public Mail(Long mailId, String toMail, String fromMail, String subject, String wcc, String bcc, String content, Long timestamp, Integer flagRead, Integer type, Long userId) {
        this.mailId = mailId;
        this.toMail = toMail;
        this.fromMail = fromMail;
        this.subject = subject;
        this.wcc = wcc;
        this.bcc = bcc;
        this.content = content;
        this.timestamp = timestamp;
        this.flagRead = flagRead;
        this.type = type;
        this.userId = userId;
    }

    public Mail(String toMail, String fromMail, String subject, String content) {
        this.toMail = toMail;
        this.fromMail = fromMail;
        this.subject = subject;
        this.content = content;
    }

    public Long getMailId() {
        return mailId;
    }

    public void setMailId(Long mailId) {
        this.mailId = mailId;
    }

    public String getToMail() {
        return toMail;
    }

    public void setToMail(String toMail) {
        this.toMail = toMail;
    }

    public String getFromMail() {
        return fromMail;
    }

    public void setFromMail(String fromMail) {
        this.fromMail = fromMail;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getWcc() {
        return wcc;
    }

    public void setWcc(String wcc) {
        this.wcc = wcc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getFlagRead() {
        return flagRead;
    }

    public void setFlagRead(Integer flagRead) {
        this.flagRead = flagRead;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "Mail{" +
                "mailId=" + mailId +
                ", toMail='" + toMail + '\'' +
                ", fromMail='" + fromMail + '\'' +
                ", subject='" + subject + '\'' +
                ", wcc='" + wcc + '\'' +
                ", bcc='" + bcc + '\'' +
                ", content='" + content + '\'' +
                ", timestamp=" + timestamp +
                ", flagRead=" + flagRead +
                ", type=" + type +
                ", userId=" + userId +
                '}';
    }

    protected Serializable pkVal() {
        return this.mailId;
    }

}

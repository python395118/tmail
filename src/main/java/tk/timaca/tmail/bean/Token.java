package tk.timaca.tmail.bean;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

/**
 * Created by dede on 2017/3/28.
 * 涉及需要权限操作的令牌表
 */
@TableName(value = "token")
public class Token extends Model<Token> {

    /**
     * 令牌主键
     */
    @TableId(value = "token_id")
    private long tokenId;

    /**
     * 用户昵称
     */
    @TableField(value = "username")
    private String username;

    /**
     * 令牌
     */
    @TableField(value = "Token")
    private String token;

    /**
     * 时间戳
     */
    private long timestamp;

    /**
     * 状态 0为有效，1为过期
     */
    private int state;

    @Override
    protected Serializable pkVal() {
        return this.tokenId;
    }

    public Token() {
    }

    public Token(long tokenId, String username, String token, long timestamp) {
        this.tokenId = tokenId;
        this.username = username;
        this.token = token;
        this.timestamp = timestamp;
    }

    public Token(long tokenId, String username, String token, long timestamp, int state) {
        this.tokenId = tokenId;
        this.username = username;
        this.token = token;
        this.timestamp = timestamp;
        this.state = state;
    }

    public Token(int state) {
        this.state = state;
    }

    public long getTokenId() {
        return tokenId;
    }

    public void setTokenId(long tokenId) {
        this.tokenId = tokenId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "Token{" +
                "tokenId=" + tokenId +
                ", username='" + username + '\'' +
                ", Token='" + token + '\'' +
                ", timestamp=" + timestamp +
                ", state=" + state +
                '}';
    }
}

package tk.timaca.tmail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import tk.timaca.tmail.config.CaptchaConfig;
import tk.timaca.tmail.service.CaptchaService;

import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.util.UUID;

/**
 * Created by dede on 2017/4/9.
 * 用于页面跳转及验证码图片生成
 */
@RequestMapping("/")
@Controller
public class PageController {

    @Autowired
    private CaptchaConfig captchaConfig;

    @Autowired
    private CaptchaService captchaService;

    /*@RequestMapping("/")
    public ModelAndView index() {
        return new ModelAndView("index");
    }*/
    @RequestMapping("inbox.jsp")
    public ModelAndView inbox() {
        return new ModelAndView("inbox");
    }

    @RequestMapping("index.jsp")
    public ModelAndView index(){
        return new ModelAndView("index");
    }

    /*@RequestMapping("index.html")
    public ModelAndView indexHtml(){
        return new ModelAndView("404");
    }

    @RequestMapping("inbox.html")
    public ModelAndView inboxHtml(){
        return new ModelAndView("404");
    }*/

    @RequestMapping(value = "captcha-image")
    public ModelAndView getKaptchaImage(HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.setDateHeader("Expires", 0);
        response.setHeader("Cache-Control",
                "no-store, no-cache, must-revalidate");
        response.addHeader("Cache-Control", "post-check=0, pre-check=0");
        response.setHeader("Pragma", "no-cache");
        response.setContentType("image/jpeg");

        /*生成验证码*/
        String capText = captchaConfig.getKaptchaBean().createText();
//        String capText = AuthUtil.CreateVerificationCode();

        try {
            /*生成uuid*/
            String uuid = UUID.randomUUID().toString().trim();
//             String uuid = StringUtil.randomLong().toString();

//            jedisConfig.getJedis().set(uuid,capText,"NX","EX",60*5);
            captchaService.saveCaptcha(uuid,capText);

            System.out.println("capText: " + capText+"UUID:"+uuid);
//            .set(uuid, capText,60*5, TimeUnit.SECONDS);
            Cookie cookie = new Cookie("captchaCode",uuid);
            response.addCookie(cookie);
        } catch (Exception e) {
            e.printStackTrace();
        }

        BufferedImage bi = captchaConfig.getKaptchaBean().createImage(capText);
        ServletOutputStream out = response.getOutputStream();
        ImageIO.write(bi, "jpg", out);
        try {
            out.flush();
        } finally {
            out.close();
        }
        return null;
    }
}

package tk.timaca.tmail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.service.MailListService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by dede on 2017/5/8.
 * 用户邮箱黑白名单接口
 */
@RequestMapping("/mailList")
@RestController
public class MailListController {

    @Autowired
    private MailListService mailListService;

    @RequestMapping(value = "/getAllBlackList",method = RequestMethod.POST)
    public JsonResult getAllBlackList(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        return mailListService.getAllBlackListByUsername(username);
    }

    @RequestMapping(value = "/getAllWhiteList",method = RequestMethod.POST)
    public JsonResult getAllWhiteList(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        return mailListService.getAllWhiteListByUsername(username);
    }

    @RequestMapping(value = "/addList",method = RequestMethod.POST)
    public JsonResult addList(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String name = map.getOrDefault("name", ParameterEnum.ERROR.getMsg()).toString();
        Integer type = Integer.valueOf(map.getOrDefault("type", ParameterEnum.ERROR.getMsg()).toString());
        return mailListService.addList(name,username,type);
    }

    @RequestMapping(value = "/deleteList",method = RequestMethod.POST)
    public JsonResult deleteList(HttpServletRequest request) {
        Map<String, Object> map = (Map<String, Object>) request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String name = map.getOrDefault("name", ParameterEnum.ERROR.getMsg()).toString();
        return mailListService.deleteList(name, username);
    }
}

package tk.timaca.tmail.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by dede on 2017/4/10.
 */
@Controller
public class ErrorController implements org.springframework.boot.autoconfigure.web.ErrorController {

    @RequestMapping(value="/error")
    public String handleError(){
        return "404";
    }

    @Override
    public String getErrorPath() {
        return "404";
    }
}

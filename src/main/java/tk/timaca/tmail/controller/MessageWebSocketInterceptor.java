package tk.timaca.tmail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.HandshakeInterceptor;
import tk.timaca.tmail.service.TokenService;

import java.util.Map;

/**
 * Created by dede on 2017/4/17.
 */
@Component
public class MessageWebSocketInterceptor implements HandshakeInterceptor {

    @Autowired
    private TokenService tokenService;

    @Override
    public boolean beforeHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Map<String, Object> attributes) throws Exception {
        System.out.println("666666666666666666666666666666666666666666666666666666");

        if (request instanceof ServletServerHttpRequest) {
            ServletServerHttpRequest servletRequest = (ServletServerHttpRequest) request;
            String username = servletRequest.getServletRequest().getParameter("username");
            String token = servletRequest.getServletRequest().getParameter("token");
            System.out.println(username+"::::"+token);
            if (username != null && token != null) {
                if(tokenService.checkToken(token,username)){
                    attributes.put("username", username);
                    attributes.put("token", token);
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }else{

        }
        return true;
    }

    @Override
    public void afterHandshake(ServerHttpRequest request, ServerHttpResponse response, WebSocketHandler wsHandler, Exception exception) {

    }
}

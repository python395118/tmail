package tk.timaca.tmail.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.service.CaptchaService;
import tk.timaca.tmail.service.TokenService;
import tk.timaca.tmail.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by dede on 2017/3/19.
 */
@RequestMapping(value = "/user")
@Api(description = "用户个人信息操作接口")
@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private CaptchaService captchaService;

    @Autowired
    private TokenService tokenService;

    @RequestMapping(value = "login",method = RequestMethod.POST)
    @ApiOperation(value = "登录")
    public JsonResult login(HttpServletRequest request) throws Exception{
        JsonResult jsonResult = new JsonResult();
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String password=map.getOrDefault("password", ParameterEnum.NO_PASSWORD).toString();
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
        if(tokenService.checkIpCanLoginOrReg(request.getRemoteAddr())){
            jsonResult = userService.isLogin(username,password);
            request.setAttribute("jsonResult",jsonResult);
        }else{
            return new JsonResult(ResultEnum.ERROR.getCode(),ParameterEnum.LOGIN_OR_REG_ERROR_TEXT.getMsg());
        }

        return jsonResult;
    }

    @RequestMapping(value = "reg",method = RequestMethod.POST)
    @ApiOperation(value = "注册")
    public JsonResult reg(HttpServletRequest request) throws Exception{
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String password=map.getOrDefault("password", ParameterEnum.NO_PASSWORD).toString();
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
        if(tokenService.checkIpCanLoginOrReg(request.getRemoteAddr())){
            return userService.createUser(username,password);
        }else{
            return new JsonResult(ResultEnum.ERROR.getCode(),ParameterEnum.LOGIN_OR_REG_ERROR_TEXT.getMsg());
        }
    }

    @RequestMapping(value = "/changepw",method = RequestMethod.POST)
    @ApiOperation(value = "更改密码")
    public JsonResult changePassword(HttpServletRequest request) throws Exception{
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
        String oldPassword=map.getOrDefault("password",ParameterEnum.NO_PASSWORD).toString();
        String newPassword=map.getOrDefault("newPassword",ParameterEnum.NO_PASSWORD).toString();
        return userService.UpdatePassword(username,oldPassword,newPassword);
    }

    @RequestMapping(value = "/logout/{username}",method = RequestMethod.POST)
    @ApiOperation(value = "退出用户")
    public JsonResult logout(@PathVariable("username") String username){
        return userService.logout(username);
    }

    @RequestMapping(value = "forgetpw",method = RequestMethod.POST)
    @ApiOperation(value = "忘记密码")
    public JsonResult forgetPassword(HttpServletRequest request){
        return new JsonResult();
    }

    @RequestMapping(value = "/getUserInfo")
    @ApiOperation(value = "获取用户信息")
    public JsonResult getUserInfo(HttpServletRequest request) throws Exception{
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
        return userService.getUserInfo(username);
    }

    @RequestMapping(value = "/updateNickname",method = RequestMethod.POST)
    public JsonResult updateNickname(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String nickname = map.getOrDefault("nickname",ParameterEnum.ERROR.getMsg()).toString();
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME.getMsg()).toString();
        return userService.updateNickname(username,nickname);
    }

    @RequestMapping(value = "/checkCapText",method = RequestMethod.POST)
    public JsonResult checkCapText(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String uuid = map.getOrDefault("uuid",ParameterEnum.ERROR).toString();
        String capText = map.getOrDefault("capText",ParameterEnum.ERROR).toString();
        return captchaService.checkCapTextForeign(uuid,capText);
    }

    @RequestMapping(value = "/isExists",method = RequestMethod.POST)
    public JsonResult isExists(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME.getMsg()).toString();
        return userService.isExistsByUsername(username);
    }

    @RequestMapping(value = "/updateAvatar",method = RequestMethod.POST)
    public JsonResult updateAvatar(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME.getMsg()).toString();
        String avatarPath=map.getOrDefault("avatarPath",ParameterEnum.ERROR.getMsg()).toString();
        return userService.updateAvatar(username,avatarPath);
    }

    @RequestMapping(value = "/updateBgPath",method = RequestMethod.POST)
    public JsonResult updateBgPath(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username=map.getOrDefault("username",ParameterEnum.NO_USERNAME.getMsg()).toString();
        String bgPath=map.getOrDefault("bgPath",ParameterEnum.ERROR.getMsg()).toString();
        return userService.updateBgPath(username,bgPath);
    }
}

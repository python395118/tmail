package tk.timaca.tmail.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.service.ContactsService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by dede on 2017/4/25.
 * 通讯录操作接口
 */
@RequestMapping("/contacts")
@RestController
public class ContactsController {

    @Autowired
    private ContactsService contactsService;

    @RequestMapping(value = "/addContacts",method = RequestMethod.POST)
    public JsonResult addContacts(HttpServletRequest request) throws Exception{
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String name = map.getOrDefault("name", ParameterEnum.ERROR.getMsg()).toString();
        String otherUsername = map.getOrDefault("otherUsername", ParameterEnum.ERROR.getMsg()).toString();
        String remarks = map.getOrDefault("remarks", otherUsername).toString();
        String address = map.getOrDefault("address", "").toString();
        String phone = map.getOrDefault("phone", "").toString();
        return  contactsService.addContacts(username,otherUsername,name,remarks,address,phone);
    }

    @RequestMapping(value = "/removeContacts",method = RequestMethod.POST)
    public JsonResult removeContacts(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String name = map.getOrDefault("name", ParameterEnum.ERROR.getMsg()).toString();
        String otherUsername = map.getOrDefault("otherUsername", ParameterEnum.ERROR.getMsg()).toString();
        return  contactsService.removeContacts(username,name,otherUsername);
    }

    @RequestMapping(value = "/addContactsGroup",method = RequestMethod.POST)
    public JsonResult addContactsGroup(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String name = map.getOrDefault("name", ParameterEnum.ERROR.getMsg()).toString();
        return  contactsService.addContactsGroup(username,name);
    }

    @RequestMapping(value = "/removeContactsGroup",method = RequestMethod.POST)
    public JsonResult removeContactsGroup(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String name = map.getOrDefault("name", ParameterEnum.ERROR.getMsg()).toString();
        return  contactsService.removeContactsGroup(username,name);
    }

    @RequestMapping(value = "/getContacts",method = RequestMethod.POST)
    public JsonResult getContacts(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        return contactsService.getContactsByUsername(username);
    }

    @RequestMapping(value = "/updateContacts",method = RequestMethod.POST)
    public JsonResult updateContacts(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String groupName = map.getOrDefault("groupName", ParameterEnum.ERROR.getMsg()).toString();
        String otherUsername = map.getOrDefault("otherUsername", ParameterEnum.ERROR.getMsg()).toString();
        String remarks = map.getOrDefault("remarks", ParameterEnum.ERROR.getMsg()).toString();
        String address = map.getOrDefault("address", ParameterEnum.ERROR.getMsg()).toString();
        String phone = map.getOrDefault("phone", ParameterEnum.ERROR.getMsg()).toString();
        String updatedGroupName = map.getOrDefault("updatedGroupName", ParameterEnum.ERROR.getMsg()).toString();
        return contactsService.updateContacts(username,groupName,otherUsername,remarks,address,phone,updatedGroupName);
    }

    @RequestMapping(value = "/updateContactsGroup",method = RequestMethod.POST)
    public JsonResult updateContactsGroup(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String groupName = map.getOrDefault("groupName", ParameterEnum.ERROR.getMsg()).toString();
        String afterGroupName = map.getOrDefault("afterGroupName", ParameterEnum.ERROR.getMsg()).toString();
        return contactsService.updateContactsGroup(username,groupName,afterGroupName);
    }

    @RequestMapping(value = "/getContact",method = RequestMethod.POST)
    public JsonResult getContact(HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username", ParameterEnum.ERROR.getMsg()).toString();
        String groupName = map.getOrDefault("groupName", ParameterEnum.ERROR.getMsg()).toString();
        String otherUsername = map.getOrDefault("otherUsername", ParameterEnum.ERROR.getMsg()).toString();
        return contactsService.getContact(username,groupName,otherUsername);
    }
}

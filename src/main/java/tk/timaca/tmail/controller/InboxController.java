package tk.timaca.tmail.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.service.InboxService;
import tk.timaca.tmail.service.MailService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by dede on 2017/3/22.
 * 邮箱操作接口
 */
@RestController(value = "/inbox")
@Api(description = "邮箱信息操作接口")
public class InboxController {

    @Autowired
    private InboxService inboxService;

    @Autowired
    private MailService mailService;

    @RequestMapping(value = "/{token}/getInbox",method = RequestMethod.POST)
    @ApiOperation(value = "获取用户邮箱列表")
    public JsonResult getInbox(@PathVariable String token, HttpServletRequest request) throws Exception{
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        Integer type = Integer.valueOf(map.getOrDefault("type", ParameterEnum.ERROR.getMsg()).toString());
        Integer whitchPage = Integer.valueOf(map.getOrDefault("whitchPage",ParameterEnum.ERROR.getMsg()).toString());
        Integer max = Integer.valueOf(map.getOrDefault("max",ParameterEnum.ERROR.getMsg()).toString());
        String listType = map.getOrDefault("listType",ParameterEnum.INBOX_LIST_BY_TIME.getMsg()).toString();
        return mailService.findMailByTokenFromMail(token,type,whitchPage,max,listType);
    }

    @RequestMapping(value = "/{token}/getInboxInfo",method = RequestMethod.POST)
    public JsonResult getInboxInfo(@PathVariable String token, HttpServletRequest request){
//        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        return mailService.getInboxInfo(token);
    }

    /*@RequestMapping(value = "SelectOne")
    public JsonResult SelectOne(HttpServletRequest request){
        return inboxService.SelectOne();
    }

    @RequestMapping(value = "parse")
    public void Parse(HttpServletRequest request){
        inboxService.ParseMessageBody();
    }*/
}

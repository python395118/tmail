package tk.timaca.tmail.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

/**
 * Created by dede on 2017/4/19.
 */
@RequestMapping(value = "/upload")
@RestController
public class UploadController {

    @RequestMapping(value = "/file",method = RequestMethod.POST)
    public String uploadImg(HttpServletRequest request) throws IOException{
        /*File file1=null;
        BufferedOutputStream stream = null;
        String src = "";
        byte[] bytes = new byte[2048];
        InputStream inputStream = request.getInputStream();
        int i = 0;
        while((bytes[i] = (byte)inputStream.read())!=-1){
            i++;
        }
        request.getInputStream().read();
        // 判断文件是否为空
//        if (!file.isEmpty()) {
            try {


//                src = "uploadImg/"+StringUtil.randomLong()+file.getOriginalFilename();
                src = "uploadImg/"+ StringUtil.randomLong()+".jpg";
                file1=new File(ParameterEnum.UPLOAD_IMAGE_BASE_PATH.getMsg()+src);
                System.out.println(file1.getAbsolutePath()+file1.exists());
                if(!file1.exists()){
//                    file1.mkdir();
                    file1.createNewFile();
                    System.out.println(file1.getAbsolutePath()+file1.exists());
                }else{}
//                bytes = file.getBytes();
                stream = new BufferedOutputStream(new FileOutputStream(file1 ));
                stream.write(bytes);
                stream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
//        }*/
        String src ="";
        if(request.getHeader("Content-Type") != null && request.getHeader("Content-Type").indexOf("multipart/form-data") != -1)
        {
            //将request变成多部分request
            MultipartHttpServletRequest multiRequest=(MultipartHttpServletRequest)request;
            //获取multiRequest 中所有的文件名
            Iterator iter=multiRequest.getFileNames();

            while(iter.hasNext())
            {
                //一次遍历所有文件
                MultipartFile file=multiRequest.getFile(iter.next().toString());
                if(file!=null)
                {
                    src = "uploadImg/"+file.getOriginalFilename();
                    File file1=new File(src);
                    System.out.println("上传文件的真实路径：：："+file1.getAbsolutePath());
                    file1.createNewFile();
                    byte[] bytes =  file.getBytes();
                    BufferedOutputStream stream = null;
                    stream = new BufferedOutputStream(new FileOutputStream(file1));
                    stream.write(bytes);
                    stream.close();
                }

            }

        }
        return src;
    }
}

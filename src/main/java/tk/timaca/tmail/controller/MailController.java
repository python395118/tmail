package tk.timaca.tmail.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import tk.timaca.tmail.bean.JsonResult;
import tk.timaca.tmail.bean.Mail;
import tk.timaca.tmail.enums.MailTypeEnum;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.service.MailService;
import tk.timaca.tmail.service.MessageWebSocketHandler;
import tk.timaca.tmail.service.TokenService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * Created by dede on 2017/4/6.
 * 邮件接口
 */
@RestController(value = "/Mail")
@Api(description = "邮件信息操作接口")
public class MailController {

    @Autowired
    private MailService mailService;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private MessageWebSocketHandler messageWebSocketHandler;

    @RequestMapping("/{user}/saveMail")
    @ApiOperation(value = "保存草稿邮件")
    public JsonResult saveMail(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String from = map.getOrDefault("from",ParameterEnum.NO_FROM).toString();
        String to = map.getOrDefault("to",ParameterEnum.NO_TO).toString();
        String subject = map.getOrDefault("subject",ParameterEnum.NO_SUBJECT).toString();
        String text = map.getOrDefault("text",ParameterEnum.NO_TEXT).toString();
        String cc = map.getOrDefault("cc",ParameterEnum.ERROR.getMsg()).toString();
        String bcc = map.getOrDefault("bcc",ParameterEnum.ERROR.getMsg()).toString();
        String mailAnnex = map.getOrDefault("mail_annex",ParameterEnum.ERROR.getMsg()).toString();
        Mail newMail = new Mail(to,from,subject,text);
        newMail.setBcc(bcc);
        newMail.setWcc(cc);
        newMail.setType(MailTypeEnum.DRAFTS.getType());
        return mailService.saveDraftsMail(newMail,mailAnnex);
    }

    @RequestMapping("/{user}/delMail")
    @ApiOperation(value = "删邮件")
    public JsonResult delMail(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        Long mailId = Long.valueOf(map.getOrDefault("mailId",ParameterEnum.NO_MAIL_ID.getMsg()).toString());
        Integer afterType  = Integer.valueOf(map.getOrDefault("afterType",ParameterEnum.ERROR.getMsg()).toString());
        return mailService.removeMail(mailId,afterType);
    }

    @RequestMapping(value = "/{user}/searchMail",method = RequestMethod.POST)
    @ApiOperation(value = "查邮件")
    public JsonResult searchMail(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String key = map.getOrDefault("key",ParameterEnum.NO_PARAMS.getMsg()).toString();
        Integer whichPage = Integer.valueOf(map.getOrDefault("whichPage",ParameterEnum.ERROR.getMsg()).toString());
        Integer max = Integer.valueOf(map.getOrDefault("max",ParameterEnum.ERROR.getMsg()).toString());
        String username = map.getOrDefault("username",ParameterEnum.NO_PARAMS.getMsg()).toString();
        String listType = map.getOrDefault("listType",ParameterEnum.INBOX_LIST_BY_TIME.getMsg()).toString();
//        return mailService.findMailByMailId(mailId);
        return mailService.searchMailByKey(key,username,whichPage,max,listType);
    }

    @RequestMapping(value = "/{user}/setRead",method = RequestMethod.POST)
    @ApiOperation(value = "设置邮件为已读邮件")
    public JsonResult setRead(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        Long mailId = Long.valueOf(map.getOrDefault("mailId",ParameterEnum.NO_MAIL_ID.getMsg()).toString());
        return mailService.setRead(mailId);
    }

    @RequestMapping(value = "/{user}/searchMailByContent",method = RequestMethod.POST)
    @ApiOperation(value = "查邮件")
    public JsonResult searchMailByContent(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        Long mailId = Long.valueOf(map.getOrDefault("mailId",ParameterEnum.NO_MAIL_ID.getMsg()).toString());
//        System.out.println(mailId);
        return mailService.findMailByMailId(mailId);
    }

    @RequestMapping("/{user}/updateMail")
    @ApiOperation(value = "修改草稿邮件")
    public JsonResult updateMail(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String from = map.getOrDefault("from",ParameterEnum.NO_FROM).toString();
        String to = map.getOrDefault("to",ParameterEnum.NO_TO).toString();
        String subject = map.getOrDefault("subject",ParameterEnum.NO_SUBJECT).toString();
        String text = map.getOrDefault("text",ParameterEnum.NO_TEXT).toString();
        Integer type = Integer.valueOf(map.getOrDefault("type",ParameterEnum.ERROR).toString());
//        Integer type = 0;
        Long mailId = Long.valueOf(map.getOrDefault("mailId",ParameterEnum.ERROR).toString());
        String cc = map.getOrDefault("cc",ParameterEnum.ERROR.getMsg()).toString();
        String bcc = map.getOrDefault("bcc",ParameterEnum.ERROR.getMsg()).toString();
        String mailAnnex = map.getOrDefault("mail_annex",ParameterEnum.ERROR.getMsg()).toString();
        Mail mail = new Mail(to,from,subject,text);
        mail.setWcc(cc);
        mail.setBcc(bcc);
        mail.setType(type);
        mail.setMailId(mailId);
        return mailService.updateMail(mail,mailAnnex);
    }

    @RequestMapping(value = "/{user}/sendMail",method = RequestMethod.POST)
    @ApiOperation(value = "发送邮件")
    public JsonResult sendMail(@PathVariable String user,HttpServletRequest request) throws Exception{
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String from = map.getOrDefault("from",ParameterEnum.ERROR.getMsg()).toString();
        String to = map.getOrDefault("to",ParameterEnum.ERROR.getMsg()).toString();
        String subject = map.getOrDefault("subject",ParameterEnum.ERROR.getMsg()).toString();
        String text = map.getOrDefault("text",ParameterEnum.ERROR.getMsg()).toString();
        String cc = map.getOrDefault("cc",ParameterEnum.ERROR.getMsg()).toString();
        String bcc = map.getOrDefault("bcc",ParameterEnum.ERROR.getMsg()).toString();
        String mailAnnex = map.getOrDefault("mail_annex",ParameterEnum.ERROR.getMsg()).toString();
        String username = map.getOrDefault("username",ParameterEnum.ERROR.getMsg()).toString();
//        return mailService.SendMail(new Mail(to,from,subject,text));
        if(tokenService.checkIPCanSend(request.getRemoteAddr())){
            return mailService.sendEmail(username,to,cc,bcc,subject,text,mailAnnex);
        }else{
            messageWebSocketHandler.send(username,ParameterEnum.SEND_ERROR_TEXT.getMsg());
            return new JsonResult(ResultEnum.ERROR);
        }

    }

    @RequestMapping(value = "/{user}/count",method = RequestMethod.POST)
    public JsonResult countMailByTokenFromMail(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String token = map.getOrDefault("token",ParameterEnum.NO_TOKEN).toString();
        Integer type = Integer.valueOf(map.getOrDefault("type",ParameterEnum.ERROR).toString());
        return mailService.countMailByTokenFromMail(token,type);
    }

    @RequestMapping(value = "/{user}/countbykey",method = RequestMethod.POST)
    public JsonResult countMailByKey(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
        String key = map.getOrDefault("type",ParameterEnum.ERROR).toString();
        return mailService.countMailByKey(key,username);
    }

    @RequestMapping(value = "/{user}/setReadByType",method = RequestMethod.POST)
    public JsonResult setReadByType(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        String username = map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
        Integer type = Integer.valueOf(map.getOrDefault("type",ParameterEnum.ERROR).toString());
        return mailService.setReadByType(username,type);
    }

    @RequestMapping(value = "/{user}/getMailAnnex")
    public JsonResult getMailAnnex(@PathVariable String user,HttpServletRequest request){
        Map<String,Object> map = (Map<String,Object>)request.getAttribute("map");
        Long mailId = Long.valueOf(map.getOrDefault("mailId",ParameterEnum.NO_MAIL_ID.getMsg()).toString());
        return mailService.getMailAnnexByMailId(mailId);
    }

}

package tk.timaca.tmail.exception;

import tk.timaca.tmail.enums.ResultEnum;

/**
 * Created by dede on 2017/3/31.
 * 定义一个没有权限的异常
 */
public class NoAuthException extends RuntimeException{

    private Integer code;

    public NoAuthException(){}

    public NoAuthException(ResultEnum resultEnum){
        super(resultEnum.getMsg());
        this.code = resultEnum.getCode() ;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}

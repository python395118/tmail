package tk.timaca.tmail.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import tk.timaca.tmail.bean.HttpLog;
import tk.timaca.tmail.config.AuthConfig;
import tk.timaca.tmail.enums.ParameterEnum;
import tk.timaca.tmail.enums.ResultEnum;
import tk.timaca.tmail.exception.NoAuthException;
import tk.timaca.tmail.service.LogService;
import tk.timaca.tmail.service.TokenService;
import tk.timaca.tmail.util.HttpUtil;
import tk.timaca.tmail.util.StringUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.Map;
import java.util.Random;

/**
 * Created by dede on 2017/3/31.
 * 访问权限切面
 */
@Aspect
@Component
public class AuthAspect {

    @Autowired
    private TokenService tokenService;

    @Autowired
    private LogService logService;

    @Autowired
    private AuthConfig authConfig;

    /**
     * 设置切点
     */
    @Pointcut("execution(public * tk.timaca.tmail.controller.*Controller.*(..))")
    public void doPointcut(){
    }

    /**
     * 判断请求是否拥有权限
     */
    @Before("doPointcut()")
    public void doBefore(JoinPoint joinPoint) throws IOException{
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
//        System.out.println(request.getHeader("Content-Type"));
        httpLog(joinPoint,request);
        if(request.getHeader("Content-Type")== null
                || request.getHeader("Content-Type").indexOf("multipart/form-data") == -1){
            /**
             * 解析request
             */
            Map<String,Object> map = HttpUtil.JsonToMap(request);
            /**
             * 将map缓存到request中
             */
            request.setAttribute("map",map);

            String token = map.getOrDefault("token", ParameterEnum.NO_TOKEN).toString();
            String username = map.getOrDefault("username",ParameterEnum.NO_USERNAME).toString();
            String name = joinPoint.getSignature().getName();
        /*获取不需要权限的方法名字符串数组*/
            String[] NoPermissionRequired = authConfig.getNoPermissionRequired();
            if(StringUtil.isContain(NoPermissionRequired,name)){

            }else{
                if (!tokenService.checkToken(token, username)) {
                    throw new NoAuthException(ResultEnum.AUTH_NO);
                }
            }
        }else{}


    }

    /**
     * 记录请求记录
     * @param joinPoint
     * @param request
     */
    private void httpLog(JoinPoint joinPoint,HttpServletRequest request) {

        logService.addLog(new HttpLog(new Random().nextInt(9999999),
                request.getRequestURL().toString(),
                request.getMethod(),
                request.getRemoteAddr(),
                joinPoint.getSignature().getDeclaringTypeName()+":"+joinPoint.getSignature().getName(),
                joinPoint.getArgs().toString(),
                System.currentTimeMillis())
        );
    }
}

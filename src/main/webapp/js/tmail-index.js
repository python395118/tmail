/**
 * Created by dede on 2017/5/8.
 */
var token;
var username;
var login_username = "";
var reg_username = "";
var capCode = "";

$(document).ready(function(){

    $("#loginbtn").attr("disabled","");/*默认情况下登录按钮为disabled*/

    $("#regbtn_commit").attr("disabled","");

    $('.modal').modal({
            dismissible: false, // Modal can be dismissed by clicking outside of the modal
            opacity: 0.5, // Opacity of modal background
            in_duration: 300, // Transition in duration
            out_duration: 200, // Transition out duration
            starting_top: '4%', // Starting top style attribute
            ending_top: '10%', // Ending top style attribute
            ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                //alert("Ready");
                username = $('#account').val();
                var JSONObject = {
                    "username": $('#account').val(),
                    "password": $('#userpw').val(),
                };
                var JSONText = JSON.stringify(JSONObject);
                $.ajax({
                    type: "POST",
                    url: "/user/login",
                    data: JSONText,
                    contentType:"application/json",
                    dataType: "json",
                    success: function(data){
//        					alert("数据: \n" + data.data.token );

                        if(data.code == 0) {
                            token = data.data.token;
                            $('#modal1').modal('close');
                            //alert("登录成功");
                            var $toastContent = $('<span>登录成功</span>');
                            Materialize.toast($toastContent, 5000);
                            window.location.href="/inbox.jsp?t="+token+"&u="+ $('#account').val();
                            /*$.ajax({
                             type: "GET",
                             url: "/inbox.jsp",
                             success: function(inboxdata){
                             $("*").html(inboxdata);

                             }
                             });*/

//                                        window.location.href = "/inbox/"+$('#account').val()+"/getInbox";
                        }else{
                            if(data.data==""){
                                var $toastContent = $('<span>登录失败</span>');
                                Materialize.toast($toastContent, 5000);
                                $('#toast-container').attr("style","top:0;");
                            }else{
                                var $toastContent = $('<span>'+data.data+'</span>');
                                Materialize.toast($toastContent, 5000);
                                $('#toast-container').attr("style","top:0;");
                            }
                            $('#modal1').modal('close');
                        }
                    }
                });
                /*$.post("/login",
                 JSONObject,
                 function(data,status){
                 //        					alert("数据: \n" + data + "\n状态: " + status);
                 if(status=="success"){
                 obj=eval("("+data+")");
                 alert(obj.code);
                 if(obj.code == 0) {
                 $('#modal1').modal('close');
                 //alert("登录成功");
                 var $toastContent = $('<span>登录成功</span>');
                 Materialize.toast($toastContent, 5000);
                 //$("*").html(data);
                 //                                        window.location.href = 'login?id=' + data;
                 }
                 }
                 else{
                 $('#modal1').modal('close');
                 var $toastContent = $('<span>登录失败</span>');
                 //$('.tooltipped').tooltip({position:'top'});
                 Materialize.toast($toastContent, 5000);
                 $('#toast-container').attr("style","top:0;");
                 //alert("登录失败");
                 }
                 });*/
                console.log(modal, trigger);
            },
            complete: function() {
                //alert('Closed');
            } // Callback for Modal close
        }
    );
});
$('#loginbtn').click(function(){
    $('#modal1').modal('open');
    //loadXMLDoc();
});

$('.switch').click(function(){
    var ischecked=$(":checkbox")[0].checked;//是否被选中
    if(ischecked==true){
        $('#userpw').attr("type","text");
        $('#reg_userpw').attr("type","text");
        $('#reg_userpw_again').attr("type","text");
    }
    else if(ischecked==false){
        $('#userpw').attr("type","password");
        $('#reg_userpw').attr("type","password");
        $('#reg_userpw_again').attr("type","password");
    }
    else{}
});

$("#regbtn").click(function () {
    $("#tmailIndex").attr("style","display:none");
    $("#tmailreg").attr("style","display:block");
});

$("#returnloginbtn").click(function () {
    $("#tmailIndex").attr("style","display:block");
    $("#tmailreg").attr("style","display:none");
});

$("#changepwbtn").click(function () {
    $("#tmailIndex").attr("style","display:none");
    $("#tmailchangepw").attr("style","display:block");
});

$("#returnloginbtn1").click(function () {
    $("#tmailIndex").attr("style","display:block");
    $("#tmailchangepw").attr("style","display:none");
});

$("#regbtn_commit").click(function () {

    var JSONObject = {
        "username": $("#reg_account").val(),
        "password":$("#reg_userpw").val(),
//            "newPassword":$("#reg_userpw_again").val(),
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "/user/reg",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
//        					alert("数据: \n" + data.data.token );

            if(data.code == 5) {
                //alert("登录成功");
                var $toastContent = $('<span>注册成功</span>');
                Materialize.toast($toastContent, 5000);
            }else{
                if(data.data==""){
                    var $toastContent = $('<span>注册失败</span>');
                    Materialize.toast($toastContent, 5000);
                    $('#toast-container').attr("style","top:0;");
                }else{
                    var $toastContent = $('<span>'+data.data+'</span>');
                    Materialize.toast($toastContent, 5000);
                    $('#toast-container').attr("style","top:0;");
                }
            }
        }
    });
});

$("#codeimg").click(function () {
    $(this).attr("src","/captcha-image?"+Math.random());
});

$("#account").blur(function () {
    var beforeclass = $('#account').parent().attr("class");

    if($('#account').val()!=""){
        var JSONObject = {
            "username": $('#account').val(),
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/user/isExists",
            data: JSONText,
            contentType:"application/json",
            dataType: "json",
            success: function(data){
                if(data.code == 200) {
                    /*var $toastContent = $('<span>用户不存在</span>');
                    Materialize.toast($toastContent, 5000);*/
                    $("#account").next().text("用户不存在");
                    $('#account').parent().attr("class",beforeclass+" mdui-textfield-invalid");
                    $("#loginbtn").attr("disabled","");
                }else if(data.code == 404){
                    $('#account').parent().attr("class",beforeclass);
                    $("#account").next().text("用户名不能为空");
                }
            }
        });
    }else{
        $("#account").next().text("用户名不能为空");
    }
});

$("#reg_account").blur(function () {
    var beforeclass = $('#reg_account').parent().attr("class");

    if($('#reg_account').val()!=""){
        var JSONObject = {
            "username": $('#reg_account').val(),
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/user/isExists",
            data: JSONText,
            contentType:"application/json",
            dataType: "json",
            success: function(data){
                if(data.code == 200) {
                    /*var $toastContent = $('<span>用户不存在</span>');
                     Materialize.toast($toastContent, 5000);*/
                    $('#reg_account').parent().attr("class",beforeclass);
                    $("#reg_account").next().text("用户名不能为空");
                }else if(data.code == 404){
                    $("#reg_account").next().text("用户名已经存在");
                    $('#reg_account').parent().attr("class",beforeclass+" mdui-textfield-invalid");
                    $("#regbtn_commit").attr("disabled","");
                }
            }
        });
    }else{
        $("#account").next().text("用户名不能为空");
    }
});

$("#reg_userpw").blur(function () {
    var beforeclass = $('#reg_userpw').parent().attr("class");
    if($("#reg_userpw").val()!=$("#reg_userpw_again").val()){
        $("#reg_userpw").next().text("两次输入的密码不同");
        $('#reg_userpw').parent().attr("class",beforeclass+" mdui-textfield-invalid");
    }else{
        $("#reg_userpw").next().text("密码至少 6 位，且包含大小写字母");
        $('#reg_userpw').parent().attr("class",beforeclass);
    }
});

$("#reg_userpw_again").blur(function () {
    var beforeclass = $('#reg_userpw_again').parent().attr("class");
    if($("#reg_userpw").val()!=$("#reg_userpw_again").val()){
        $("#reg_userpw_again").next().text("两次输入的密码不同");
        $('#reg_userpw_again').parent().attr("class",beforeclass+" mdui-textfield-invalid");
    }else{
        $("#reg_userpw_again").next().text("密码至少 6 位，且包含大小写字母");
        $('#reg_userpw_again').parent().attr("class",beforeclass);
    }
});

$("#reg_userpw_again").blur(function () {
    var beforeclass = $('#reg_userpw_again').parent().attr("class");
    if($("#reg_userpw").val()!=$("#reg_userpw_again").val()){
        $("#reg_userpw_again").next().text("两次输入的密码不同");
        $('#reg_userpw_again').parent().attr("class",beforeclass+" mdui-textfield-invalid");
    }else{
        $("#reg_userpw_again").next().text("密码至少 6 位，且包含大小写字母");
        $('#reg_userpw_again').parent().attr("class",beforeclass);
    }
});

$("#code").blur(function () {
    var beforeclass = $('#code').parent().attr("class");
    if($("#code").val()!=capCode) {
        capCode == $("#code").val();
        var JSONObject = {
            "uuid": $.cookie("captchaCode"),
            "capText": $("#code").val(),
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/user/checkCapText",
            data: JSONText,
            contentType: "application/json",
            dataType: "json",
            success: function (data) {
                if (data.code == 200) {
                    // $("#regbtn_commit").removeAttr("disabled");
                    $("#code").next().text("验证码不能为空");
                    $('#code').parent().attr("class",beforeclass);
                } else if (data.code == 404) {
                    /*var $toastContent = $('<span>验证码错误</span>');
                    Materialize.toast($toastContent, 5000);
                    $("#regbtn_commit").attr("disabled", "");*/
                    $("#code").next().text("验证码错误");
                    $('#code').parent().attr("class",beforeclass+" mdui-textfield-invalid");

                }
            }
        });
    }
});

$("input[name='login_from']").keyup(function () {
    if(document.getElementById("userpw").checkValidity()&&document.getElementById("account").checkValidity()){
        $("#loginbtn").removeAttr("disabled");
    }else{
        $("#loginbtn").attr("disabled","");/*默认情况下登录按钮为disabled*/
    }
});

$("input[name='reg_from']").keyup(function () {
    if(document.getElementById("reg_userpw").checkValidity()&&document.getElementById("reg_account").checkValidity()
        &&document.getElementById("reg_userpw_again").checkValidity()&&document.getElementById("code").checkValidity()){
        $("#regbtn_commit").removeAttr("disabled");
    }else{
        $("#regbtn_commit").attr("disabled","");/*默认情况下登录按钮为disabled*/
    }
});

/*
$("input[name='login_from']").change(function () {
    if($('#account').val()!=""&&$('#userpw').val()!=""){
        if($('#account').val()!=login_username){
            login_username=$('#account').val();
            var JSONObject = {
                "username": $('#account').val(),
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/user/isExists",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        var $toastContent = $('<span>用户不存在</span>');
                        Materialize.toast($toastContent, 5000);
                        $("#loginbtn").attr("disabled","");
                    }else if(data.code == 404){
                        $("#loginbtn").removeAttr("disabled");

                    }
                }
            });
        }
    }else{
        $("#loginbtn").attr("disabled","");
    }
});
*/
/*
$("input[name='reg_from']").blur(function () {
    if($('#reg_account').val()!=""&&$('#reg_userpw').val()!=""
        && $("#reg_userpw").val()==$("#reg_userpw_again").val() && $("#code").val()!=""){
        if($('#reg_account').val()!=reg_username){
            reg_username=$('#reg_account').val();
            var JSONObject = {
                "username": $('#reg_account').val(),
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/user/isExists",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        $("#regbtn_commit").removeAttr("disabled");
                    }else if(data.code == 404){
                        var $toastContent = $('<span>用户名已经存在</span>');
                        Materialize.toast($toastContent, 5000);
                        $("#regbtn_commit").attr("disabled","");

                    }
                }
            });
        }
        if($("#code").val()!=capCode){
            capCode == $("#code").val();
            var JSONObject = {
                "uuid": $.cookie("captchaCode"),
                "capText":$("#code").val(),
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/user/checkCapText",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        $("#regbtn_commit").removeAttr("disabled");
                    }else if(data.code == 404){
                        var $toastContent = $('<span>验证码错误</span>');
                        Materialize.toast($toastContent, 5000);
                        $("#regbtn_commit").attr("disabled","");

                    }
                }
            });
        }
    }else{
        $("#regbtn_commit").attr("disabled","");
    }
});*/

/*	function loadXMLDoc()
 {
 JSONObject = {
 "ip": $('#userip').val(),
 "key": $('#userkey').val(),
 };
 JSONText = JSON.stringify(JSONObject);
 //AJAX script goes here ...
 var xmlhttp;
 if (window.XMLHttpRequest){// code for IE7+, Firefox, Chrome, Opera, Safari
 xmlhttp=new XMLHttpRequest();
 }else{// code for IE6, IE5
 xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
 }
 xmlhttp.open("POST","sinaapi",true);
 xmlhttp.send(JSONText);
 alert(xmlhttp.responseText);

 resultJson=xmlhttp.responseText;
 if (xmlhttp.readyState==4 && xmlhttp.status==200){
 resultJson=xmlhttp.responseText;
 alert(xmlhttp.responseText);
 }

 }*/
/**
 * Created by dede on 2017/5/8.
 */
//	Logfirst=0;
//	ReptileLogfirst=0;
//	SinaArtifirst=0;
//	max=10;
//	ObjectName='SinaArti';
//	by=0;

var mailList;
var username;
var token;
var sockjs;
var key;/*关键字检索*/
var type = 2;/*邮件类型 如果type为100则表示用户在检索*/
var contacts_html;
var contacts_group_html;
var default_tmail_contacts;
var send_mail_File = new Array();
var ajaxArray = new Array();
var history_hash ="";
var prev_history_hash = "";
var editor;
var static_mailId;
var isSearch = 1;/*搜索标识，>1则允许向后台发送搜索请求*/
var isToast = 1;/*操作结果标识，>1则允许显示操作结果*/
/*监听当前邮件类型*/
/*var data = {type:2};
Object.defineProperty(data, "type", {
    set: function(value) {
        console.log(value);
        list_type = "时间";
    }
});*/
var list_type = "时间";

$(document).ready(function(){

//	    $('#p').text(token);
    $('.modal').modal();
    $(".button-collapse").sideNav();
//		$('.button-collapse').sideNav('show');
    $('ul.tabs').tabs();
    editor = new wangEditor('text');
    editor.config.uploadImgUrl = "/upload/file";
    editor.create();
    $('.collapsible').collapsible();
    $('.scrollspy').scrollSpy();

    $("#mailbodydiv div:first").attr("style","display:block");
    cardhtml = $("#mailbodydiv").html();
    /*contacts_html = '<li class="mdui-list-item mdui-ripple">'
                        +'<div class="mdui-list-item-avatar"><img src="avatar1.jpg"/></div>'
                        +'<div class="mdui-list-item-content">Brendan Lim</div>'
                    +'<button class="mdui-btn mdui-btn-icon"><i class="mdui-icon material-icons">&#xe163;</i></button>'
                    +'</li>';
    contacts_group_html = '<li class="mdui-subheader-inset">Friends</li>';*/
    // alert(cardhtml);
    $("#mailbodydiv div:first").attr("style","display:none");

    var showmailbody = $("#showmailbody").html();

//	    GetData(10,0,'ReptileLog');
//	    GetData(10,0,'Log');
    //update();
    token = GetQueryString("t");
    username = GetQueryString("u");
    $('title').text(username+"的个人ttmail邮箱");

    $("a[href='#!email'] span").text(username+"@ttmail.ml")

    var JSONObject = {
        "username": username,
        "token": token,
        "type":2,
        "whitchPage":1,
        "max":10,
    };
    var JSONText = JSON.stringify(JSONObject);
    getmail(JSONText,token+"/getInbox");

    var userInfoJSONObject = {
        "username": username,
    };
    var userInfoJSONText = JSON.stringify(userInfoJSONObject)

    $.ajax({
        type: "POST",
        url: "/user/getUserInfo",
        data: userInfoJSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                $("a[href='#!user'] img").attr("src",data.data.avatar_path);
                $("a[href='#!name'] span").text(data.data.nick_name);
                $("#set_nickname").val(data.data.nick_name);
                $("#set_img").attr("src",data.data.avatar_path);

                setbgPath($("#set_bg").attr("src"),data.data.bg_path);
                $.adaptiveBackground.run();
            }
        }
    });

    $(".waves-effect").each(function () {
        $(this).bind('click',function() {

            /*$("#editorbtn").attr("style","display:block");
            $("#readmore_view").attr("style","display:none");
            $("#main_view").attr("style","display:block");
*/
            var typename = $(this).text();
//                alert($(this).html+$(this).text());
            if(typename.indexOf("收件箱") != -1){
                type = 2;
                var getmailJSONObject = {
                    "username": username,
                    "token": token,
                    "type": type,
                    "whitchPage":1,
                    "max":10,
                };
                var getmailJSONText = JSON.stringify(getmailJSONObject);
                getmail(getmailJSONText,token+"/getInbox");

                history_hash = "incoming";
            }else if(typename.indexOf("草稿箱") != -1){
                type = 0;
                var getmailJSONObject = {
                    "username": username,
                    "token": token,
                    "type": type,
                    "whitchPage":1,
                    "max":10,
                };
                var getmailJSONText = JSON.stringify(getmailJSONObject);
                getmail(getmailJSONText,token+"/getInbox");

                history_hash = "drafts";
            }else if(typename.indexOf("已发送") != -1){
                type = 1;
                var getmailJSONObject = {
                    "username": username,
                    "token": token,
                    "type": type,
                    "whitchPage":1,
                    "max":10,
                };
                var getmailJSONText = JSON.stringify(getmailJSONObject);
                getmail(getmailJSONText,token+"/getInbox");

                history_hash = "send";
            }else if(typename.indexOf("回收站") != -1){
                type = 3;
                var getmailJSONObject = {
                    "username": username,
                    "token": token,
                    "type": type,
                    "whitchPage":1,
                    "max":10,
                };
                var getmailJSONText = JSON.stringify(getmailJSONObject);
                getmail(getmailJSONText,token+"/getInbox");

                history_hash = "trash";
            }else if(typename.indexOf("垃圾箱") != -1){
                type = 4;
                var getmailJSONObject = {
                    "username": username,
                    "token": token,
                    "type": type,
                    "whitchPage":1,
                    "max":10,
                };
                var getmailJSONText = JSON.stringify(getmailJSONObject);
                getmail(getmailJSONText,token+"/getInbox");

                history_hash = "spam";
            }else if(typename.indexOf("设置") != -1){

                scale("readmore_view");
                scale("set_view");
                scale("main_view");
                scale("editorbtn");

                setTimeout('$("#readmore_view").attr("style","display:none");' +
                    '$("#set_view").attr("style","display:block");' +
                    '$("#main_view").attr("style","display:none");' +
                    '$("#editorbtn").attr("style","display:none");',500);

                $("#setAllIsRead").attr("disabled","");
                $("#refresh").attr("disabled","");
                $("#list_type").attr("disabled","");
            }else if(typename.indexOf("退出") != -1){
                var logoutJSONObject = {
                    "username": username,
                    "token": token,
                };
                var logoutJSONText = JSON.stringify(logoutJSONObject);

                $.ajax({
                    type: "POST",
                    url: "user/logout/"+username,
                    data: logoutJSONText,
                    contentType:"application/json",
                    dataType: "json",
                    async:false, //等待执行后再执行下面的源码
                    success: function(data){
                        if(data.code == 9) {
                         window.location.href="/index.jsp";
                        }else{
                            setMessageInnerHTML("退出账号失败");
                        }
                    }
                });

            }
            else{
//					type = -1;
            }

        });
    });

    var sockjs_url = "/sockjs/message?username="+username+"&token="+token;
    sockjs = new SockJS(sockjs_url);
    sockjs.onopen    = function()  {/*setMessageInnerHTML(sockjs.protocol);*/};
    sockjs.onmessage = function(e) {setMessageInnerHTML(e.data);};
    sockjs.onclose   = function()  {/*setMessageInnerHTML("CLOSE");*/};
    //监听窗口关闭事件，当窗口关闭时，主动去关闭websocket连接，防止连接还没断开就关闭窗口，server端会抛异常。
    window.onbeforeunload = function(){
        sockjs.close();
    }

    var getInboxInfoJSONObject = {
        "username": username,
        "token": token,
    };
    var getInboxInfoJSONText = JSON.stringify(getInboxInfoJSONObject);

    $.ajax({
        type: "POST",
        url: "/"+token+"/getInboxInfo",
        data: getInboxInfoJSONText,
        contentType:"application/json",
        dataType: "json",
        async:false, //等待执行后再执行下面的源码
        success: function(data){
            if(data.code == 200) {
//                    alert(data.data.drafts);
                $(".new")[0].innerHTML = data.data.incoming;
                $(".new")[1].innerHTML = data.data.drafts;
                $(".new")[2].innerHTML = data.data.send;
                if(data.data.incoming == '0'){
                    $(".new")[0].setAttribute("style","display:none");
                }else{
                    $(".new")[0].setAttribute("style","display:block");
                }
                if(data.data.drafts == '0'){
                    $(".new")[1].setAttribute("style","display:none");
                }else{
                    $(".new")[1].setAttribute("style","display:block");
                }
                if(data.data.send == '0'){
                    $(".new")[2].setAttribute("style","display:none");
                }else{
                    $(".new")[2].setAttribute("style","display:block");
                }
            }
        }
    });

    getContactsList();
    // $("#tmail_contacts").append(contacts_html);
    loadMailAnnex(new Array());

    $('#set_upload_avatar').filer({
        showThumbs: false,
        templates: {
            box: '',
            item: '',
            itemAppend: '',
            progressBar: '',
            itemAppendToEnd: false,
            removeConfirmation: true,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        uploadFile: {
            url: "/upload/file",
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function(){},
            success: function(data, el){
                /*var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-success\"><i class=\"mdui-icon material-icons\">&#xe5ca;</i> 成功</div>").hide().appendTo(parent).fadeIn("slow");
                });*/
                $("#set_img").attr("src",data);
                $("a[href='#!user'] img").attr("src",data);

                var JSONObject = {
                    "username": username,
                    "token": token,
                    "avatarPath":data,
                };
                var JSONText = JSON.stringify(JSONObject);
                $.ajax({
                    type: "POST",
                    url: "/user/updateAvatar",
                    data: JSONText,
                    contentType:"application/json",
                    dataType: "json",
                    success: function(data){
                        if(data.code == 200) {
                            setMessageInnerHTML("设置成功");
                        }else{
                            setMessageInnerHTML("设置失败");
                        }
                    }
                });
            },
            error: function(el){
                /*var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"mdui-icon material-icons\">&#xe000;</i> 失败</div>").hide().appendTo(parent).fadeIn("slow");
                });*/
                setMessageInnerHTML("设置失败")
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        },
        onRemove: function(itemEl, file){
            var file = file.name;
        }
    });
    $("#set_upload_avatar").next("div[class='file-field input-field']").attr("style","display:none");

    $('#set_upload_bg').filer({
        extensions: ['jpg', 'png', 'gif'],
        showThumbs: false,
        templates: {
            box: '',
            item: '',
            itemAppend: '',
            progressBar: '',
            itemAppendToEnd: false,
            removeConfirmation: true,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        uploadFile: {
            url: "/upload/file",
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function(){},
            success: function(data, el){
                // $("#set_bg").attr("src",data);
                setbgPath($("#set_bg").attr("src"),data);
                $.adaptiveBackground.run();

                var JSONObject = {
                    "username": username,
                    "token": token,
                    "bgPath":data,
                };
                var JSONText = JSON.stringify(JSONObject);
                $.ajax({
                    type: "POST",
                    url: "/user/updateBgPath",
                    data: JSONText,
                    contentType:"application/json",
                    dataType: "json",
                    success: function(data){
                        if(data.code == 200) {
                            setMessageInnerHTML("设置成功");
                        }else{
                            setMessageInnerHTML("设置失败");
                        }
                    }
                });

            },
            error: function(el){
                setMessageInnerHTML("设置失败")
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        },
        onRemove: function(itemEl, file){
            var file = file.name;
        }
    });
    $("#set_upload_bg").next("div[class='file-field input-field']").attr("style","display:none");

    getMailList();

    $("body").on("click", "button[name='set_view_deleteMailList']", function () {
         // alert("body on click");
        var JSONObject = {
            "username": username,
            "token": token,
            "name":$(this).prev(".mdui-list-item-content").text(),
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/mailList/deleteList",
            data: JSONText,
            contentType:"application/json",
            dataType: "json",
            success: function(data){
                if(data.code == 200) {
                    setMessageInnerHTML("删除成功");
                    getMailList();
                }else{
                    setMessageInnerHTML("删除失败");
                }
            }
        });
    });
});

/*修改背景路径*/
function setbgPath(path,updatePath){
    $("img[src='"+path+"']").each(function(){
        $(this).attr("src",updatePath);
    });
    $("body").attr("background",updatePath);
}

$("img[data-adaptive-background='1']").load(function(){
    // 加载完成
    // var data_ab_color = $("img[data-adaptive-background='1']").attr("data-ab-color");
    /*while($("img[data-adaptive-background='1']").attr("data-ab-color")==null){
         data_ab_color = $("img[data-adaptive-background='1']").attr("data-ab-color");
    }*/
    if($("img[data-adaptive-background='1']").attr("data-ab-color")!=null) {
        setTheme();
    }else{
        setTimeout('setTheme()',2000);
    }
});

function setTheme() {
    var data_ab_color = $("img[data-adaptive-background='1']").attr("data-ab-color");
    console.log(data_ab_color);
    $("nav[class='top-nav']").attr("style", "height:60px;background-color:" + data_ab_color);
    var bac = /background-color.*?;/;
    $("style").text($("style").text().replace(bac, "background-color:" + data_ab_color));
}

/*//ajax长连接
 $(function () {

 (function longPolling() {

 var timeout=setTimeout(function(){
 //超时啦，干点什么
 longPolling();
 },
 5000 //超时时间，考虑到网络问题，5秒还是比较合理的
 );
 $.get(url,
 function(data,status){
 if(timeout){ //清除定时器
 clearTimeout(timeout);
 timeout=null;
 }
 //该干嘛干嘛
 if(status="success"){
 update(1);
 }
 longPolling();
 });
 });
 });*/

/**/
$("#editorbtn").click(function () {
//        alert($("#mailbody").attr("style"));
    scale("mailbody");
    scale("editorbody");

    setTimeout('if($("#mailbody").attr("style").indexOf("none")!=-1){'+
                '$("#mailbody").attr("style","display:block");'+
               ' $("#editorbody").attr("style","display:none");'+
           ' }else {'+
               ' $("#mailbody").attr("style", "display:none");'+
               ' $("#editorbody").attr("style", "display:block");'+
            '}',500);
    });

function curdMailAjax(url,mailId,type){
    scale("mailbody");
    scale("editorbody");

    setTimeout('$("#mailbody").attr("style","display:block");' +
        '$("#editorbody").attr("style","display:none");',500);

    var to_chip_length = $('.chips-initial').material_chip('data').length;
    var to_address = "";
    while(to_chip_length>0){
        to_address += ","+$('.chips-initial').material_chip('data')[to_chip_length-1].tag;
        to_chip_length--;
    }
    var cc_address = "";
    var cc_chip_length = $('.chips-autocomplete').material_chip('data').length;
    while(cc_chip_length>0){
        cc_address += ","+ $('.chips-autocomplete').material_chip('data')[cc_chip_length-1].tag;
        cc_chip_length--;
    }
    var bcc_address = "";
    var bcc_chip_length = $('.chips-placeholder.chips-initial.chips-autocomplete').material_chip('data').length;
    while(bcc_chip_length>0){
        bcc_address += ","+ $('.chips-placeholder.chips-initial.chips-autocomplete').material_chip('data')[bcc_chip_length-1].tag;
        bcc_chip_length--;
    }

    var mail_annexs = "";
    var send_mail_file_i = 0;
    while(send_mail_file_i<send_mail_File.length){
        if(send_mail_File[send_mail_file_i]!=null){
            mail_annexs += ","+send_mail_File[send_mail_file_i]
        }else{}
        send_mail_file_i++;
    }
    var JSONObject;
    if(mailId != null && type != null){
         JSONObject = {
            "username": username,
            "token": token,
            "from":username,
            "to":to_address.substring(1,to_address.length),
            "subject":$("#subject").val(),
            "text":$("#text").val(),
            "cc":cc_address.substring(1,cc_address.length),
            "bcc":bcc_address.substring(1,bcc_address.length),
            "mail_annex":mail_annexs.substring(1,mail_annexs.length),
            "mailId":mailId,
            "type":type,
        };
    }else if(mailId == null && type == null){
        JSONObject = {
            "username": username,
            "token": token,
            "from":username,
            "to":to_address.substring(1,to_address.length),
            "subject":$("#subject").val(),
            "text":$("#text").val(),
            "cc":cc_address.substring(1,cc_address.length),
            "bcc":bcc_address.substring(1,bcc_address.length),
            "mail_annex":mail_annexs.substring(1,mail_annexs.length),
        };
    }else{}
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "Mail"+username+"/"+url,
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
//        					alert("数据: \n" + data.data.token );

            if(data.code == 15 || data.code == 200) {
                /*var $toastContent = $('<span>操作成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("操作成功");
//                    alert(data.data);
            }else{
                // var $toastContent = $('<span>发送失败</span>');
                // Materialize.toast($toastContent, 5000);
                // $('#toast-container').attr("style","top:0;");
            }
        }
    });
    send_mail_File = new Array();//清空附件
}

$("#savemail").click(function(){
    if(document.getElementById("subject").checkValidity()){
        if(type==0){
            curdMailAjax("updateMail",static_mailId,0);
        }else{
            curdMailAjax("saveMail");
        }
    }else{
        var beforeclass = $('#subject').parent().attr("class");
        $('#subject').parent().attr("class",beforeclass);
    }
});

$("#sendmail").click(function () {
    if(document.getElementById("subject").checkValidity()){
        curdMailAjax("sendMail");
    }else{
        var beforeclass = $('#subject').parent().attr("class");
        $('#subject').parent().attr("class",beforeclass);
    }
});

/*$("a[href='#[1-9]([0-9]{0,10})']").click(function () {
 alert(66666);
 });*/

$('.chips-placeholder').material_chip({
    placeholder: '',
    secondaryPlaceholder: '            +邮箱地址',
});

function getmail(JSONText,url) {
    /*$(".horizontal.card").each(function () {
     $(this).html("");
     });*/
    $("#mailbodydiv").html("");

    $("#set_view").attr("style","display:none");
    $("#editorbtn").attr("style","display:block");
    $("#readmore_view").attr("style","display:none");
    $("#main_view").attr("style","display:block");

    $("#setAllIsRead").removeAttr("disabled");
    $("#refresh").removeAttr("disabled");
    $("#list_type").removeAttr("disabled");

    $.ajax({
        type: "POST",
        url: url,
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 17||data.code == 200) {
                mailList = data.data;
                var i = 0;
                while(i<mailList.length){
                    var beforehtml = $("#mailbodydiv").html();
                    $("#mailbodydiv").html(beforehtml+cardhtml);

                    var userInfoJSONObject = {
                        "username": mailList[i].fromMail,
                    };
                    var userInfoJSONText = JSON.stringify(userInfoJSONObject);

                    $.ajax({
                        type: "POST",
                        url: "/user/getUserInfo",
                        data: userInfoJSONText,
                        contentType:"application/json",
                        dataType: "json",
                        async:false, //等待执行后再执行下面的源码
                        success: function(data){
                            if(data.code == 200) {
                                $(".card-image img:last").attr("src",data.data.avatar_path);
                                data.data.nick_name;
                            }
                        }
                    });

                    mailList[i].toMail;
                    $(".card-content p:last").html(mailList[i].subject);
                    mailList[i].wcc;
                    mailList[i].bcc;
                    mailList[i].content;
                    mailList[i].timestamp;
                    if(mailList[i].type == 0){
                        $("div[class='card-action']:last")
                            .append('<a href="#" name="editMail" mdui-tooltip="{content: \'编辑\'}"><i class="mdui-icon material-icons">&#xe3c9;</i></a>');
                        $(".card-action a:last").prev("a").prev("a").attr("href","#"+mailList[i].mailId);
                    }else if(mailList[i].type ==2){
                        $("div[class='card-action']:last")
                            .append('<a href="#" name="replyMail" mdui-tooltip="{content: \'回复\'}"><i class="mdui-icon material-icons">&#xe15e;</i></a>');
                        $(".card-action a:last").prev("a").prev("a").attr("href","#"+mailList[i].mailId);
                        $("div[class='card-action']:last")
                            .append('<a href="#" name="forwardMail" mdui-tooltip="{content: \'转发\'}"><i class="mdui-icon material-icons">&#xe154;</i></a>');
                        $(".card-action a:last").prev("a").prev("a").attr("href","#"+mailList[i].mailId);
                    }
                    else{}
                    if(mailList[i].flagRead == 0){
                        $(".card-action a span").attr("style","display:block");
                    }else{}
                    $(".card-action a:last").attr("href","#"+mailList[i].mailId);
                    $(".card-action a:last").prev("a").attr("href","#"+mailList[i].mailId);
                    // $(".card-action a:last").prev("a").prev("a").attr("href","#"+mailList[i].mailId);
                    i++;
                }
                $("a[name='readmore']").click(function () {
                    scale("readmore_view");
                    scale("main_view");
                    scale("set_view");

                    setTimeout('$("#readmore_view").attr("style","display:block");' +
                        '$("#main_view").attr("style","display:none");' +
                        '$("#set_view").attr("style","display:none");',500);

                    $("#editorbtn").attr("style","display:none");

//                        alert($(this).attr("href"));
                    var mailId = $(this).attr("href");
                    var JSONObject = {
                        "username": username,
                        "token": token,
                        "mailId":mailId.substring(1,mailId.length),
                    };

//                        alert(mailId.substring(1,mailId.length));
                    var JSONText = JSON.stringify(JSONObject);
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/searchMailByContent",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        async:false, //等待执行后再执行下面的源码
                        success: function(data){
                            if(data.code == 200) {
//                                    var $toastContent = $('<span>发送成功</span>');
//                                    Materialize.toast($toastContent, 5000);
                                $("#readmore_mail_content").html(data.data.content);
                                $("#readmore_mail_subject").html(data.data.subject);
//									$("#readmore_mail_from").html('<img src="fontImage/'+data.data.fromMail.substring(0,1)+'.png" alt="Contact Person">'+data.data.fromMail);
//                                    $("#readmore_mail_from").html('<img src="fontImage/'+data.data.toMail.substring(0,1)+'.png" alt="Contact Person">'+data.data.toMail);
                                $("#readmore_mail_from").text(data.data.fromMail);
                                $("#readmore_mail_to").text(data.data.toMail);
                                $("#readmore_mail_time").html(new Date(data.data.timestamp).toLocaleString());
                                if(data.data.wcc!=null){
                                    $("#readmore_mail_cc").text(data.data.wcc);
                                }else{
                                    $("#readmore_mail_cc").html("");
                                }
                                if(data.data.bcc!=null){
                                    $("#readmore_mail_bcc").text(data.data.bcc);
                                }else{
                                    $("#readmore_mail_bcc").html("");
                                }

                            }else{
                                /*var $toastContent = $('<span>查询失败</span>');
                                Materialize.toast($toastContent, 5000);*/
                                setMessageInnerHTML("查询失败");
                                $('#toast-container').attr("style","top:0;");
                            }
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/setRead",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data){
                            if(data.code == 200) {
//                                    var $toastContent = $('<span>发送成功</span>');
//                                    Materialize.toast($toastContent, 5000);
                            }else{
                                /*var $toastContent = $('<span>操作失败</span>');
                                Materialize.toast($toastContent, 5000);*/
                                setMessageInnerHTML("操作失败");
                                $('#toast-container').attr("style","top:0;");
                            }
                        }
                    });
                    send_mail_File = new Array();
                    $("#readmore_mail_annex").html("");
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/getMailAnnex",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data){
                            if(data.code == 200) {
                                var files_length = data.data.files.length;
                                console.log(files_length+data.data.files[files_length-1].name);
                                while(files_length>0){
                                    $('#readmore_mail_annex').append('<li class="mdui-list-item mdui-ripple">'+
                                        '<div class="mdui-list-item-content">文件名：'+data.data.files[files_length-1].name+' 大小：'+data.data.files[files_length-1].size+'byte </div>'+
                                        '<a src="'+data.data.files[files_length-1].path+'"></a>' +
                                        '<button class="mdui-btn mdui-btn-icon" name="download_mail_annex"><i class="mdui-icon material-icons">&#xe2c4;</i></button>'+
                                        '</li>');
                                    files_length--;
                                }
                                $("button[name='download_mail_annex']").click(function(){
                                    // download($(this).prev("a").attr("src"));
                                    window.location.href=$(this).prev("a").attr("src");
                                });

                            }else{

                            }
                        }
                    });
                });

                $("a[name='delMail']").click(function () {
//                        alert("delMail"+$(this).attr("href"));
                    $(this).parent().parent().parent().parent().attr("style","display:none");
                    var mailId = $(this).attr("href");
                    var JSONObject = {
                        "username": username,
                        "token": token,
                        "mailId":mailId.substring(1,mailId.length),
                        "afterType":3,
                    };
                    var JSONText = JSON.stringify(JSONObject);
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/delMail",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data){
                            if(data.code == 200) {
                                /*var $toastContent = $('<span>删除成功</span>');
                                Materialize.toast($toastContent, 5000);*/
                                setMessageInnerHTML("删除成功");
                            }else{
                                /*var $toastContent = $('<span>删除失败</span>');
                                Materialize.toast($toastContent, 5000);*/
                                setMessageInnerHTML("删除失败");
                                $('#toast-container').attr("style","top:0;");
                            }
                        }
                    });
                });

                $("a[name='editMail']").click(function () {
                    var mailId = $(this).attr("href");
                    static_mailId = mailId.substring(1);
                    var JSONObject = {
                        "username": username,
                        "token": token,
                        "mailId":mailId.substring(1,mailId.length),
                    };
                    var JSONText = JSON.stringify(JSONObject);
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/searchMailByContent",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data) {
                            loadMail(data);
                        }
                    });
                    send_mail_File = new Array();
                    $("#readmore_mail_annex").html("");
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/getMailAnnex",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data){
                            if(data.code == 200) {
//                                    var $toastContent = $('<span>删除成功</span>');
//                                    Materialize.toast($toastContent, 5000);
                                var files_length = data.data.files.length;
                                var showfiles = new Array();
                                while(files_length>0){
                                    var file={
                                        name: data.data.files[files_length-1].name,
                                        size: data.data.files[files_length-1].size,
                                        type: data.data.files[files_length-1].type,
                                        file: data.data.files[files_length-1].path,
                                    };
                                    showfiles.push(file);
                                    files_length--;
                                }
                                loadMailAnnex(showfiles);
                            }else{
//                                    var $toastContent = $('<span>删除失败</span>');
//                                    Materialize.toast($toastContent, 5000);
//                                    $('#toast-container').attr("style","top:0;");
                            }
                        }
                    });
                });

                $("a[name='replyMail']").click(function () {
                    var mailId = $(this).attr("href");
                    var JSONObject = {
                        "username": username,
                        "token": token,
                        "mailId":mailId.substring(1,mailId.length),
                    };
                    var JSONText = JSON.stringify(JSONObject);
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/searchMailByContent",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data) {
                            if(data.code == 200){
                                var fromMail = data.data.toMail;
                                var toMail = data.data.fromMail;
                                var subject = "Re:"+data.data.subject;
                                var content = "</p>在"+new Date(data.data.timestamp).toLocaleString()+","+toMail+"写道：</br></p>"+data.data.content;
                                data.data.bcc = "";
                                data.data.wcc = "";
                                data.data.toMail = toMail;
                                data.data.fromMail = fromMail;
                                data.data.subject = subject;
                                data.data.content = content;
                            }else{}
                            loadMail(data);
                        }
                    });
                    send_mail_File = new Array();
                    $("#readmore_mail_annex").html("");
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/getMailAnnex",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data){
                            if(data.code == 200) {
//                                    var $toastContent = $('<span>删除成功</span>');
//                                    Materialize.toast($toastContent, 5000);
                                var files_length = data.data.files.length;
                                var showfiles = new Array();
                                while(files_length>0){
                                    var file={
                                        name: data.data.files[files_length-1].name,
                                        size: data.data.files[files_length-1].size,
                                        type: data.data.files[files_length-1].type,
                                        file: data.data.files[files_length-1].path,
                                    };
                                    showfiles.push(file);
                                    files_length--;
                                }
                                loadMailAnnex(showfiles);
                            }else{
//                                    var $toastContent = $('<span>删除失败</span>');
//                                    Materialize.toast($toastContent, 5000);
//                                    $('#toast-container').attr("style","top:0;");
                            }
                        }
                    });
                });

                $("a[name='forwardMail']").click(function () {
                    var mailId = $(this).attr("href");
                    var JSONObject = {
                        "username": username,
                        "token": token,
                        "mailId":mailId.substring(1,mailId.length),
                    };
                    var JSONText = JSON.stringify(JSONObject);
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/searchMailByContent",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data) {
                            if(data.code == 200){
                                var fromMail = data.data.toMail;
                                var toMail = data.data.fromMail;
                                var subject = "Fw:"+data.data.subject;
                                var content = "<p>----- 转发邮件信息 -----</br>" +
                                    "发件人："+data.data.fromMail+"</br>" +
                                    "发送时间："+new Date(data.data.timestamp).toLocaleString()+"</br>" +
                                    "收件人："+data.data.toMail+"</br>" +
                                    "主题："+data.data.subject+"</br></p>" +data.data.content;
                                data.data.bcc = "";
                                data.data.wcc = "";
                                data.data.toMail = toMail;
                                data.data.fromMail = fromMail;
                                data.data.subject = subject;
                                data.data.content = content;
                            }else{}
                            loadMail(data);
                        }
                    });
                    send_mail_File = new Array();
                    $("#readmore_mail_annex").html("");
                    $.ajax({
                        type: "POST",
                        url: "Mail"+username+"/getMailAnnex",
                        data: JSONText,
                        contentType:"application/json",
                        dataType: "json",
                        success: function(data){
                            if(data.code == 200) {
//                                    var $toastContent = $('<span>删除成功</span>');
//                                    Materialize.toast($toastContent, 5000);
                                var files_length = data.data.files.length;
                                var showfiles = new Array();
                                while(files_length>0){
                                    var file={
                                        name: data.data.files[files_length-1].name,
                                        size: data.data.files[files_length-1].size,
                                        type: data.data.files[files_length-1].type,
                                        file: data.data.files[files_length-1].path,
                                    };
                                    showfiles.push(file);
                                    files_length--;
                                }
                                loadMailAnnex(showfiles);
                            }else{
//                                    var $toastContent = $('<span>删除失败</span>');
//                                    Materialize.toast($toastContent, 5000);
//                                    $('#toast-container').attr("style","top:0;");
                            }
                        }
                    });
                });

                /*统计当前类型邮件数目，用于显示分页*/
                var CountObject = {
                    "username": username,
                    "token": token,
                    "type":type,
                };
                var CountText = JSON.stringify(CountObject);
                $.ajax({
                    type: "POST",
                    url: "Mail"+username+"/count",
                    data: CountText,
                    contentType:"application/json",
                    dataType: "json",
                    success: function(data){
//                            alert(data.code+data.data.count);
                        if(data.code == 17) {
                            var count = data.data.count;
//								alert(count);
                            if(count > 0 ){
                                /*显示分页控件*/
                                $("ul[class='pagination']").attr('style','display:block');
                                var page = Math.ceil(count/10);
//                                    alert(page);
                                $("ul[class='pagination']").children()[0].setAttribute("style","display:none");
                                $("ul[class='pagination']").children()[6].setAttribute("style","display:none");
                                if(page > 5){
                                    $("ul[class='pagination']").children()[5].innerHTML
                                        = '<a href="#!">'+page+'</a>';
                                    $("ul[class='pagination']").children()[3].innerHTML
                                        = '<a href="#!">...</a>';
                                }else if(page < 5){
                                    for(var i=5;i>page;i--){
                                        $("ul[class='pagination']").children()[i].setAttribute("style","display:none");
                                    }
                                }
                            }
                        }else{

                        }
                    }
                });
                /*  $(".card-action a").each(function () {
                 var card = $(this);
                 $(this).bind('click',function(){
                 //                            alert($(this).attr("href"));
                 var showmailcard = card.parent().parent().parent().parent();
                 showmailcard.next("div").attr("style","display:block");
                 showmailcard.attr("style","display:none");
                 var k = 0;
                 while (k < mailList.length){
                 //                                alert($(this).attr("href")+"::"+"#"+mailList[k].mailId);
                 if("#"+mailList[k].mailId == $(this).attr("href")){
                 showmailcard.next("div").children(":last").children("div").children("div").children("input")[0].value = mailList[k].toMail;
                 showmailcard.next("div").children(":last").children("div").children("div").children("input")[1].value = mailList[k].fromMail;
                 showmailcard.next("div").children(":last").children("div").children("div").children("span").html(mailList[k].subject);
                 showmailcard.next("div").children(":last").children("div").children("div").children("p").html(mailList[k].content);
                 }else{}
                 k++;
                 }

                 $(".card-action a").each(function () {
                 var othercard = $(this);
                 //                                alert(card.attr("href"));
                 if(card.attr("href") != othercard.attr("href")&&othercard.attr("href") != '#'){
                 othercard.parent().parent().parent().parent().attr("style","display:block");
                 othercard.parent().parent().parent().parent().next("div").attr("style","display:none");
                 }
                 });
                 });
                 });*/

                /*var $toastContent = $('<span>加载成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("加载成功");
            }else{
                /*var $toastContent = $('<span>加载失败</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("加载失败");
                $('#toast-container').attr("style","top:0;");
            }
        }
    });

}

$("#search").bind("input propertychange",function(){


    if($("#search").val() != "" && isSearch>0){
        isSearch =0;
        type = 100;
        key = $("#search").val();

        var JSONObject = {
            "username": username,
            "token": token,
            "key":key,
            "whichPage":1,
            "max":10,
            "listType":"时间",
        };
        var JSONText = JSON.stringify(JSONObject);
        getmail(JSONText,"Mail"+username+"/searchMail");
    }else{}

    setTimeout("isSearch=1; ",2000);
});

$("#set_nickname_submit").click(function () {
//		alert($("#set_nickname").val());
    var JSONObject = {
        "username": username,
        "token": token,
        "nickname":$("#set_nickname").val(),
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "/user/updateNickname",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                /*var $toastContent = $('<span>更改成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("更改成功");
                $("a[href='#!name'] span").text($("#set_nickname").val());
            }else{
                /*var $toastContent = $('<span>更改失败</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("更改失败");
                $('#toast-container').attr("style","top:0;");
            }
        }
    });

});
$("#annex_commit").click(function () {
    var file = $("#annex_lists")[0].files;
    var data = new FormData();
    data.append("filename",file.name);
    data.append("file",file);
    $.ajax({
        type: "POST",
        url: "/upload/file",
        data: data,
        processData: false,//告诉jquery不要去处理发送的数据
        contentType: false,//告诉jquery不要去设置content-type
        success: function(data){
            if(data.code == 200) {
                /*var $toastContent = $('<span>更改成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("更改成功");
            }else{
                /*var $toastContent = $('<span>更改失败</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("更改失败");
                $('#toast-container').attr("style","top:0;");
            }
        }
    });
});

$("#set_img").click(function () {
    $("#set_upload_avatar").click();
});

$("#set_bg").click(function () {
    $("#set_upload_bg").click();
});

$("#set_password_submit").click(function () {
    var JSONObject = {
        "username": username,
        "token": token,
        "password":$("#set_password").val(),
        "newPassword":$("#set_password_again").val(),
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "/user/changepw",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 7) {
                /*var $toastContent = $('<span>更改成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("更改成功");
            }else{
               /* var $toastContent = $('<span>更改失败</span>');
                Materialize.toast($toastContent, 5000);*/
               setMessageInnerHTML("更改失败");
                $('#toast-container').attr("style","top:0;");
            }
        }
    });
});

$("ul[class='pagination']").children().click(function () {

    if(type!=100){
        //                        alert("delMail"+$(this).attr("href"));
//		resetPage();
        setPageActive();
        $(this).attr("class","active");
//		alert($(this).text());
        var getmailJSONObject = {
            "username": username,
            "token": token,
            "type": type,
            "whitchPage":$(this).text(),
            "max":10,
            "listType":list_type,
        };
        var getmailJSONText = JSON.stringify(getmailJSONObject);
        getmail(getmailJSONText,token+"/getInbox");
    }

});

/*将所有分页控件*/
function setPageActive()
{
    var i = 1;
    while(i <= 5) {
        $("ul[class='pagination']").children()[i].setAttribute("class", "waves-effect");
        i++;
    }
}

/*重置分页控件*/
function resetPage()
{
    $("ul[class='pagination']").html('<li class="disabled"><a href="#!"><i class="mdui-icon material-icons">&#xe408;</i></a></li>'
        +'<li class="active"><a href="#!">1</a></li>'
        +'<li class="waves-effect"><a href="#!">2</a></li>'
        +'<li class="waves-effect"><a href="#!">3</a></li>'
        +'<li class="waves-effect"><a href="#!">4</a></li>'
        +'<li class="waves-effect"><a href="#!">5</a></li>'
        +'<li class="waves-effect"><a href="#!"><i class="mdui-icon material-icons">&#xe409;</i></a></li> ');
}

$("#readmore_view_close").click(function(){
    scale("readmore_view");
    scale("set_view");
    scale("main_view");

    setTimeout('$("#readmore_view").attr("style","display:none");' +
        '$("#set_view").attr("style","display:none");' +
        '$("#main_view").attr("style","display:block");',500);

    $("#editorbtn").attr("style","display:block");
});

$("#set_view_close").click(function(){

    scale("readmore_view");
    scale("set_view");
    scale("main_view");
    scale("editorbtn");

    setTimeout(' $("#readmore_view").attr("style","display:none");' +
        '$("#set_view").attr("style","display:none");' +
        '$("#main_view").attr("style","display:block");' +
        '$("#editorbtn").attr("style","display:block");',500);

    $("#setAllIsRead").removeAttr("disabled");
    $("#refresh").removeAttr("disabled");
    $("#list_type").removeAttr("disabled");
});

$("#add_contacts_commit").click(function () {
    $("#add_contacts_nickname").val();
    var JSONObject = {
        "username": username,
        "token": token,
        "name":$("#add_contacts_whitchGroup").val(),
        "otherUsername":$("#add_contacts_username").val(),
        "remarks":$("#add_contacts_nickname").val(),
        "phone":$("#add_contacts_phone").val(),
        "address":$("#add_contacts_address").val(),
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "/contacts/addContacts",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                /*var $toastContent = $('<span>添加成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("添加成功");
                getContactsList();
            }else{
                /*var $toastContent = $('<span>添加失败</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("添加失败");
                $('#toast-container').attr("style","top:0;");
            }
        }
    });
});

$("#add_contacts_group_commit").click(function () {
    var JSONObject = {
        "username": username,
        "token": token,
        "name":$("#add_contacts_group_name").val(),
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "/contacts/addContactsGroup",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                /*var $toastContent = $('<span>添加成功</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("添加成功");
                getContactsList();
            }else{
                /*var $toastContent = $('<span>添加失败</span>');
                Materialize.toast($toastContent, 5000);*/
                setMessageInnerHTML("添加失败");
                $('#toast-container').attr("style","top:0;");
            }
        }
    });
});

$("#mian_view_editor_close").click(function(){
    $("#editorbtn").click();
});

function getContactsList(){
    /*获取通讯录列表*/
    var getContactsListJSONObject = {
        "username": username,
        "token": token,
    };
    var getContactsListJSONText = JSON.stringify(getContactsListJSONObject);
    /*contacts_html = '<li class="mdui-list-item mdui-ripple">'
     +'<div class="mdui-list-item-avatar"><img src="avatar1.jpg"/></div>'
     +'<div class="mdui-list-item-content">Brendan Lim</div>'
     +'<button class="mdui-btn mdui-btn-icon"><i class="mdui-icon material-icons">&#xe163;</i></button>'
     +'</li>';
     contacts_group_html = '<li class="mdui-subheader-inset">Friends</li>';*/
    $.ajax({
        type: "POST",
        url: "/contacts/getContacts",
        data: getContactsListJSONText,
        contentType:"application/json",
        dataType: "json",
        async:false, //等待执行后再执行下面的源码
        success: function(data){
            if(data.code == 200) {
                $("#tmail_contacts").html("");//清空联系人列表
                $("#add_contacts_whitchGroup").html("");//清空新增联系人表单中的下拉框
                $("#set_view_contacts_row").html("");
                for(var key in data.data){
                    $("#add_contacts_whitchGroup").append('<option value="'+key+'">'+key+'</option>');//初始化新增联系人表单中的下拉框

                    /*初始化设置界面的分组*/
                    $("#set_view_contacts_row").append('<div class="col s12">'
                        +'<span name="set_view_contacts_row_group">'+key+'</span>'
                        +'<button class="mdui-btn mdui-btn-icon mdui-float-right" name="set_view_contacts_row_group_del" mdui-tooltip="{content: \'删除分组\'}"><i class="mdui-icon material-icons">&#xe92b;</i></button>'
                        +'<button class="mdui-btn mdui-btn-icon mdui-float-right" name="set_view_contacts_row_group_edit" mdui-tooltip="{content: \'编辑分组\'}"><i class="mdui-icon material-icons">&#xe3c9;</i></button>'
                        +'</div>');

                    $("#tmail_contacts").append('<li class="mdui-subheader-inset mdui-text-truncate">'+key+'</li>');
                    var contacts = data.data[key];
                    for(var key1 in contacts){
                        var contact = contacts[key1];
                        if(contact.nick_name == null){
                            contact.nick_name = contact.user_name;
                        }
                        $("#tmail_contacts").append('<li class="mdui-list-item mdui-ripple">'
                            +'<div class="mdui-list-item-avatar"><img src="'+contact.avatar+'"/></div>'
                            +'<div class="mdui-list-item-content mdui-text-truncate">'
                                +'<div class="mdui-list-item-title">'+contact.nick_name+'</div>'
                                +'<div class="mdui-list-item-text mdui-list-item-one-line"><span class="mdui-text-color-theme-text">'+contact.user_name+'</span></div>'
                            +'</div>'
                            +'</div>'
                            +'<button name="editbycontacts" class="mdui-btn mdui-btn-icon"><i class="mdui-icon material-icons">&#xe163;</i></button>'
                            +'</li>');
                    }
                }
                /*获取默认值*/
                default_tmail_contacts = $("#tmail_contacts").html();
                /*初始化下拉框*/
                $('select').material_select();

                initContactsAciton();

                /*$(".mdui-subheader-inset").click(function(){
                    $(this).append('<ul class="mdui-menu" id="menu">'
                        +'<li class="mdui-menu-item">'
                        +'<a href="javascript:;" class="mdui-ripple">Refresh</a>'
                        +'</li>'
                        +'<li class="mdui-menu-item" disabled>'
                        +'<a href="javascript:;">Help & feedback</a>'
                        +'</li>'
                        +'<li class="mdui-menu-item">'
                        +'<a href="javascript:;" class="mdui-ripple">Settings</a>'
                        +'</li>'
                        +'<li class="mdui-divider"></li>'
                        +'<li class="mdui-menu-item">'
                        +'<a href="javascript:;" class="mdui-ripple">Sign out</a>'
                        +'</li>'
                        +'</ul>');
                    var inst = new mdui.Menu('.mdui-subheader-inset', '#menu');

                    inst.open();
                });*/
            }
        }
    });
}
/*触发搜索事件*/
$("#searchContact").keyup(function(){
    $("#tmail_contacts").html(default_tmail_contacts);
    var scu = 0;
    while(scu < $(".mdui-list-item-text").length){
        // var search_contact_avatar = $($(".mdui-list-item-avatar")[scu]).children("img").attr("src");
        var search_contact_username = $($(".mdui-list-item-text")[scu]).text();
        var search_contact_nickname = $($(".mdui-list-item-title")[scu]).text();
        // var searcg_contact_group = $($(".mdui-list-item.mdui-ripple")[scu]).prev(".mdui-subheader-inset").text();
        /*if(search_contact_username.indexOf($("#searchContact").val())!=-1 || search_contact_nickname.indexOf($("#searchContact").val())!=-1){
            $("#tmail_contacts").html("");//清空联系人列表
            $("#tmail_contacts").append('<li class="mdui-subheader-inset" style="display: none">'+searcg_contact_group+'</li>');
            $("#tmail_contacts").append('<li class="mdui-list-item mdui-ripple">'
                +'<div class="mdui-list-item-avatar"><img src="'+search_contact_avatar+'"/></div>'
                +'<div class="mdui-list-item-content">'
                +'<div class="mdui-list-item-title">'+search_contact_nickname+'</div>'
                +'<div class="mdui-list-item-text mdui-list-item-one-line"><span class="mdui-text-color-theme-text">'+search_contact_username+'</span></div>'
                +'</div>'
                +'</div>'
                +'<button name="editbycontacts" class="mdui-btn mdui-btn-icon"><i class="mdui-icon material-icons">&#xe163;</i></button>'
                +'</li>');
        }else{}
        scu++;*/
        if(search_contact_username.indexOf($("#searchContact").val())==-1 && search_contact_nickname.indexOf($("#searchContact").val())==-1){
            $($(".mdui-list-item.mdui-ripple")[scu]).attr("style","display:none");
        }else{}
        scu++;
    }

    if($("#searchContact").val()==""){
        $("#tmail_contacts").html(default_tmail_contacts);
    }
    initContactsAciton();
});

/*搜索框关闭按钮触发事件*/
$("#searchContact_close").click(function () {
    $("#searchContact").val("");
    $("#tmail_contacts").html(default_tmail_contacts);
    initContactsAciton();
});

/*初始化通讯录联系人的相关操作*/
function initContactsAciton(){
    /*按钮点击事件*/
    $("button[name='editbycontacts']").click(function(){
        if($(this).attr("class").indexOf("mdui-btn mdui-btn-icon")!=-1){
            var to = $(this).prev().children(".mdui-list-item-text").text();
            $("#editorbtn").click();
            $('.chips-initial').material_chip({
                data: [{
                    tag: to,
                }],
            });
        }else{}
        return false;
    });
    /*初始化联系人详情div*/
    $(".mdui-list-item.mdui-ripple").click(function(){
        var contacts_group = $($(this).prevAll(".mdui-subheader-inset")[0]).text();
        var contacts_username = $(this).children(".mdui-list-item-content").children(".mdui-list-item-text").text();
        var contacts_nickname = $(this).children(".mdui-list-item-content").children(".mdui-list-item-title").text();
        // alert(toto);
        if($("#contactsbodydiv").attr("class").indexOf("scale-in")==-1){
            scale("contactsbodydiv");
            scale("mailbodydiv");
            setTimeout('$("#contactsbodydiv").attr("style","display:block");' +
                '$("#mailbodydiv").attr("style","display:none");' ,500);
        }else{
            $("#contactsbodydiv_close").click();
        }


        $("#contactsbody_email").text("邮箱:"+contacts_username);
        $("#contactsbody_username").text(contacts_username);
        $("#contactsbody_remarks").text("备注:"+contacts_nickname);
        $("#contactsbody_group").text("所在分组："+contacts_group);

        var JSONObject = {
            "username": username,
            "token": token,
            "groupName":contacts_group,
            "otherUsername":contacts_username,
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/contacts/getContact",
            data: JSONText,
            contentType:"application/json",
            dataType: "json",
            success: function(data){
                if(data.code == 200) {
                    $("#contactsbody_address").text('地址：'+data.data.address);
                    $("#contactsbody_phone").text('电话：'+data.data.phone);
                }else{
                }
            }
        });
    });

    $("button[name='set_view_contacts_row_group_edit']").click(function(){
        var set_view_contacts_row = $(this);
        var set_view_contacts_row_group = set_view_contacts_row.prevAll("span[name='set_view_contacts_row_group']").parent();
        var text = set_view_contacts_row_group.children("span").text();
        var html = set_view_contacts_row_group.html();
        $(this).prevAll("span[name='set_view_contacts_row_group']").parent().html('<div class="input-field">'+
            '<input value="'+text+'" type="text" class="validate">'+
            '</div>'+
            '<button class="mdui-btn mdui-btn-icon mdui-float-right" name="set_view_contacts_row_group_edit_commit"><i class="mdui-icon material-icons">&#xe5ca;</i></button>'+
            '<button class="mdui-btn mdui-btn-icon mdui-float-right" name="set_view_contacts_row_group_edit_cancle"><i class="mdui-icon material-icons">&#xe5cd;</i></button>');

        $("button[name='set_view_contacts_row_group_edit_commit']").click(function(){
            var JSONObject = {
                "username": username,
                "token": token,
                "groupName":text,
                "afterGroupName": $(this).prevAll("div").children("input").val(),
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/contacts/updateContactsGroup",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        getContactsList();
                        $("button[name='set_view_contacts_row_group_edit_cancle']").click();
                    }else{
                    }
                }
            });
        });

        $("button[name='set_view_contacts_row_group_edit_cancle']").click(function(){
            $(this).parent().html(html);
            getContactsList();
        });
    });

    $("button[name='set_view_contacts_row_group_del']").click(function(){
        var JSONObject = {
            "username": username,
            "token": token,
            "name":$(this).prevAll("span[name='set_view_contacts_row_group']").text(),
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/contacts/removeContactsGroup",
            data: JSONText,
            contentType:"application/json",
            dataType: "json",
            success: function(data){
                if(data.code == 200) {
                    getContactsList();
                }else{
                }
            }
        });
    });
}

$("#refresh").click(function(){
    var getmailJSONObject = {
        "username": username,
        "token": token,
        "type": type,
        "whitchPage":1,
        "max":10,
    };
    var getmailJSONText = JSON.stringify(getmailJSONObject);
    getmail(getmailJSONText,token+"/getInbox");
});

$("#listByTime").click(function () {
    list_type="时间";
    var getmailJSONObject = {
        "username": username,
        "token": token,
        "type": type,
        "whitchPage":1,
        "max":10,
        "listType":list_type,
    };
    var getmailJSONText = JSON.stringify(getmailJSONObject);
    getmail(getmailJSONText,token+"/getInbox");
});

$("#listByFrom").click(function () {
    list_type="发件人";
    var getmailJSONObject = {
        "username": username,
        "token": token,
        "type": type,
        "whitchPage":1,
        "max":10,
        "listType":list_type,
    };
    var getmailJSONText = JSON.stringify(getmailJSONObject);
    getmail(getmailJSONText,token+"/getInbox");
});

$("#listBySubject").click(function () {
    list_type="主题";
    var getmailJSONObject = {
        "username": username,
        "token": token,
        "type": type,
        "whitchPage":1,
        "max":10,
        "listType":list_type,
    };
    var getmailJSONText = JSON.stringify(getmailJSONObject);
    getmail(getmailJSONText,token+"/getInbox");
});

$("#setAllIsRead").click(function () {
    var JSONObject = {
        "username": username,
        "token": token,
        "type":type,
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "Mail"+username+"/setReadByType",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                $("#refresh").click();
            }else{
            }
        }
    });
});

$("#contactsbodydiv_close").click(function(){
    scale("contactsbodydiv");
    scale("mailbodydiv");
    setTimeout('$("#contactsbodydiv").attr("style","display:none");' +
        '$("#mailbodydiv").attr("style","display:block");',500);

    $("#contactsbodydiv_edit").removeAttr("disabled");
    $("#contactsbody").html('<div class="container">'
    +'<h4 id="contactsbody_username">用户名</h4>'
        +'</div>'
        +'<div class="container">'
        +'<h5 id="contactsbody_email">邮箱</h5>'
        +'</div>'
        +'<div class="container">'
        +'<h5 id="contactsbody_phone">电话</h5>'
        +'</div>'
        +'<div class="container">'
        +'<h5 id="contactsbody_address">地址</h5>'
        +'</div>'
        +'<div class="container">'
        +'<h5 id="contactsbody_remarks">备注</h5>'
        +'</div>'
        +'<div class="container">'
        +'<h5 id="contactsbody_group">所在分组</h5>'
        +'</div>');
});

$("#contactsbodydiv_edit").click(function () {
    $("#contactsbodydiv_edit").attr("disabled","");
    var cb_username = $("#contactsbody_username").text();
    var cb_nickname = $("#contactsbody_remarks").text();
    var cb_group = $("#contactsbody_group").text();
    var cb_address = $("#contactsbody_address").text();
    var cb_phone = $("#contactsbody_phone").text();
    cb_group = cb_group.substring(5,cb_group.length);
    cb_nickname = cb_nickname.substring(3,cb_nickname.length);
    cb_address = cb_address.substring(3,cb_address.length);
    cb_phone = cb_phone.substring(3,cb_phone.length);
    $("#contactsbody").html('<div class="container">'
    +'<div>编辑联系人</div>'
        +'<div class="mdui-textfield">'
        +'<label class="mdui-textfield-label">邮箱地址</label>'
        +'<input id="edit_contacts_username" class="mdui-textfield-input" type="email"/>'
        +'</div>'
        +'<div class="mdui-textfield">'
        +'<label class="mdui-textfield-label">昵称</label>'
        +'<input id="edit_contacts_nickname" class="mdui-textfield-input" type="text"/>'
        +'</div>'
        +' <div class="mdui-textfield">'
        +'<label class="mdui-textfield-label">电话</label>'
        +'<input id="edit_contacts_phone" class="mdui-textfield-input" type="text"/>'
        +' </div>'
        +' <div class="mdui-textfield">'
        +'<label class="mdui-textfield-label">地址</label>'
        +' <input id="edit_contacts_address" class="mdui-textfield-input" type="text"/>'
        +' </div>'
        +'<div class="input-field">'
        +'<select id="edit_contacts_whitchGroup">'
        +'<option value="" disabled selected>'+cb_group+'</option>'
        +'</select>'
        +'<label>移动到</label>'
        +'</div>'
        +'</div>'
        +'<button id="edit_contacts_commit" class="mdui-btn mdui-ripple mdui-btn-block"><i class="mdui-icon material-icons">&#xe5ca;</i>确定</button>');
    $("#edit_contacts_username").val(cb_username);
    $("#edit_contacts_email").val(cb_username);
    $("#edit_contacts_nickname").val(cb_nickname);
    $("#edit_contacts_phone").val(cb_phone);
    $("#edit_contacts_address").val(cb_address);
    var ec=0;
    while(ec<$("#add_contacts_whitchGroup").children().length){
        var ec_group = $("#add_contacts_whitchGroup").children()[ec].text;
        if(ec_group != cb_group){
            $("#edit_contacts_whitchGroup").append('<option value="'+ec_group+'">'+ec_group+'</option>');//初始化编辑联系人表单中的下拉框
        }
        ec++;
    }
    $('select').material_select();

    $("#edit_contacts_commit").click(function(){
        var updatedGroupName = $("#edit_contacts_whitchGroup").val();
        if(updatedGroupName == null){
            updatedGroupName = cb_group;
        }else{}
        var JSONObject = {
            "username": username,
            "token": token,
            "groupName":cb_group,
            "otherUsername":cb_username,
            "remarks":cb_nickname,
            "address":$("#edit_contacts_address").val(),
            "phone":$("#edit_contacts_phone").val(),
            "updatedGroupName":updatedGroupName,
        };
        var JSONText = JSON.stringify(JSONObject);
        $.ajax({
            type: "POST",
            url: "/contacts/updateContacts",
            data: JSONText,
            contentType:"application/json",
            dataType: "json",
            success: function(data){
                if(data.code == 200) {
                    getContactsList();
                }else{
                }
            }
        });
        $("#contactsbodydiv_close").click();
    });
});

$("#contactsbodydiv_del").click(function(){
    var cb_username = $("#contactsbody_username").text();
    var cb_group = $("#contactsbody_group").text();
    cb_group = cb_group.substring(5,cb_group.length);
    mdui.confirm('确认删除联系人'+$("#contactsbody_username").text()+'吗？',
        function(){
            // mdui.alert('点击了确认按钮');
            var JSONObject = {
                "username": username,
                "token": token,
                "name":cb_group,
                "otherUsername":cb_username,
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/contacts/removeContacts",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        /*var $toastContent = $('<span>添加成功</span>');
                         Materialize.toast($toastContent, 5000);*/
                        getContactsList();
                    }else{
                        /*var $toastContent = $('<span>添加失败</span>');
                         Materialize.toast($toastContent, 5000);
                         $('#toast-container').attr("style","top:0;");*/
                    }
                }
            });
        },
        function(){
            // mdui.alert('点击了取消按钮');
        }
    );
    $("#contactsbodydiv_close").click();
});

$("#add_blacklist").click(function () {
    // 含标题
    mdui.prompt('添加黑名单', '请输入需要添加的黑名单，黑名单只支持邮箱地址',
        function (value) {
            // mdui.alert('你输入了：' + value + '，点击了确认按钮');
            var JSONObject = {
                "username": username,
                "token": token,
                "name":value,
                "type":0,
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/mailList/addList",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        setMessageInnerHTML("添加成功");
                        $("#add_blacklist").next("ul").append('<li class="mdui-list-item mdui-ripple">'+
                            '<div class="mdui-list-item-content">'+value+'</div>'+
                            '<button class="mdui-btn mdui-btn-icon" name="set_view_deleteMailList"><i class="mdui-icon material-icons">&#xe15b;</i></button>'+
                            '</li>');
                    }else{
                        setMessageInnerHTML("添加失败");
                    }
                }
            });
        },
        function (value) {
            // mdui.alert('你输入了：' + value + '，点击了取消按钮');
        }
    );
});

$("#add_whitelist").click(function () {
    // 含标题
    mdui.prompt('添加白名单', '请输入需要添加的白名单，白名单只支持邮箱地址',
        function (value) {
            // mdui.alert('你输入了：' + value + '，点击了确认按钮');
            var JSONObject = {
                "username": username,
                "token": token,
                "name":value,
                "type":1,
            };
            var JSONText = JSON.stringify(JSONObject);
            $.ajax({
                type: "POST",
                url: "/mailList/addList",
                data: JSONText,
                contentType:"application/json",
                dataType: "json",
                success: function(data){
                    if(data.code == 200) {
                        setMessageInnerHTML("添加成功");
                        $("#add_whitelist").next("ul").append('<li class="mdui-list-item mdui-ripple">'+
                            '<div class="mdui-list-item-content">'+value+'</div>'+
                            '<button class="mdui-btn mdui-btn-icon" name="set_view_deleteMailList"><i class="mdui-icon material-icons">&#xe15b;</i></button>'+
                        '</li>');
                    }else{
                        setMessageInnerHTML("添加失败");
                    }
                }
            });
        },
        function (value) {
            // mdui.alert('你输入了：' + value + '，点击了取消按钮');
        }
    );
});

function getMailList() {
    var JSONObject = {
        "username": username,
        "token":token,
    };
    var JSONText = JSON.stringify(JSONObject);
    $.ajax({
        type: "POST",
        url: "/mailList/getAllBlackList",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                $("#add_blacklist").next("ul").html("");
                var list_length = data.data.length;
                // alert(list_length+"##"+data.data[list_length]);
                while(list_length>0){
                    $("#add_blacklist").next("ul").append('<li class="mdui-list-item mdui-ripple">'+
                        '<div class="mdui-list-item-content">'+data.data[list_length-1]+'</div>'+
                        '<button class="mdui-btn mdui-btn-icon" name="set_view_deleteMailList"><i class="mdui-icon material-icons">&#xe15b;</i></button>'+
                        '</li>');
                    list_length--;
                }
            }else{
                setMessageInnerHTML("加载失败");
            }
        }
    });
    $.ajax({
        type: "POST",
        url: "/mailList/getAllWhiteList",
        data: JSONText,
        contentType:"application/json",
        dataType: "json",
        success: function(data){
            if(data.code == 200) {
                $("#add_whitelist").next("ul").html("");
                var list_length = data.data.length;
                while(list_length>0){
                    $("#add_whitelist").next("ul").append('<li class="mdui-list-item mdui-ripple">'+
                        '<div class="mdui-list-item-content">'+data.data[list_length-1]+'</div>'+
                        '<button class="mdui-btn mdui-btn-icon" name="set_view_deleteMailList"><i class="mdui-icon material-icons">&#xe15b;</i></button>'+
                        '</li>');
                    list_length--;
                }
            }else{
                setMessageInnerHTML("加载失败");
            }
        }
    });
}

function scale(id){
    var beforeClass = $("#"+id).attr("class");
    if(beforeClass.indexOf("scale-transition")!=-1){
        if(beforeClass.indexOf("scale-transition scale-out")!=-1){
            if(beforeClass.indexOf("scale-transition scale-out scale-in")!=-1){
                $("#"+id).attr("class",beforeClass.replace("scale-transition scale-out scale-in","scale-transition scale-out"))
            }else{
                $("#"+id).attr("class",beforeClass.replace("scale-transition scale-out","scale-transition scale-out scale-in"));
            }
        }else{
            $("#"+id).attr("class",beforeClass.replace("scale-transition","scale-transition scale-out"));
        }
    }else{
        $("#"+id).attr("class",beforeClass+" scale-transition");
    }
}

//兼容调节元素的透明度
function setOpacity(elem,level){
    if(elem.filters)
        elem.style.filter = "alpha(opacity="+level+")";
    else
        elem.style.opacity = level/100;
}
//用递归渐渐显示,
//这里的speed参数为显示速率，我将其暂时定义成：speed越大，过度时间越长
function fadeIn(elem,speed){
    var e = typeof elem == 'string'? document.getElementById(elem) : elem;
    setOpacity(e,0);//先设置透明度为0
    e.style.display="block";//再让其block
    var i=0;
    function dg(){
        if(i<=100)
        {(function(){
            var pos =i;
            setTimeout(function(){
                setOpacity(e,pos);
            },(pos+1)*speed);
        })();
            i+=1;dg();}
    }
    dg();
}

$(document).ajaxComplete(function(event,jqXHQ,options){

    // console.log(ajaxArray.length);
    /*console.log(options.url.indexOf("getMailAnnex"));
    if(options.url.indexOf("getMailAnnex")!=-1){
        $("#readmore_mail_annex").html("");
    }else{}*/
    ajaxArray.push(options.url);
    // addHistory(options.url);
    if(jqXHQ.responseJSON.code==14){
        setMessageInnerHTML("用户验证信息已过期，请重新登陆");
        sockjs.close();
        window.stop;
        mdui.prompt('用户验证信息已过期，请重新登陆', '密码',
            function (value) {
                // mdui.alert('你输入了：' + value + '，点击了确认按钮');
                var JSONObject = {
                    "username": username,
                    "password": value,
                };
                var JSONText = JSON.stringify(JSONObject);
                $.ajax({
                    type: "POST",
                    url: "/user/login",
                    data: JSONText,
                    contentType:"application/json",
                    dataType: "json",
                    success: function(data){
                        if(data.code == 0) {
                            token = data.data.token;
                            $('#modal1').modal('close');
                            /*var $toastContent = $('<span>登录成功</span>');
                            Materialize.toast($toastContent, 5000);*/
                            setMessageInnerHTML("登录成功");
                            window.location.href="/inbox.jsp?t="+token+"&u="+ username;
                        }else{
                            if(data.data==""){
                                /*var $toastContent = $('<span>登录失败</span>');
                                Materialize.toast($toastContent, 5000);*/
                                setMessageInnerHTML("登录失败");
                                $('#toast-container').attr("style","top:0;");
                            }else{
                                /*var $toastContent = $('<span>'+data.data+'</span>');
                                Materialize.toast($toastContent, 5000);*/
                                setMessageInnerHTML(data.data);
                                $('#toast-container').attr("style","top:0;");
                            }
                            $('#modal1').modal('close');
                        }
                    }
                });
            },
            function (value) {
                // mdui.alert('你输入了：' + value + '，点击了取消按钮');
            }
        );
    }else if(history_hash!=prev_history_hash){
        // addHistory(history_hash);
        prev_history_hash = history_hash;
    } else{}
});
function addHistory(address){
    history.pushState({title: document.title, html: document.body.innerHTML}, '', "#"+address);
    window.onpopstate = function(event){
        if(event && event.state){
            document.title = event.state.title;
            document.body.innerHTML = event.state.html;
        }
    }
}

function loadMail(data){
    if(data.code == 200) {
        $("#editorbtn").click();
        var bcc = data.data.bcc.split(",");
        editor.$txt.html(data.data.content);
        data.data.fromMail;
        var wcc = data.data.wcc.split(",");
        $("#subject").val(data.data.subject);
        var tomail = data.data.toMail.split(",");
        var timestamp = new Date(data.data.timestamp).toLocaleString();

        var tomail_length = tomail.length;
        var bcc_length = bcc.length;
        var wcc_length = wcc.length;
        var tomail_array = new Array();
        var bcc_array = new Array();
        var wcc_array = new Array();
        while(tomail_length>0){
            var chip = {
                tag: tomail[tomail_length-1],
                image: '', //optional
                id: tomail_length, //optional
            };
            tomail_array.push(chip);
            tomail_length--;
        }
        while(bcc_length>0){
            var chip = {
                tag: bcc[bcc_length-1],
                image: '', //optional
                id: bcc_length, //optional
            };
            bcc_array.push(chip);
            bcc_length--;
        }
        while(wcc_length>0){
            var chip = {
                tag: wcc[wcc_length-1],
                image: '', //optional
                id: wcc_length, //optional
            };
            wcc_array.push(chip);
            wcc_length--;
        }
        /*console.log(wcc==null);*/
        /*console.log("wcc:"+wcc+
                "     wcc_length:"+wcc_length+
                "      wcc_array:"+wcc_array+
                "        wcc_array.length"+wcc_array.length+
                "        tomail:"+tomail+
                "     tomail_length:"+tomail_length+
                "        tomail_array:"+tomail_array+
            "       tomal:"+tomail_array.length+"       bcc:::::"+bcc_length+"     length::::::::::"+bcc_array.length);*/
        $('.chips.chips-initial.chips-placeholder').material_chip({
            data: tomail!=""?tomail_array:new Array(),
        });
        $('.chips.chips-placeholder.chips-autocomplete').material_chip({
            data: wcc!=""?wcc_array:new Array(),
        });
        $('.chips.chips-placeholder.chips-initial.chips-autocomplete').material_chip({
            data: bcc!=""?bcc_array:new Array(),
        });

        var mail_annexs = "";
        var send_mail_file_i = 0;
        while(send_mail_file_i<send_mail_File.length){
            if(send_mail_File[send_mail_file_i]!=null){
                mail_annexs += ","+send_mail_File[send_mail_file_i]
            }else{}
            send_mail_file_i++;
        }
    }else{
//
    }
}

function loadMailAnnex(showfiles){
    if($('#filer_input').parent().attr("class")!=null){
        $('#filer_input').parent().html('<input type="file" name="files[]" id="filer_input" multiple="multiple">');
    }


    $('#filer_input').filer({
        showThumbs: true,
        files:showfiles,
        templates: {
            box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
            item: '<li class="jFiler-item">\
                    <div class="jFiler-item-container">\
                        <div class="jFiler-item-inner">\
                            <div class="jFiler-item-thumb">\
                                <div class="jFiler-item-status"></div>\
                                <div class="jFiler-item-info">\
                                    <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                    <span class="jFiler-item-others">{{fi-size2}}</span>\
                                </div>\
                                {{fi-image}}\
                            </div>\
                            <div class="jFiler-item-assets jFiler-row">\
                                <ul class="list-inline pull-left">\
                                    <li>{{fi-progressBar}}</li>\
                                </ul>\
                                <ul class="list-inline pull-right">\
                                    <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                </ul>\
                            </div>\
                        </div>\
                    </div>\
                </li>',
            itemAppend: '<li class="jFiler-item">\
                        <div class="jFiler-item-container">\
                            <div class="jFiler-item-inner">\
                                <div class="jFiler-item-thumb">\
                                    <div class="jFiler-item-status"></div>\
                                    <div class="jFiler-item-info">\
                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name | limitTo: 25}}</b></span>\
                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                    </div>\
                                    {{fi-image}}\
                                </div>\
                                <div class="jFiler-item-assets jFiler-row">\
                                    <ul class="list-inline pull-left">\
                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                    </ul>\
                                    <ul class="list-inline pull-right">\
                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                    </ul>\
                                </div>\
                            </div>\
                        </div>\
                    </li>',
            progressBar: '<div class="bar"></div>',
            itemAppendToEnd: false,
            removeConfirmation: true,
            _selectors: {
                list: '.jFiler-items-list',
                item: '.jFiler-item',
                progressBar: '.bar',
                remove: '.jFiler-item-trash-action'
            }
        },
        uploadFile: {
            url: "/upload/file",
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function(){},
            success: function(data, el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                send_mail_File.push(data);
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-success\"><i class=\"mdui-icon material-icons\">&#xe5ca;</i> 成功</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            error: function(el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"mdui-icon material-icons\">&#xe000;</i> 失败</div>").hide().appendTo(parent).fadeIn("slow");
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        },
        onRemove: function(itemEl, file){
            var file = file.name;
            var i = 0;
            while(i<send_mail_File.length){
                if(send_mail_File[i].indexOf(file)!=-1){
                    send_mail_File.splice(i,1);
                    i=send_mail_File.length;
                }else{}
                i++;
            }
            console.log(file+"被删除了");
            // $.post('./php/remove_file.php', {file: file});
        }
    });

    // $(".jFiler-input-caption").children("span").text("选择需要上传的文件");
    // $(".jFiler-input-button").text("附件上传");
    $(".file-path.validate").attr("style","display:none");
}

$("#addBccOrWccBtn").click(function(){
    if($(this).parent().parent().next().attr("style")==null){
        $(this).parent().parent().next().attr("style","display:none");
        $(this).parent().parent().next().next().attr("style","display:none");
    }else{
        if($(this).parent().parent().next().attr("style").indexOf("none")!=-1){
            $(this).parent().parent().next().attr("style","display:block");
            $(this).parent().parent().next().next().attr("style","display:block");
        }else{
            $(this).parent().parent().next().attr("style","display:none");
            $(this).parent().parent().next().next().attr("style","display:none");
        }
    }

});

$("input[name='add_contacts']").keyup(function () {
    if(document.getElementById("add_contacts_username").checkValidity()){
        $("#add_contacts_commit").removeAttr("disabled");
    }else{
        $("#add_contacts_commit").attr("disabled","");
    }
});

$("#add_contacts_group_name").keyup(function () {
    if(document.getElementById("add_contacts_group_name").checkValidity()){
        $("#add_contacts_group_commit").removeAttr("disabled");
    }else{
        $("#add_contacts_group_commit").attr("disabled","");
    }
});

$("#subject").keyup(function () {
    if(document.getElementById("subject").checkValidity()){
        $("#sendmail").removeAttr("disabled");
        $("#savemail").removeAttr("disabled");
    }else{
        $("#sendmail").attr("disabled","");
        $("#savemail").attr("disabled","");
    }
});

//将消息显示在网页上
function setMessageInnerHTML(innerHTML){
    if(isToast > 0){
        isToast =0;
        var $toastContent = $('<span>'+innerHTML+'</span>');
        Materialize.toast($toastContent, 5000);
    }else{}
    setTimeout("isToast = 1;",5000);
}

$('#toTop').click(function() {
    $('body,html').animate({ scrollTop: 0 }, 200);
});

/*//关闭连接
 function closeWebSocket(){
 websocket.close();
 }

 //发送消息
 function send(){
 var message = document.getElementById('text').value;
 websocket.send(message);}*/

function GetQueryString(name)//获取地址栏链接参数
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if(r!=null)return  unescape(r[2]); return null;
}

function reload(){
    window.location.reload()
}

/*function update(num)
 {
 if(num!=-1){
 var $toastContent = $('<a href="#" onclick="reload()">'+num+'条新数据，刷新可查看</a>');
 Materialize.toast($toastContent, 5000);
 $('#toast-container').attr("style","top:5%;");
 }
 else{
 var $toastContent = $('<a href="#" onclick="reload()">没有新数据</a>');
 Materialize.toast($toastContent, 5000);
 $('#toast-container').attr("style","top:5%;");
 }
 }*/
